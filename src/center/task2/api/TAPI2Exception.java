package center.task2.api;

public class TAPI2Exception extends Exception {

	private static final long serialVersionUID = 8164535828994155306L;
	public TAPI2Exception(String cause) {
		super(cause);
	}

}
