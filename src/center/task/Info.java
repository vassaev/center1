package center.task;

import java.io.IOException;
import java.io.InputStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

public final class Info {
	private Info() {
		super();
	}

	public static void main(String[] args) {
		new Info().info();
	}

	public void info() {
		final InputStream mfStream = getClass().getClassLoader()
				.getResourceAsStream("META-INF/MANIFEST.MF");
		Manifest mf = new Manifest();
		try {
			mf.read(mfStream);
		} catch (IOException e) {
			e.printStackTrace();
		}

		Attributes atts = mf.getMainAttributes();
		System.out.println(atts.getValue(Attributes.Name.IMPLEMENTATION_TITLE));
		System.out.println("version: "
				+ atts.getValue(Attributes.Name.IMPLEMENTATION_VERSION));
		System.out.println("built date:" 
				+ atts.getValue("Built-Date"));
	}
}