package center.task.api.mem;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import ru.vassaev.core.Queue;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.thread.Processed;
import ru.vassaev.core.util.Strings;
import center.task.State;
import center.task.TypeOfState;
import center.task.api.ATaskAPI;
import center.task.api.ITaskAPI;
import center.task.api.TaskCI;

public final class TaskAPI extends ATaskAPI {

	private static Map<String, ITaskAPI> helpers = new TreeMap<String, ITaskAPI>();
	public static ITaskAPI getInstance(String alias) throws SysException {
		synchronized (helpers) {
			ITaskAPI obj = helpers.get(alias);
			if (obj != null) {
				return obj;
			} else {
				TaskAPI o = new TaskAPI(alias);
				helpers.put(alias, o);
				return o;
			}
		}
	}

	private final String alias;
  private final long base_id;
  private final Object sync; 
  //private final Map<Long, Set<String>> classes;

  private final Map<Long, TSK> tasks_all;
  private final Map<Long, TSK> tasks_created;
  // Готовые задачи по классам 
  private final Map<String, ArrayList<TSK>> tasks_ready; 
  // Задачи, блокированные процессором 
  private final Map<Long, Map<Long, TSK>> tasks_blocked; 

  private final Map<Long, TSK> tasks_processing; 
  private final Map<Long, TSK> tasks_finished; 
  private final Map<Long, SBJ> subjects;
	private final ProcessGarbage prc;
	private final Queue<TSK> tasks; 

  @SuppressWarnings("unused")
	private void print_status() {
  	Map<State, Integer> s = new HashMap<State, Integer>();
		synchronized(tasks_all) {
			for (TSK t : tasks_all.values()) {
				Integer c = s.get(t.status_id);
				if (c == null)
					c = 1;
				else
				  c++;
				s.put(t.status_id, c);
			}
		}
		for (Map.Entry<State, Integer> t : s.entrySet()) {
			System.out.println(alias + ":::" + t.getKey() + ":::" + t.getValue()); 
		}
  }
  
	// Класс сборщика мусора
	private class ProcessGarbage implements Processed<Object> {
		public void process_subjects() {
			synchronized(subjects) {
				Iterator<Map.Entry<Long, SBJ>> s = subjects.entrySet().iterator();
				while (s.hasNext()) {
					Map.Entry<Long, SBJ> e = s.next();
					SBJ v = e.getValue();
					if (System.currentTimeMillis() - v.last_ping > 1000L*3600L) {
						if (v.classes == null || v.classes.size() == 0) {
							v.ping();
							continue;
						}
						s.remove();
						System.out.println("Subject by id = " + e.getKey() + " was removed!");
					}
				}
			}
		}
		@Override
		public void processing(Object prm) throws Throwable {
			@SuppressWarnings("unchecked")
			Queue<TSK> tasks = (Queue<TSK>)prm;
			TSK first = new TSK(0, null, 0);
			while(true) {
				TSK tsk = tasks.get();
				if (first.id == null) {
					synchronized(first) {
						try {
							first.wait(10000);
//							print_status();
						} catch (InterruptedException e) {
							throw e;
						}
					}
					tasks.put(first);
					process_subjects();
					continue;
				}
				State st;
				long dt;
				synchronized(tsk) {
					st = tsk.status_id;
					dt = tsk.dt_arch;
				}
				if (st.getType().equals(TypeOfState.LAST) 
						&& System.currentTimeMillis() > dt + 1000L*60L) {
					synchronized(tasks_finished) {
						tasks_finished.remove(tsk.id);
					}
					synchronized (prms) {
						prms.remove(tsk.id);
					}
					synchronized(tasks_all) {
						tasks_all.remove(tsk.id);
					}
					System.out.println("Task by id = " + tsk.id + " was removed!");
					continue;
				}
				tasks.put(tsk);
			}
		}
		
	}
  private TaskAPI(String alias) throws SysException {
		this.alias = alias;
		long tm = System.currentTimeMillis();
		this.base_id =  tm << 16L;
		this.sync = new Object();
		this.sync_ready_blocked = new Object();
		this.subjects = new HashMap<Long, SBJ>();
		this.tasks_all = new HashMap<Long, TSK>();
		this.tasks_created = new HashMap<Long, TSK>();
		this.tasks_ready = new HashMap<String, ArrayList<TSK>>();
		this.tasks_blocked = new HashMap<Long, Map<Long, TSK>>();
		this.tasks_processing = new HashMap<Long, TSK>();
		this.tasks_finished = new HashMap<Long, TSK>();
		this.prms = new HashMap<Long, Map<String, PRM>>();
		this.prc = new ProcessGarbage();
		this.tasks = new Queue<TSK>();
		new ru.vassaev.core.thread.Process(prc).start(tasks);
	}

	public String getAlias() {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		return alias;
	}

	private long last_subject = 0;
	public long getSubject(long old_subject_id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		SBJ sb;
    synchronized(sync) {
    	sb = subjects.get(old_subject_id);
    	if (sb != null)
    		return sb.id;
  		long subject = last_subject++;
  		sb = new SBJ(base_id + subject);
    	subjects.put(sb.id, sb);
    }
		return sb.id;
	}

	public void registerClasses(long subject_id, Set<String> hs)
			throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
  	SBJ sb;
    synchronized(sync) {
    	sb = subjects.get(subject_id);
    }
    if (sb == null)
    	throw new SysException ("Unknown Subject");
    else synchronized(sb) {
    	sb.ping();
    }
		Set<String> r = new HashSet<String>();
    for (String s : hs) {
      r.add(s);
      log(subject_id, 0, "Class by name " + s + " was added for searching");
    }
  	synchronized(sb) {
  		sb.classes = r;
  	}
	}

	public long log(long subject_id, long id, String info, boolean base)
			throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
  	SBJ sb;
    synchronized(sync) {
    	sb = subjects.get(subject_id);
    }
    if (sb == null)
    	throw new SysException ("Unknown Subject");
    else synchronized(sb) {
    	sb.ping();
    }
		synchronized (sync) {
			System.out.println(this.alias + " : " + subject_id + " : " + id
					+ " :\t" + info);
		}
		return id;
	}

	public long log(long subject_id, long id, String info) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
  	SBJ sb;
    synchronized(sync) {
    	sb = subjects.get(subject_id);
    }
    if (sb == null)
    	throw new SysException ("Unknown Subject");
    else synchronized(sb) {
    	sb.ping();
    }
		synchronized (sync) {
			System.out.println(this.alias + " : " + subject_id + " : " + id
					+ " :\t" + info);
		}
		return id;
	}

	private final Object sync_ready_blocked;
	private void putBlocked(long subject_id, TSK tsk) {
		Map<Long, TSK> tsks = tasks_blocked.get(subject_id);
		if (tsks == null) {
			tsks = new HashMap<Long, TSK>();
	    tasks_blocked.put(subject_id, tsks);
		}
		tsk.status_id = State.BLOCKED;
		tsks.put(tsk.id, tsk);
	}
	public TaskCI getBlocked(long subject_id, long wait)
			throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
  	SBJ sb;
    synchronized(sync) {
    	sb = subjects.get(subject_id);
    }
    if (sb == null)
    	throw new SysException ("Unknown Subject");
    else synchronized(sb) {
    	sb.ping();
    }
		long tm = System.currentTimeMillis();
		TaskCI tci = new TaskCI();
		tci.id_task = 0;
		Set<String> fcls;
		synchronized(sync) {
			fcls = sb.classes;
		}
		if (fcls == null || fcls.size() == 0)
			return tci;
		ArrayList<TSK> tsks;
		TSK tsk;
		long delta;
		synchronized(sync_ready_blocked) {
			while(true) {
				for (String c : fcls) {
				  tsks = tasks_ready.get(c);
				  if (tsks == null || tsks.size() == 0)
				  	continue;
				  while(!tsks.isEmpty())  {
				  	tsk = tsks.remove(0);
				    if (tsk.dt_end_block != 0 && tsk.dt_end_block < System.currentTimeMillis())
				  	  continue;
				    if (tsk.finish_duration != 0 
				    		&& (tsk.dt_goal + tsk.finish_duration*1000L <= System.currentTimeMillis()))
				  	  continue;
				    putBlocked(subject_id, tsk);
				    tci.id_task = tsk.id;
				    tci.cls_name = tsk.classname;
				    return tci;
				  }
				}
				if (wait < 0)
					return tci;
				try {
					if (wait == 0)
						sync_ready_blocked.wait();
  				else { 
  					delta = System.currentTimeMillis() - tm;
  					if (wait - delta > 0)
	  				  sync_ready_blocked.wait(wait - delta);
  					else
  						return tci;
  				}
				} catch (InterruptedException e) {
					e.printStackTrace();
					return tci;
				}
			}
		}
	}

	private long last_task = 0;
	public long createTask(long subject_id, String cls) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		return createTask(subject_id, cls, 0);
	}

	public long createTask(long subject_id, String cls, long parent_task_id)
			throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
  	SBJ sb;
    synchronized(sync) {
    	sb = subjects.get(subject_id);
    }
    if (sb == null)
    	throw new SysException ("Unknown Subject");
    else synchronized(sb) {
    	sb.ping();
    }
		TSK tsk = new TSK(subject_id, cls, parent_task_id);
		tsk.processor_id = 0L;
		tsk.parent_id = parent_task_id;
		tsk.block_duration = 10;
		tsk.finish_duration = 120;
		tsk.status_id = State.CREATED;
		tsk.id = base_id + (last_task++);
		tsk.arch_day = 1;
		tsk.classname = cls;
		tsk.dt_created = System.currentTimeMillis();
		tsk.dt_end_block = tsk.dt_created + tsk.block_duration*1000;
		tsk.dt_goal = tsk.dt_created;
		synchronized(tasks_created) {
			tasks_created.put(tsk.id, tsk);
		}
		synchronized(tasks_all) {
			tasks_all.put(tsk.id, tsk);
		}
		tasks.put(tsk);
		return tsk.id;
	}

	public long setTaskDT(long id, String goal_dt, String finish_duration,
			String block_duration, Double arch_day) throws SysException {
		// TODO Надо поменять эту функцию!!!
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		TSK tsk;
		synchronized(tasks_created) {
			tsk = tasks_created.get(id);
		}
		if (tsk == null)
			return 0;
		synchronized(tsk) {
			tsk.dt_goal = Strings.parseIntegerNvl(goal_dt, System.currentTimeMillis());
			tsk.block_duration = Strings.parseIntegerNvl(block_duration, 10L);
			tsk.dt_end_block = tsk.block_duration * 1000 + tsk.dt_created;
			tsk.finish_duration = Strings.parseIntegerNvl(finish_duration, 120L);
			tsk.last_ping = System.currentTimeMillis();
		}
		return tsk.dt_goal;
	}

	public long setReady(long subject_id, long id, Long processor_id)
			throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
  	SBJ sb;
    synchronized(sync) {
    	sb = subjects.get(subject_id);
    }
    if (sb == null)
    	throw new SysException ("Unknown Subject");
    else synchronized(sb) {
    	sb.ping();
    }
		TSK tsk;
		synchronized(tasks_created) {
			tsk = tasks_created.remove(id);
			if (tsk == null)
				return 0;
			if (tsk.initiator_id != subject_id) {
				tasks_created.put(id, tsk);
				return 0;
			}
		}
		synchronized(tsk) {
			tsk.status_id = State.READY;
			tsk.last_ping = System.currentTimeMillis();
			tsk.processor_id = subject_id;
		}
		synchronized(sync_ready_blocked) {
		  ArrayList<TSK> tsks = tasks_ready.get(tsk.classname);
		  if (tsks == null || tsks.size() == 0) {
		  	tsks = new ArrayList<TSK>();
		  	tasks_ready.put(tsk.classname, tsks);
		  }
		  tsks.add(tsk);
			log(subject_id, id, "set Status=READY");
		  sync_ready_blocked.notifyAll();
		}
		return id;
	}

	public long setProcessing(long subject_id, long id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
  	SBJ sb;
    synchronized(sync) {
    	sb = subjects.get(subject_id);
    }
    if (sb == null)
    	throw new SysException ("Unknown Subject");
    else synchronized(sb) {
    	sb.ping();
    }
		TSK tsk;
		synchronized(sync_ready_blocked) {
			Map<Long, TSK> tsks = tasks_blocked.get(subject_id);
		  if (tsks == null || tsks.size() == 0) 
		    return 0;
		  tsk = tsks.remove(id);
			if (tsk == null)
				return 0;
			if (tsk.processor_id != subject_id)
				tsks.put(tsk.id, tsk);
		}
		synchronized(tsk) {
			tsk.status_id = State.PROCESSING;
		}
		synchronized(tasks_processing) {
			tasks_processing.put(id, tsk);
		}
		return tsk.id;
	}

	private Map<Long, Map<String, PRM>> prms;
	public Object getParam(long id, String fullname) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		Map<String, PRM> prm;
		synchronized (prms) {
			prm = prms.get(id);
			if (prm == null)
				return null;
		}
		synchronized(prm) {
			PRM p = prm.get(fullname);
			if (p != null)
				return p.getValue();
			return null;
		}
	}

	public State getState(long id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		TSK tsk;
		synchronized(tasks_all) {
			tsk = tasks_all.get(id);
		}
		if (tsk == null)
			return State.UNKNOWN;
		return tsk.status_id;
	}

	public long setParamObject(long task_id, String name, Object value,
			long wait, long calc, long scall) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		Map<String, PRM> prm;
		synchronized(prms) {
			prm = prms.get(task_id);
			if (prm == null) {
				prm = new HashMap<String, PRM>();
			  prms.put(task_id, prm);
			}
		}
		PRM p;
		synchronized(prm) {
			p = prm.get(name);
			if (p == null) {
				p = new PRM(name, value);
				prm.put(name, p);
			} else
				p.setValue(value);
			p.setTime(wait, calc, scall);
		}
		return p.id;
	}

	public State waitFinished(long subject_id, long id, long wait_msec)
			throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
  	SBJ sb;
    synchronized(sync) {
    	sb = subjects.get(subject_id);
    }
    if (sb == null)
    	throw new SysException ("Unknown Subject");
    else synchronized(sb) {
    	sb.ping();
    }
		TSK tsk;
		long tm = System.currentTimeMillis();
		synchronized(tasks_all) {
	  	tsk = tasks_all.get(id);
		}
  	if (tsk == null)
  		return null;
  	long delta;
		synchronized(tsk) {
		  while (true) {
		  	if (TypeOfState.LAST.equals(tsk.status_id.getType()))
		  		return tsk.status_id;
				delta = System.currentTimeMillis() - tm;
				if (wait_msec - delta > 0)
					try {
						tsk.wait(wait_msec - delta);
					} catch (InterruptedException e) {
						throw new SysException(e);
					}
				else
					return tsk.status_id;
		  }
		}
	}


	public long setDoneOk(long subject_id, long id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
  	SBJ sb;
    synchronized(sync) {
    	sb = subjects.get(subject_id);
    }
    if (sb == null)
    	throw new SysException ("Unknown Subject");
    else synchronized(sb) {
    	sb.ping();
    }
		TSK tsk;
		synchronized(tasks_processing) {
			tsk = tasks_processing.remove(id);
		}
  	if (tsk == null) {
  		synchronized(tasks_all) {
  			tsk = tasks_all.get(id);
  		}
    	if (tsk == null) 
  		  return 0;
  		synchronized(tsk) {
  			if (State.DONE_OK.equals(tsk.status_id)) {
  				return tsk.id;
  			}
  		}
    	return 0;
  	}
  	State old_st;
		synchronized(tsk) {
			old_st = tsk.status_id;
			if (State.BREAKING.equals(old_st) || State.PROCESSING.equals(old_st)) {
			  tsk.status_id = State.DONE_OK;
			  tsk.notifyAll();
			}
		}
		synchronized(tasks_finished) {
			tasks_finished.put(tsk.id, tsk);
		}
		return tsk.id;
	}

	public long setDoneErr(long subject_id, long id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
  	SBJ sb;
    synchronized(sync) {
    	sb = subjects.get(subject_id);
    }
    if (sb == null)
    	throw new SysException ("Unknown Subject");
    else synchronized(sb) {
    	sb.ping();
    }
		TSK tsk;
		synchronized(tasks_processing) {
			tsk = tasks_processing.remove(id);
		}
  	if (tsk == null) {
  		synchronized(tasks_all) {
  			tsk = tasks_all.get(id);
  		}
    	if (tsk == null) 
  		  return 0;
  		synchronized(tsk) {
  			if (State.DONE_ERR.equals(tsk.status_id)) {
  				return tsk.id;
  			}
  		}
    	return 0;
  	}
  	State old_st;
		synchronized(tsk) {
			old_st = tsk.status_id;
			if (State.BREAKING.equals(old_st) || State.PROCESSING.equals(old_st)) {
			  tsk.status_id = State.DONE_ERR;
			  tsk.notifyAll();
			}
		}
		synchronized(tasks_finished) {
			tasks_finished.put(tsk.id, tsk);
		}
		return tsk.id;
	}

	public long setTaskError(long id, Throwable e) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		TSK tsk;
		synchronized(tasks_all) {
			tsk = tasks_all.get(id);
			if (tsk == null)
				return 0;
		}
		synchronized(tsk) {
			tsk.errs = e.toString();
			tsk.errp = e;
		}
		return tsk.id;
		
	}

	public String getParamTask(long id, String name) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		TSK tsk;
		synchronized(tasks_all) {
			tsk = tasks_all.get(id);
			if (tsk == null)
				return null;
		}
		return tsk.getFieldByName(name);
	}

	public long setTimeout(long subject_id, long id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
  	SBJ sb;
    synchronized(sync) {
    	sb = subjects.get(subject_id);
    }
    if (sb == null)
    	throw new SysException ("Unknown Subject");
    else synchronized(sb) {
    	sb.ping();
    }
		TSK tsk;
		synchronized(tasks_all) {
			tsk = tasks_all.get(id);
			if (tsk == null)
				return 0;
		}
		State st;
		synchronized(tsk) {
			if (State.DONE_TOUT.equals(tsk.status_id))
				return tsk.id;
			if ((tsk.processor_id != subject_id) && (tsk.initiator_id != subject_id))
			  return 0;
			if (TypeOfState.LAST.equals(tsk.status_id.getType()))
				return 0;
			st = tsk.status_id;
		  tsk.status_id = State.DONE_TOUT;
		  tsk.notifyAll();
		}
		if (st.equals(State.BREAKING) || st.equals(State.PROCESSING))
			synchronized(tasks_processing) {
				tasks_processing.remove(id);
			}
		else if (st.equals(State.CREATED))
			synchronized(tasks_created) {
				tasks_created.remove(id);
			}
		synchronized(tasks_finished) {
			tasks_finished.put(tsk.id, tsk);
		}
		return tsk.id;
	}

	@Override
	public Map<String, Object> getGroupParams(long id, String group)
			throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		Map<String, Object> res = new HashMap<String, Object>();
		Map<String, PRM> ps;
		synchronized (prms) {
			ps = prms.get(id);
		}
		if (ps == null)
			return null;
		synchronized (ps) {
			PRM p;
			for (Map.Entry<String, PRM> e : ps.entrySet()) {
			  p = e.getValue();
			  if (group.equals(p.grp))
			  	res.put(e.getKey(), p.getValue());
			}
		}
		return res;
	}

	@Override
	public long setBreaking(long subject_id, long id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
  	SBJ sb;
    synchronized(sync) {
    	sb = subjects.get(subject_id);
    }
    if (sb == null)
    	throw new SysException ("Unknown Subject");
    else synchronized(sb) {
    	sb.ping();
    }
		TSK tsk;
		synchronized(tasks_all) {
			tsk = tasks_all.get(id);
			if (tsk == null)
				return 0;
		}
		synchronized(tsk) {
			State st = tsk.status_id;
			if (State.BREAKING.equals(st))
				return tsk.id;
			if (State.PROCESSING.equals(st)) { 
				tsk.status_id = State.BREAKING;
				return tsk.id;
			}
			if (State.BLOCKED.equals(st)) { 
				tsk.status_id = State.BREAKING;
				return tsk.id;
			}
		}
		return 0;
	}

	@Override
	public long setScheduled(long subject_id, long id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
  	SBJ sb;
    synchronized(sync) {
    	sb = subjects.get(subject_id);
    }
    if (sb == null)
    	throw new SysException ("Unknown Subject");
    else synchronized(sb) {
    	sb.ping();
    }
		TSK tsk;
		synchronized(tasks_all) {
			tsk = tasks_all.get(id);
			if (tsk == null)
				return 0;
		}
		synchronized(tsk) {
			State st = tsk.status_id;
			if (State.SCHEDULED.equals(st))
				return tsk.id;
			if (State.BLOCKED.equals(st)) { 
				tsk.status_id = State.SCHEDULED;
				return tsk.id;
			}
		}
		return 0;
	}

	@Override
	public long setBroken(long subject_id, long id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
  	SBJ sb;
    synchronized(sync) {
    	sb = subjects.get(subject_id);
    }
    if (sb == null)
    	throw new SysException ("Unknown Subject");
    else synchronized(sb) {
    	sb.ping();
    }
		TSK tsk;
		synchronized(tasks_all) {
			tsk = tasks_all.get(id);
			if (tsk == null)
				return 0;
		}
		State st;
		synchronized(tsk) {
			st = tsk.status_id;
			if (State.BROKEN.equals(st))
				return tsk.id;
		}
		if (st.equals(State.BREAKING) && subject_id == tsk.processor_id) {
			synchronized(tasks_processing) {
				tasks_processing.remove(id);
			}
  		synchronized(tasks_finished) {
	  		tasks_finished.put(tsk.id, tsk);
		  }
  		synchronized(tsk) {
  			tsk.status_id = State.BROKEN;
  		}
  		return tsk.id;
	  }
		return 0;
	}

	@Override
	public long setCanceled(long subject_id, long id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
  	SBJ sb;
    synchronized(sync) {
    	sb = subjects.get(subject_id);
    }
    if (sb == null)
    	throw new SysException ("Unknown Subject");
    else synchronized(sb) {
    	sb.ping();
    }
		TSK tsk;
		synchronized(tasks_all) {
			tsk = tasks_all.get(id);
			if (tsk == null)
				return 0;
		}
		State st;
		synchronized(tsk) {
			st = tsk.status_id;
			if (State.CANCELED.equals(st))
				return tsk.id;
		}
		if (st.equals(State.CREATED) && subject_id == tsk.initiator_id) {
			synchronized(tasks_created) {
				tsk = tasks_processing.remove(id);
			}
			if (tsk != null) { 
  		  synchronized(tasks_finished) {
	  		  tasks_finished.put(tsk.id, tsk);
		    }
  		  synchronized(tsk) {
  			  tsk.status_id = State.BROKEN;
  		  }
  		  return tsk.id;
			}
	  }
		if (st.equals(State.READY) && subject_id == tsk.initiator_id) {
			synchronized(tasks_ready) {
				tsk = tasks_processing.remove(id);
			}
			if (tsk != null) { 
  		  synchronized(tasks_finished) {
	  		  tasks_finished.put(tsk.id, tsk);
		    }
  		  synchronized(tsk) {
  			  tsk.status_id = State.BROKEN;
  		  }
  		  return tsk.id;
			}
	  }
		return 0;
	}

	@Override
	public String getClassName(long id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		TSK tsk;
		synchronized(tasks_all) {
			tsk = tasks_all.get(id);
		}
		if (tsk == null)
			return null;
		synchronized(tsk) {
		  return tsk.classname;
		}
	}
	@Override
	public long setBlocked(long subject_id, long id) throws SysException {
		// TODO Auto-generated method stub
  	SBJ sb;
    synchronized(sync) {
    	sb = subjects.get(subject_id);
    }
    if (sb == null)
    	throw new SysException ("Unknown Subject");
    else synchronized(sb) {
    	sb.ping();
    }
		System.out.println("!!! CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + Thread.currentThread().getStackTrace()[1].getLineNumber());
		return 0;
	}
  /////////////////////////////////////////////////

	/*
	public boolean checkDNF(long id, DNF dnf, Object[] flds) throws SysException {
		// TODO Auto-generated method stub
		System.out.println("!!! CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName());
		return false;
	}
  //*/
  /*
	@Override
	public Reader getTaskParamLong(long id, String name) throws SysException {
		// TODO Auto-generated method stub
		System.out.println("!!! CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName());
		return null;
	}
  //*/

	@Override
	public void writeLogToFile(long id, File file, String charset)
			throws SysException {
		// TODO Auto-generated method stub
		System.out.println("!!! CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName());
		
	}

}
