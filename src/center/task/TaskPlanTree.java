package center.task;

import center.task.prm.IDependentParam;

import java.util.Map;
import java.util.Set;

import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.EdgeType;

/**
 * Класс содержит план выполнения задания
 */
public class TaskPlanTree {
  private Graph<String, String> graph = new DirectedSparseGraph<String, String>();
  public TaskPlanTree(TaskInfo ti) {
    Map<String, Object> prms = ti.prms;
    Graph<String, String> d = graph;
    String k = "TSK:" + ti.clsName;
    if (!d.containsVertex(k))
      d.addVertex(k);
    for (Map.Entry<String, Object> p : prms.entrySet()) {
      Object o = p.getValue();
      if (o instanceof IDependentParam) {
        k = "PRM:" + p.getKey();
        if (!d.containsVertex(k))
          d.addVertex(k);
        Set<String> ch = ((IDependentParam)o).depedentOn();
        if (ch != null)
        for (String n : ch) {
          n = "PRM:" + n;
          if (!d.containsVertex(n))
            d.addVertex(n);
          if (!d.containsEdge(n+"->"+k))
            d.addEdge(n+"->"+k, n, k, EdgeType.DIRECTED);
        }
      }
    }
  }
  public Graph<String, String> getGraph() {
    return graph;
  }
}
