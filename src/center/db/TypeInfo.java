package center.db;

import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.db.Manager;
import ru.vassaev.core.db.Types;
import ru.vassaev.core.ClassTableModel;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.sql.ResultSet;

/**
 *
 */
public class TypeInfo extends ClassTableModel {

  public class Row {
    public String TYPE_NAME;
    public int DATA_TYPE;
    public long PRECISION;
    public String LITERAL_PREFIX;
    public String LITERAL_SUFFIX;
    public String CREATE_PARAMS;
    public Short NULLABLE;
    public Boolean CASE_SENSITIVE;
    public Short SEARCHABLE;
    public Boolean UNSIGNED_ATTRIBUTE;
    public Boolean FIXED_PREC_SCALE;
    public Boolean AUTO_INCREMENT;
    public String LOCAL_TYPE_NAME;
    public Short MINIMUM_SCALE;
    public Short MAXIMUM_SCALE;
    public Integer SQL_DATA_TYPE;
    public Integer SQL_DATETIME_SUB;
    public Integer NUM_PREC_RADIX;
  }
  private static Class cl = Row.class;

  public TypeInfo() throws SysException {
    super(cl);
  }

  public synchronized void loadInfoFromSource(DatabaseMetaData md)
    throws SQLException, SysException {
    ResultSet rs = md.getTypeInfo();
    try {
        Types t = Manager.getTypes(md.getConnection());
      while (rs.next()) {
        Row inf = new Row();

        inf.TYPE_NAME = t.getString(rs, "TYPE_NAME");
        inf.DATA_TYPE = t.getInteger(rs, "DATA_TYPE");
        inf.PRECISION = t.getLong(rs, "PRECISION");
        inf.LITERAL_PREFIX = t.getString(rs, "LITERAL_PREFIX");
        inf.LITERAL_SUFFIX = t.getString(rs, "LITERAL_SUFFIX");
        inf.CREATE_PARAMS = t.getString(rs, "CREATE_PARAMS");
        inf.NULLABLE = t.getShort(rs, "NULLABLE");
        inf.CASE_SENSITIVE = t.getBoolean(rs, "CASE_SENSITIVE");
        inf.SEARCHABLE = t.getShort(rs, "SEARCHABLE");
        inf.UNSIGNED_ATTRIBUTE = t.getBoolean(rs, "UNSIGNED_ATTRIBUTE");
        inf.FIXED_PREC_SCALE = t.getBoolean(rs, "FIXED_PREC_SCALE");
        inf.AUTO_INCREMENT = t.getBoolean(rs, "AUTO_INCREMENT");

        inf.LOCAL_TYPE_NAME = t.getString(rs, "LOCAL_TYPE_NAME");
        inf.MINIMUM_SCALE = t.getShort(rs, "MINIMUM_SCALE");
        inf.MAXIMUM_SCALE = t.getShort(rs, "MAXIMUM_SCALE");
        inf.SQL_DATA_TYPE = t.getInteger(rs, "SQL_DATA_TYPE");
        inf.SQL_DATETIME_SUB = t.getInteger(rs, "SQL_DATETIME_SUB");
        inf.NUM_PREC_RADIX = t.getInteger(rs, "NUM_PREC_RADIX");

        addRowAt(inf, getRowCount());
      }
    } finally {
      rs.close();
    }
  }
}
