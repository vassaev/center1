package center.task.api.http;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import ru.vassaev.core.container.Container;
import ru.vassaev.core.exception.SysException;
import center.task.State;
import center.task.api.ITaskAPI;
import center.task.api.TaskCI;

public class TaskAPI implements ITaskAPI {

	private static Map<String, Map<String, String>> prms = new HashMap<String, Map<String, String>>();

	public static void init(Element el, Container con) throws Throwable {
		NodeList children = el.getChildNodes();
		int l = children.getLength();
		Node n;
		Element e;
		String alias;
		String lookup;
		System.out.println("Reading TaskAPI elements...");
		for (int i = 0; i < l; i++) {
			n = children.item(i);
			if (!(n instanceof Element))
				continue;
			e = (Element) n;
			if (e.getTagName().equals("TaskAPI")) {
				alias = e.getAttribute("alias");
				lookup = e.getAttribute("lookup");
				synchronized (prms) {
					Map<String, String> values = prms.get(alias);
					if (values == null)
						values = new HashMap<String, String>();
					values.put("lookup", lookup);
					prms.put(alias, values);
				}
				System.out.println("Registering of TaskAPI (alias = " + alias
						+ ", lookup = " + lookup + ")");
			}

		}
	}

	private static Map<String, ITaskAPI> helpers = new TreeMap<String, ITaskAPI>();

	public static ITaskAPI getInstance(String alias) throws SysException {
		synchronized (helpers) {
			ITaskAPI obj = helpers.get(alias);
			if (obj != null) {
				return obj;
			} else {
				TaskAPI o = new TaskAPI(alias);
				helpers.put(alias, o);
				return o;
			}
		}
	}
	
	private final String alias;
	private final Map<Long, Set<String>> classes;
	private final String lookup;

	private Object[] execute(String method, Object[] ps) throws RemoteException, SysException {
  	//Process prc = Process.currentProcess();
  	//String urls = "http://localhost:29901/";
    URL url = null;
    URLConnection con = null;
    try {
      url = new URL(lookup);
      con = url.openConnection();
    } catch (MalformedURLException e) {
      throw new RemoteException(e.getMessage());
    } catch (IOException e) {
      throw new RemoteException(e.getMessage());
    }
    StringBuffer sb_prms = new StringBuffer();
    for (Object prm : ps) {
    	sb_prms = Handler.getElementByObject(sb_prms, prm);
    }
    byte[] b;
    InputStream is;
		try {
			b = sb_prms.toString().getBytes("utf-8");
      //is = new ByteArrayInputStream(b);
		} catch (UnsupportedEncodingException e) {
      throw new SysException(e);
		}
		con.addRequestProperty("ITaskAPI.Alias", alias);
		con.addRequestProperty("ITaskAPI.Method", method);
    con.addRequestProperty("Content-Length", Long.toString(b.length + 2));
    con.setDoInput(true);
    con.setDoOutput(true);
    try {
      OutputStream os = con.getOutputStream();
      os.write(b);
      //int ch;
      //while((ch = is.read()) != -1) {
        //System.out.print((char)ch);
        //os.write(ch);
      //}
      os.write("\r\n".getBytes());
      //System.out.print("\r\n");
    } catch (IOException e) {
      throw new RemoteException(e.getMessage());
    } finally {
      //try {
        //is.close();
      //} catch (IOException e) {
        //e.printStackTrace();
      //}
    }
    //System.out.println("=============Response=============");
    is = null;
    try {
    	/*
    	Map<String, List<String>> flds = con.getHeaderFields();
      for(Map.Entry<String, List<String>> fld : flds.entrySet()) {
        List<String> val = fld.getValue();
        StringBuffer sb = new StringBuffer();
        for(String v : val) {	
          sb.append(v).append("\r\n");
        }
        if (sb.length() > 0)
          sb.setLength(sb.length() - 2);
        String n = fld.getKey();
        //System.out.println(((n == null)? "" : n + ":") + sb.toString());
      }
      */
      int l = con.getContentLength();
      if (l > 2) {
      	byte[] bytes = new byte[l - 2];
        is = con.getInputStream();
      	int len = 0;
      	int ch;
      	while ((len < l - 2) && (ch = is.read()) != -1) {
      		bytes[len] = (byte)(ch & 0xff);
      		len++;
      	}
        StringBuffer sb = new StringBuffer(new String(bytes, "utf-8"));
        Object[] objs = Handler.getObjects(sb);
        return objs;
      }  else if (l < 0) {
        byte[] bytes = new byte[1024*1024 + 1];
        is = con.getInputStream();
        int c = is.read(bytes);
        if (c <= 0 || c == bytes.length)
          return new Object[]{};
        StringBuffer sb = new StringBuffer(new String(bytes, 0, c, "utf-8"));
        Object[] objs = Handler.getObjects(sb);
        return objs;
      }
      return new Object[]{};
    } catch (IOException e) {
    	e.printStackTrace();	
			throw new RemoteException(e.getMessage());
		} finally {
			if (is != null)
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private TaskAPI(String alias) throws SysException {
		this.alias = alias;
		synchronized (prms) {
			Map<String, String> values = prms.get(alias);
			if (values == null)
				throw new SysException("Unregistered TaskAPI by alias '" + alias + "'");
			lookup = values.get("lookup");
		}
		this.classes = new HashMap<Long, Set<String>>();
	}

	public void registerClasses(long subject_id, Set<String> hs)
			throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Set<String> s = new HashSet<String>(hs);
			execute("registerClasses", new Object[]{subject_id, s});
			synchronized(classes) {
				classes.put(subject_id, s);
			}
			return;
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	public String getAlias() throws SysException {
		return alias;
	}

	@Override
	public long getSubject(long old_subject_id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("getSubject", new Object[]{old_subject_id});
			return (Long)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public TaskCI getBlocked(long subject_id, long wait) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("getBlocked", new Object[]{subject_id, wait});
			if (r.length > 0)
				return (TaskCI)(r[0]);
			return null;
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public long setParamObject(long task_id, String name, Object value)
			throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("setParamObject", new Object[]{task_id, name, value});
			return (Long)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public long setParamObject(long task_id, String name, Object value,
			long wait, long calc, long scall) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("setParamObject2", new Object[]{task_id, name, value, wait, calc, scall});
			return (Long)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public long createTask(long subject_id, String cls) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("createTask", new Object[]{subject_id, cls});
			return (Long)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public long createTask(long subject_id, String cls, long parent_task_id)
			throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("createTask2", new Object[]{subject_id, cls, parent_task_id});
			return (Long)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public String getClassName(long id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("getClassName", new Object[]{id});
			return (String)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public long setTaskDT(long id, String dt, String duration,
			String block_duration, Double arch_day) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("setTaskDT", new Object[]{id, dt, duration,
					block_duration, arch_day});
			return (Long)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public long setTaskError(long id, Throwable t) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("setTaskError", new Object[]{id, t});
			return (Long)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public String getParamTask(long id, String name) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("getParamTask", new Object[]{id, name});
			return (String)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getGroupParams(long id, String group)
			throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("getGroupParams", new Object[]{id, group});
			return (Map<String, Object>)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public State getState(long id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("getState", new Object[]{id});
			return (State)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public long setState(long subject_id, long id, State st) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("setState", new Object[]{subject_id, id, st});
			return (Long)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public long setState(long subject_id, long id, State st, Long processor_id)
			throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("setState2", new Object[]{subject_id, id, st, processor_id});
			return (Long)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public long setReady(long subject_id, long id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("setReady", new Object[]{subject_id, id});
			return (Long)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public long setReady(long subject_id, long id, Long processor_id)
			throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("setReady2", new Object[]{subject_id, id, processor_id});
			return (Long)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public long setBlocked(long subject_id, long id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("setBlocked", new Object[]{subject_id, id});
			return (Long)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public long setProcessing(long subject_id, long id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("setProcessing", new Object[]{subject_id, id});
			return (Long)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public long setTimeout(long subject_id, long id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("setTimeout", new Object[]{subject_id, id});
			return (Long)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public long setScheduled(long subject_id, long id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("setScheduled", new Object[]{subject_id, id});
			return (Long)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public long setBreaking(long subject_id, long id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("setBreaking", new Object[]{subject_id, id});
			return (Long)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public long setBroken(long subject_id, long id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("setBroken", new Object[]{subject_id, id});
			return (Long)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public long setCanceled(long subject_id, long id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("setCanceled", new Object[]{subject_id, id});
			return (Long)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public long setDoneErr(long subject_id, long id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("setDoneErr", new Object[]{subject_id, id});
			return (Long)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public long setDoneOk(long subject_id, long id) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("setDoneOk", new Object[]{subject_id, id});
			return (Long)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public State waitFinished(long subject_id, long id, long msec)
			throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("waitFinished", new Object[]{subject_id, id, msec});
			return (State)(r[0]);
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public long log(long subject_id, long id, String info, boolean base)
			throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		System.out.println(this.alias + " : " + subject_id + " : " + id + " :\t" + info);
		if (base == false)
			return 0;
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("log", new Object[]{subject_id, id, info, base});
			return ((Long)(r[0])).longValue();
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

	@Override
	public long log(long subject_id, long id, String info) throws SysException {
		System.out.println(this.alias + " : " + subject_id + " : " + id + " :\t" + info);
		return 0;
	}

	@Override
	public void writeLogToFile(long id, File file, String charset)
			throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			execute("writeLogToFile", new Object[]{id, file, charset});
			return;
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
		
	}

	@Override
	public Object getParam(long id, String fullname) throws SysException {
		//System.out.println("+++ CALL method " + Thread.currentThread().getStackTrace()[1].getMethodName() + ":" + Thread.currentThread().getStackTrace()[1].getLineNumber());
		int attempt = 2;
		while (true)
		try {
			Object[] r = execute("getParam", new Object[]{id, fullname});
			return r[0];
		} catch (RemoteException e) {
			e.printStackTrace();
			attempt--;
			if (attempt <= 0) 
				throw new SysException(e);
		}
	}

}
