package center.task.prm.alg;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.w3c.dom.Element;

import ru.vassaev.core.base.Null;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.io.OutputByteBuffer;
import ru.vassaev.core.types.TimeList;
import ru.vassaev.core.types.TimeList.Type;
import ru.vassaev.core.util.Strings;
import ru.vassaev.core.xml.XMLFileWaiter;
import center.task.CalculateException;
import center.task.Context;
import center.task.prm.Prm;
/**
 * Алгоритм реализует получение ссылки на файл
 * @author Vassaev A.V.
 * @version 1.1 23/02/2013
 */
public final class FileLink extends Merge {
  private Object fill = null;
  private String encode = "UTF-8";
	public FileLink(center.task.prm.Type tp, String owner, Element e) throws SysException {
    super(tp, owner, e, "filename");
    Element f = (Element) Strings.getXMLObject(e, "fill");
    if (f != null)
      fill = Prm.getPrm(f);
    if (e.hasAttribute("encode"))
    	encode = e.getAttribute("encode");
	}

	protected Object calculate(TimeList tl, Context cntx) throws CalculateException {
		try {
	  	tl.addPointTime(Type.START);
	  	String filename = Strings.getString(super.calculate(tl, cntx));
	  	if (filename == null)
        return null;
	  	File f = new File(filename);
	  	if (fill != null) {
		  	Object o = null;
	  		if (f.createNewFile()) {
	  			if (fill instanceof Prm)
						try {
							o = ((Prm)fill).getObject(cntx);
						} catch (SysException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	  			else
	  				o = fill;
	  			if (Null.equ(o))
	  				return f;
	  			if (o instanceof Element)
	  				XMLFileWaiter.putFile((Element)o, f, encode);
	  			else if (o instanceof OutputByteBuffer) {
	  				InputStream is = ((OutputByteBuffer)o).getIS();
	  				FileOutputStream os = null;
	  				try {
		  				os = new FileOutputStream(f.getCanonicalPath()); 
		  				int ch = 0;
	  					while((ch = is.read()) > 0)
	  						os.write(ch);
	  				} catch (IOException e) {
	  					throw new CalculateException(owner, e);
	  				} finally {
	  					if (os != null)
	  						os.close();
	  					is.close();
	  				}
	  			} else if (o instanceof StringBuffer || o instanceof String) {
	  				String sb = o.toString();
	  				FileOutputStream os = null;
	  				try {
		  				os = new FileOutputStream(f.getCanonicalPath());
		  				os.write(sb.getBytes(encode));
	  				} catch (IOException e) {
	  					throw new CalculateException(owner, e);
	  				} finally {
	  					if (os != null)
	  						os.close();
	  				}
	  			}
	  		}
	  	}
	  	return f;
		} catch (IOException e) {
			throw new CalculateException(owner, e);
		} finally {
	  	tl.addPointTime(Type.END);
		}
	}

}
