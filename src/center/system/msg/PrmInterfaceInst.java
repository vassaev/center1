package center.system.msg;

import java.util.HashMap;
import java.util.Map;

import ru.vassaev.core.PrmInterface;
import ru.vassaev.core.base.Null;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.types.StringList;
import ru.vassaev.core.util.Strings;

public class PrmInterfaceInst implements PrmInterface {
	private Map<String, Object> fields = new HashMap<String, Object>();
	public void clearField(String key) throws SysException {
		fields.remove(key);
	}

	public void clearFields() throws SysException {
		fields.clear();
	}

	public String getField(String key) throws SysException {
		return Strings.getString(fields.get(key));
	}

	public StringList getFieldNames() {
		StringList r = new StringList();
		r.addAll(fields.keySet());
		return r;
	}

	public boolean isPresentField(String key) throws SysException {
		return fields.containsKey(key);
	}

	public void setField(String key, String val) throws SysException {
		if (val == null) {
			fields.put(key, Null.getInstance());
		} else {
			fields.put(key, val);
		}
	}

	public byte[] getByteField(String key) throws SysException {
		Object o = fields.get(key);
		if (o == null)
			return null;
		if (o instanceof byte[])
			return (byte[])o;
		return Strings.getString(o).getBytes();
	}

}
