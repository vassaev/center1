package center.task;

import java.io.Serializable;
import java.util.*;

import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import ru.vassaev.core.Expression;
import ru.vassaev.core.util.Strings;
import ru.vassaev.core.exception.SysException;
import center.task.prm.tp.PrmFactory;


/**
 * @author Vassaev A.V.
 * @version 1.0
 */
public class TaskInfo extends ATaskInfo implements Serializable {

	private static final long serialVersionUID = -9220722504188872520L;


	public TaskInfo() throws SysException {
		super();
	}
	public static class MailInfo {
		public MailInfo(Element e) {
			to = Strings.getXMLValue(e, "TO");
			bcc = Strings.getXMLValue(e, "BCC");
			subject = Strings.getXMLValue(e, "SUBJECT");
			body = Strings.getXMLValue(e, "BODY");
			sender = Strings.getXMLValue(e, "SENDER");
			smtphost = Strings.getXMLValue(e, "SMTPHOST");
			smtpport = Strings.getXMLValue(e, "SMTPPORT");
			smtpuser = Strings.getXMLValue(e, "SMTPUSER");
			smtppwd = Strings.getXMLValue(e, "SMTPPWD");
		}

		public String to = "";
		public String bcc = "";
		public String subject = "";
		public String body = "";
		public String sender = "";
		public String smtphost = "";
		public String smtpport = "";
		public String smtpuser = "";
		public String smtppwd = "";
	}

	public static class Cntxt implements java.io.Serializable {
		private static final long serialVersionUID = 8468641133170385673L;
		public enum Time {
			DURING {
			},
			AFTER {
			},
			BOTH {
				public boolean isPartOf(Time x) {
					return x.equals(BOTH);
				}
			};

			public boolean isPartOf(Time x) {
				return (x.equals(BOTH) || this.equals(x));
			}
		}

		public String cls = null;
		public String dt = null;
		public String duration = null;
		public String block_duration = null;
		public Map<String, Object> prms = new TreeMap<String, Object>();
		public Time time = Time.BOTH;
	}

	public static class DNF extends center.system.DNF<Cntxt> {
		private static final long serialVersionUID = 3978682914209979006L;
		public java.util.Set<String> flds = new java.util.HashSet<String>();
		private Cntxt cntx = new Cntxt();

		public Cntxt getContext() {
			return cntx;
		}
	}

	public ArrayList<DNF> dnf = new ArrayList<DNF>();
	public Cntxt _else = new Cntxt();
	public Map<String, Object> mails = new TreeMap<String, Object>();
	public String clsName = null;
	public Class<?> cls = null;

	public NewTaskInfo child;
  public TaskPlanTree task_plan;

  
	public void load(ATaskInfo parent, ATaskInfo extend, Element el) throws SysException {
    super.load(parent, extend, el);
		// Имя класса задачи
		clsName = Strings.getXMLValue(el, "#name");
		if (extend != null) {
			processor = extend.processor;
			owndnf = extend.owndnf;
		}

		// Класс JAVA по выполнению задачи
		String processor_ = Strings.getXMLValue(el, "#processor");
		if (processor_ != null)
			processor = processor_;
    try {
    	ClassLoader CL = ClassLoader.getSystemClassLoader();
      cls = Class.forName(processor, true, CL);
      Object o = cls.newInstance();
      if (!(o instanceof AProcessor))
        throw new SysException("The class by name '" + processor + "' is not AProcessor");
    } catch (ClassNotFoundException e) {
      throw new SysException(e);
    } catch (IllegalAccessException e) {
      throw new SysException(e);
    } catch (InstantiationException e) {
      throw new SysException(e);
    }
		// Загрузить информацию о параметрах
		if (extend != null)
			prms = new TreeMap<String, Object>(extend.prms);
		else
			prms = new TreeMap<String, Object>();
		loadParams(prms, el);
		// Загрузить информацию о рассылке сообщений
		loadMailInfo(el);
		// Загрузить DNF для запуска задачи после выполнения текущей
		loadDNF(el);
		// Условия и параметры дочернего задания
		Element child = (Element) Strings.getXMLObject(el, "child");
		if (child != null) {
			this.child = new NewTaskInfo();
			this.child.load(this, null, child);
		}
    //task_plan = new TaskPlanTree(this);
  }


	/**
	 * Получить список параметров
	 * 
	 * @param prm_
   * @param pnl Element
	 * @throws SysException исключение
	 */
	private void loadParams(Map<String, Object> prm_, Element pnl) throws SysException {
		NodeList nl = pnl.getChildNodes();
    PrmFactory fact = new PrmFactory();
		for (int i = nl.getLength() - 1; i >= 0; i--) {
			Node n = nl.item(i);
			if (n.getNodeType() != Node.ELEMENT_NODE)
				continue;
      Element e = (Element) n;
      String nn = n.getNodeName();
			if (nn.equals("param")) {
				n = fact.getParamDBTS(e); // Поддержка параметров DBTS
      } else if (nn.equals("prm")) {
        String name = Strings.getXMLValue(e, "#name");
        String grp = Strings.getXMLValue(e, "#grp");
        String fn = (grp == null || grp.length() == 0) ? name : grp + "/"
            + name;
        prm_.put(fn, fact.getParam(e, this));
      }
		}

	}
                                           
	/**
	 * Получить условия информирования о результатах выполнения задания
	 * 
	 * @param pnl
	 *          Element
	 */
	private void loadMailInfo(Element pnl) {
		Element emi = (Element) Strings.getXMLObject(pnl, "mailinfo[0]");
		if (emi != null) {
			Element a;
			State[] states = State.values();
			for (int i = 0; i < states.length; i++) {
				String n = states[i].name();
				a = (Element) Strings.getXMLObject(emi, n + "[0]");
				if (a != null) {
					mails.put(n, new MailInfo(a));
				}
			}
		}
	}

	private String prefix = "$$";
	private String suffix = null;

	/**
	 * Получить dnf для выбора следующей задачи на основе данной
	 * 
	 * @param pnl
	 *          Element
	 * @throws SysException 
	 */
	private void loadDNF(Element pnl) throws SysException {
		Element ednf = (Element) Strings.getXMLObject(pnl, "dnf[0]");
		if (ednf != null) {
			suffix = Strings.getXMLValue(ednf, "#suffix");
			if ((suffix != null) && (suffix.trim().equals("")))
				suffix = null;
			prefix = Strings.getXMLValue(ednf, "#prefix");
			if ((prefix == null) || (prefix.trim().equals("")))
				prefix = "$$";
			NodeList nl = ednf.getChildNodes();
			for (int i = nl.getLength() - 1; i >= 0; i--) {
				Node n = nl.item(i);
				if (n.getNodeType() == Node.ELEMENT_NODE) {
					Element e = (Element) n;
					String nm = e.getNodeName();
					if (nm.equals("or")) {
						DNF o = new DNF();
						dnf.add(o);

						o.cntx.cls = Strings.getXMLValue(e, "#cls");
						o.cntx.dt = Strings.getXMLValue(e, "#dt");
						o.cntx.duration = Strings.getXMLValue(e, "#duration");
						o.cntx.block_duration = Strings.getXMLValue(e, "#block_duration");
						try {
							o.cntx.time = Cntxt.Time.valueOf(Strings
									.getXMLValue(e, "#time"));
						} catch (RuntimeException ex) {
							o.cntx.time = Cntxt.Time.BOTH;
						} catch (Exception ex) {
							o.cntx.time = Cntxt.Time.BOTH;
						}
						NodeList nl_and = e.getChildNodes();

						for (int j = nl_and.getLength() - 1; j >= 0; j--) {
							Node n_and = nl_and.item(j);
							if (n_and.getNodeType() != Node.ELEMENT_NODE)
								continue;
							if (!n_and.getNodeName().equals("and"))
								continue;
							Element e_and = (Element) n_and;
							center.system.DNF.Conjunct or = new center.system.DNF.Conjunct();
							o.add(or);
							{
								NodeList nl_e = e_and.getChildNodes();
								for (int k = nl_e.getLength() - 1; k >= 0; k--) {
									Node n_e = nl_e.item(k);
									if (n_e.getNodeType() != Node.ELEMENT_NODE)
										continue;
									if (!n_e.getNodeName().equals("e"))
										continue;
									Element e_e = (Element) n_e;
									String tp = Strings.getXMLValue(e_e, "#type");
									Expression ex = new Expression(e_e.getTextContent(), prefix);
									Iterator<String> prs = ex.getParams();
									if ("ordinal".equals(tp)) {
										while (prs.hasNext()) {
											String nmpr = prs.next();
											String xFld = nmpr
													+ ((suffix == null) ? "" : (prefix + suffix));
											ex.setValue(nmpr, "\"" + xFld + "\"");
											o.flds.add(xFld);
										}
									} else {
										while (prs.hasNext()) {
											String nmpr = prs.next();
											ex.setValue(nmpr, "\"" + nmpr + "\"");
											o.flds.add(nmpr);
										}
									}
									or.add(ex.toString());
								}
							}
						}
						loadParams(o.cntx.prms, e);
					} else if (nm.equals("else")) {
						_else.cls = Strings.getXMLValue(e, "#cls");
						_else.dt = Strings.getXMLValue(e, "#dt");
						_else.duration = Strings.getXMLValue(e, "#duration");
						_else.block_duration = Strings.getXMLValue(e, "#block_duration");
						try {
							_else.time = Cntxt.Time
									.valueOf(Strings.getXMLValue(e, "#time"));
						} catch (RuntimeException ex) {
							_else.time = Cntxt.Time.BOTH;
						} catch (Exception ex) {
							_else.time = Cntxt.Time.BOTH;
						}
						loadParams(_else.prms, e);
					}
				}
			}
		}
	}

	/**
	 * Получить список DNF
	 * @throws SysException
	 */
	public synchronized ArrayList<DNF> getDNF(Context cntx) throws SysException {
		ArrayList<DNF> dnfs = new ArrayList<DNF>();
		for (int i = 0; i < dnf.size(); i++) {
			DNF d = (DNF) dnf.get(i).clone();
			Object[] flds = d.flds.toArray();
			d.flds.clear();
			for (int j = flds.length - 1; j >= 0; j--) {
				Expression ex = new Expression(flds[j].toString(), prefix);
				Iterator<String> ps = ex.getParams();
				while (ps.hasNext()) {
					String k = ps.next();
					Object o = cntx.getPrmByFullName(k);
					if (o != null) {
						ex.setValue(k, o.toString());
					} else
						ex.setValue(k, "");
				}
				d.flds.add(ex.toString());
			}

			for (int j = d.getLength() - 1; j >= 0; j--) {
				Object o = d.getItem(j);
				Expression ex = new Expression(o.toString(), prefix);
				Iterator<String> ps = ex.getParams();
				while (ps.hasNext()) {
					String k = ps.next();
					o = cntx.getPrmByFullName(k);
					if (o != null) {
						ex.setValue(k, o.toString());
					} else
						ex.setValue(k, "");
				}
				d.setItem(j, ex.toString());
			}
			dnfs.add(d);
		}
		return dnfs;
	}
  public final AProcessor newProcessorInstance() throws SysException {
    try {
      return (AProcessor) cls.newInstance();
    } catch (IllegalAccessException e) {
      throw new SysException(e);
    } catch (InstantiationException e) {
      throw new SysException(e);
    }
  }
}
