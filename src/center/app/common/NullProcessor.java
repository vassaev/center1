package center.app.common;

import java.util.Set;

import ru.vassaev.core.exception.SysException;

import center.task.AProcessor;
import center.task.Context;
import center.task.State;

/**
 * Процессор, который ничего не выполняет
 *
 * @author Vassaev A.V.
 * @version 1.0
 */
public final class NullProcessor extends AProcessor {
  public State process(Context cntx) throws SysException {
    return State.DONE_OK;
  }

  public NullProcessor() {
    super();
  }

  public Set<String> dependentOn() {
    return null;
  }

  public Set<String> loopDependentOn() {
    return null;
  }
}
