package center.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.*;

import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.db.RsField;

/**
 * @author Vassaev A.V.
 * @version 1.0
 */
public final class Select extends Statement {

  public static class Row {
    protected static long versionUID;

    public final long getVersionUID() {
      return versionUID;
    }
  }

  public static Select getInstance(Connection con, String query) throws
          SysException, SQLException {
    return new Select(con, query);
  }

  public void destroy() throws SQLException {
    st.close();
  }

  public Select(Connection con, String query)
          throws SQLException, SysException {
    super(con);
    String s = "select ";
    if (!query.trim().substring(0, 7).equalsIgnoreCase(s)) {
      throw new SysException("The query is not correct.");
    }
    if (st != null)
      if (!st.isClosed())
        st.close();
    st = con.prepareStatement(query);
    try {
      flds = tp.getFields(st);
    } catch (Throwable ex) {
      flds = new RsField[0];
    }
    if (flds.length == 0) {
      ResultSet rs = st.executeQuery();
      rs.close();
      try {
        flds = tp.getFields(st);
      } catch (Throwable ex) {
        flds = new RsField[0];
      }
      st.clearWarnings();
      st.clearParameters();
      for (int i = 0; i < flds.length; i++) {
        fldsh.put(flds[i].FIELD_NAME, flds[i].INDEX);
      }
    }
  }

//    protected Hashtable<String,
//            Integer> fldsh = new Hashtable<String, Integer>();
  protected Map<String, Object> fldsh = new TreeMap<String, Object>();

  public boolean isFieldExist(String name) {
    return (prmsh.get(name) != null);
  }

  public String getDeclareDataClassBody() {
    return getDeclareDataClassBody(false);
  }

  public String getDeclareDataClassBody(boolean withRow) {
    StringBuffer sb = new StringBuffer();
    //sb.append("{\n");
    for (int i = 0; i < flds.length; i++) {
      String fld = tp.getDeclareFieldClass(flds[i]).replaceAll("\n",
                                                               "\n\t");
      sb.append('\t').append(fld).append(";\n");
    }
    if (withRow) {
      sb.append("\tstatic { versionUID = ")
              .append(System.currentTimeMillis()).append("L; }\n");
    }
    //sb.append("}");
    return sb.toString();
  }

  public String getDeclareHelperClassBody(String dataClass, boolean withRow) {
    StringBuffer sb = new StringBuffer();
    //sb.append("{\n");
    sb.append('\t')
            .append("public void loadFromRs(")
            .append(ru.vassaev.core.db.Types.class.getCanonicalName()).append(" tp, ")
            .append(ResultSet.class.getCanonicalName()).append(" rs, ")
            .append("Object row) \n" +
                    "\t\tthrows ").append(SysException.class.getCanonicalName())
            .append(" {\n")
            .append("\t\t").append(dataClass).append(" dt = (")
            .append(dataClass).append(") row;\n");
    sb.append("\t\ttry {\n");
    ArrayList<RsField> oFlds = tp.getOLFlds(flds);
    int l = flds.length;
    Set<Class> exsl = new HashSet<Class>();
    for (int i = 0; i < l; i++) {
      RsField fld = oFlds.get(i);
      Method m = tp.getMethodLoadFromRs(fld);
      Class[] exs = m.getExceptionTypes();
      for (int j = exs.length - 1; j >= 0; j--)
        exsl.add(exs[j]);

      sb.append("\t\t\tdt.").append(fld.FIELD_NAME).append(" = ")
              .append("tp.")
              .append(m.getName()).append("(rs, ").append(fld.INDEX)
              .append(");\n");
    }
    sb.append("\t\t} catch (")
            .append(SQLException.class.getCanonicalName())
            .append(" ex) {\n")
            .append("\t\t\tthrow new ")
            .append(SysException.class.getCanonicalName())
            .append("(ex);\n");

    for (Iterator<Class> ic = exsl.iterator(); ic.hasNext();) {
      Class c = ic.next();
      if (!c.equals(SQLException.class))
        sb.append("\t\t} catch (")
                .append(c.getCanonicalName())
                .append(" ex) {\n")
                .append("\t\t\tthrow new ")
                .append(SysException.class.getCanonicalName())
                .append("(ex);\n");
    }

    sb.append("\t\t}\n");
    sb.append('\t').append("}\n");
    if (withRow) {
      sb.append("\tstatic { versionUID = ")
              .append(System.currentTimeMillis()).append("L; }\n");
    }
    //sb.append("}");
    return sb.toString();
  }

  public ResultSet executeQuery() throws SQLException {
    st.execute();
    ResultSet rs = st.getResultSet();
    return rs;
  }

  public ArrayList<String> getFieldNames() {
    Iterator<String> i = fldsh.keySet().iterator();
    ArrayList<String> v = new ArrayList<String>();
    while (i.hasNext())
      v.add(i.next());
    return v;
  }
}
