package center.task.prm.alg;

import org.w3c.dom.Element;

import center.task.CalculateException;
import center.task.Context;
import center.task.prm.Alg;
import center.task.prm.Recipient;
import ru.vassaev.core.types.TimeList;
import ru.vassaev.core.types.TimeList.Type;
import ru.vassaev.core.base.Null;
import ru.vassaev.core.exception.SysRuntimeException;

import java.util.Set;


/**
 * Параметр, имеющий значение только внутри потока
 * Используется в задачах, многопоточной обработки
 *
 * @author Vassaev A.V.
 */
public final class TimeThread extends Alg implements Recipient {

  private String name;

  public TimeThread(center.task.prm.Type tp, String owner, String name) {
    super(tp, owner);
    if (name == null)
      throw new SysRuntimeException("The param of theard's time hasn't name");
    this.name = name;
  }

  public TimeThread(center.task.prm.Type tp, String owner, Element e) {
    super(tp, owner);
    name = e.getTextContent();
  }

  public String getName() {
    return name;
  }

  public boolean adobt(Object val) {
    ru.vassaev.core.thread.Process.currentProcess().regResourceName(val, name);
    return true;
  }

  public final Object calculate(TimeList tl, Context cntx)  throws CalculateException {
  	try {
  		tl.addPointTime(Type.START);
      Thread curr = ru.vassaev.core.thread.Process.currentThread();
      if (curr instanceof ru.vassaev.core.thread.Process) {
        ru.vassaev.core.thread.Process cur = (ru.vassaev.core.thread.Process) curr;
        return cur.getResourceByName(name);
      }
      return Null.NULL;
  	} finally {
  		tl.addPointTime(Type.END);
  	}
  }

  public Set<String> depedentOn() {
    return null;
  }

  public Set<String> depedentOnParent() {
    return null; 
  }
}