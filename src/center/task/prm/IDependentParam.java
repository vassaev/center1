package center.task.prm;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import ru.vassaev.core.TypeOutput;
import ru.vassaev.core.thread.LogEvent;
import ru.vassaev.core.thread.LogInfo;
import ru.vassaev.core.thread.LogLevel;
import ru.vassaev.core.types.TimeList;

import center.task.CalculateException;
import center.task.Context;

/**
 * Зависимый параметр
 * @author Vassaev A.V.
 * @version 1.0
 */
public abstract class IDependentParam implements LogInfo {
  // Уровень логирования
	private Object sync_log = new Object();
  private Map<LogEvent, LogLevel> log = null;
  private LogEvent cur = null;
  public abstract boolean getLog();
  public abstract boolean getTrace();
  public abstract Object getFromSection();
  public final void setLogLevel(LogEvent le, LogLevel ll) {
  	if (le == null || ll == null)
  		return;
  	synchronized(sync_log) {
    	if (log == null) 
    		log = new HashMap<LogEvent, LogLevel>();
    	log.put(le, ll);
  	}
  }
  public final LogLevel getLogLevel(LogEvent le) {
  	synchronized(sync_log) {
  		if (log == null || le == null)
  			return null;
  		return log.get(le);
  	}
  }
  public final LogEvent setEvent(LogEvent le) {
  	synchronized(sync_log) {
    	if ((le != null && le.equals(cur)) || le == cur)
    		return le;
    	LogEvent oldle = cur;
    	cur = le;
    	LogLevel ll = getLogLevel(le);
    	setLogLevel(TypeOutput.INFO, ll);
    	return oldle;
  	}
  	
  }
	private LogLevel lve;
	private LogLevel lvi;
	private LogLevel lvw;
	public synchronized final void setLogLevel(TypeOutput tp, LogLevel lv) {
		if (tp == null || TypeOutput.INFO.equals(tp))
			this.lvi = lv;
		else if (TypeOutput.ERROR.equals(tp))
			this.lve = lv;
		else
			this.lvw = lv;
	}
	public synchronized final LogLevel getLogLevel(TypeOutput tp) {
		if (tp == null || TypeOutput.INFO.equals(tp))
			return this.lvi;
		else if (TypeOutput.ERROR.equals(tp))
			return this.lve;
		else
			return this.lvw;
	}
  /**
   * Функция расчёта значения параметра
   * @param tl - статистика контекста расчёта
   * @param cntx - контекст расчёта
   * @return результат расчёта
   * @throws CalculateException 
   */
  public abstract Object calculate(TimeList tl, Context cntx) throws CalculateException;
  /**
   * Аттрибут кэшируемости
   * @return true - если параметр должен кэшироваться
   */
  public abstract boolean isCache();
  /**
   * Аттрибут отмены чтения из базы данных
   * @return true - если параметр только из памяти
   */
  public abstract boolean isNonDB();
  /**
   * Получить значение параметра
   * Учитывает атрибуты параметра:
   * @param cntx - текущий контекст
   * @return Значение параметра
   * @throws CalculateException 
   */
  public abstract Object getValue(Context cntx) throws CalculateException;
  /** Тип зависимости от параметра
   * MANDATORY - зависит при любых условиях (обязательно)
   * CONDITION - зависит от параметра "при условии" (условие можно вычисляется по графу)
   */
  public enum DependentsType {MANDATORY, CONDITION}
  /**
   * Получить список параметров от которых зависит данный параметр
   * @return список имен
   */
  public abstract Set<String> depedentOn();
  public abstract Set<String> depedentOnParent();
  
  /**
   * Получить "глобальное" имя параметра, уникальное в пределах данного контекста
   * @return уникальное имя
   */
  public abstract String getGLName();
  /**
   * Получить "полное" имя параметра
   * @return полное имя
   */
  public abstract String getFullName();
  /**
   * Выполнить копирование
   * @param cntx - текущий контекст
   * @param v - значение параметра
   */
  public abstract void executeCopyTo(Context cntx, Object v);
  /**
   * Если признак установлен, то параметр не сохраняется 
   * в базе данных задачи при создании задачи
   * @return
   */
  public abstract boolean isNoSave();
}
