package center.task.api.http;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sun.net.httpserver.HttpServer;

import center.task.api.ATaskAPI;
import center.task.api.ITaskAPI;
import ru.vassaev.core.container.AppInfo;
import ru.vassaev.core.container.ApplicationManager;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.thread.PoolThread;
import ru.vassaev.core.thread.Process;
import ru.vassaev.core.thread.Processed;
import ru.vassaev.core.util.Strings;

public class Server implements Processed<AppInfo> {

	public Server() {
		super();
	}

	private Map<String, ITaskAPI> api = new HashMap<String, ITaskAPI>();

	private ITaskAPI register(String alias, String cls_native,
			String alias_native) throws SysException {
		synchronized (api) {
			ITaskAPI ta = api.get(alias);
			if (ta == null) {
				ta = ATaskAPI.getInstance(cls_native, center.task.api.mem.TaskAPI.class,
							alias_native);
				if (ta == null)
					return null;
				api.put(alias, ta);
			}
			return ta;
		}
	}
	
	protected ITaskAPI getTaskAPI (String alias) {
		synchronized (api) {
			return api.get(alias);
		}
	}

	@Override
	public void processing(AppInfo ai) throws Throwable {
    Element el = ai.getDocInfo();
    NodeList children = el.getChildNodes();

    port = Strings.parseIntegerNvl(el.getAttribute("port"), 29900);
		System.out.println("port = " + port);
    
    int l = children.getLength();
    Node n;
    Element e;
    String alias;
    String alias_native;
    String cls_native;
		System.out.println("Reading TaskAPI elements...");
    for (int i = 0; i < l; i++) {
    	n = children.item(i);
    	if (!(n instanceof Element))
    		continue;
    	e = (Element) n;
    	if (e.getTagName().equals("TaskAPI")) {
    		alias = e.getAttribute("alias");
				cls_native = e.getAttribute("cls_native");
				alias_native = e.getAttribute("alias_native");
				System.out.print("Registering of TaskAPI (alias = " 
						+ alias + ", cls_native = " + cls_native + ", alias_native = " + alias_native + ") ... ");
    		ITaskAPI ta = register(alias
    				, cls_native
    				, alias_native);
				System.out.print((ta == null) ? "ERROR" : "OK");
    	}
    	
    }
    String hostname = "localhost";
    InetAddress addr = InetAddress.getLocalHost();
    addr.getAddress();
    hostname = addr.getHostName();
    
    Process prc = Process.currentProcess();
    String poolProcessName = el.getAttribute("process_pool");
    executor = (PoolThread) ApplicationManager.getPoolByName(poolProcessName, Process.class);
    server = HttpServer.create(new InetSocketAddress(port), 0);
    server.createContext("/", new HttpHandler(prc, this));
    server.setExecutor(executor);
    server.start();
		
		System.out.println("Server (http://" + hostname + ":" + port + "/" + ") is ready...");
		
		try {
      while (true) {
        if (prc.isWillBreak())
          return;
        Thread.sleep(2000);//!!!
      }
    } catch (InterruptedException e1) {
      throw new SysException(e1);
    } finally {
      if (server != null)
        server.stop(0);
    }

	}

  private HttpServer server = null;
  private Integer port;
  private PoolThread executor = null;
  /*
  private SSLParameters sslp = null;
  private boolean https = false;
  private String story_file = null;
  private char[] story_pwd = null;
  private char[] key_pwd = null;
  //*/
}
