package center.db.prcs;

import center.system.*;
import center.db.*;
import ru.vassaev.core.util.Strings;

import java.util.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.io.*;

import ru.vassaev.core.base.Null;
import ru.vassaev.core.db.Manager;
import ru.vassaev.core.exception.SysException;

public class RowFileToDBUpdater extends Processor {
  public RowFileToDBUpdater() {
    Properties props = getInitProperties();
    Null n = Null.getInstance();
    props.put("update", n);
    props.put("connect", n);
    props.put("columns", n);
    props.put("syschar", n);
    props.put("delimiter", n);
    props = getStepProperties();
    props.put("charset", "UTF-8");
    props.put("file", n);
  }

  protected StatementNew upd = null;
  protected Connection con = null;
  protected String conName = null;
  protected char sysch = '\\';
  protected char delch = ';';
  protected String sql = null;
  protected Map<String, Integer> names = new HashMap<String, Integer>();


  protected void initialize() throws SysException {
    Properties props = getInitProperties();

    conName = props.getProperty("connect");
    sql = props.getProperty("update");

    String syschar = props.getProperty("syschar");
    if ((syschar == null) || (syschar.length() == 0))
      sysch = '\\';
    else
      sysch = syschar.charAt(0);
    String delimiter = props.getProperty("delimiter");
    if ((delimiter == null) || (delimiter.length() == 0))
      delch = ';';
    else
      delch = delimiter.charAt(0);
    String[] names = Strings.parseXVSLine(props.getProperty("columns"), sysch, delch);
    for (int i = 0; i < names.length; i++) {
      this.names.put(names[i], i);
    }

    upd = new StatementNew();
    upd.prepare(conName, sql);
  }

  protected boolean execute() throws SysException {
    con = Manager.getConnection(conName);
    try {

      Properties props = getStepProperties();
      String charset = props.getProperty("charset");
      Object file = props.get("file");
      java.io.Reader fr = null;
      if (file instanceof String) {
        fr = new InputStreamReader(new FileInputStream((String) file), charset);
      } else if (file instanceof InputStream) {
        fr = new InputStreamReader((InputStream) file, charset);
      } else if (file instanceof java.io.Reader) {
        fr = (java.io.Reader) file;
      } else
        throw new SysException("Unsupported type of parameter ('file')");
      String[] vs;
      ArrayList<String> prms = upd.getParamNames();
      int j = 0;
      PreparedStatement st = upd.getStatement(con);
      try {
        while ((vs = Strings.parseXVSLine(fr, sysch, delch)).length > 0) {
          for (int i = prms.size() - 1; i >= 0; i--) {
            String name = prms.get(i);
            Integer ind = names.get(name);
            if ((ind != null) && (vs.length > ind)) {
              upd.setParam(st, name, vs[ind], java.sql.Types.VARCHAR);
            } else {
              upd.setParam(st, name, null, java.sql.Types.VARCHAR);
            }
          }
          st.execute();
          j++;
          if (j % 10 == 0) {
            con.commit();
          }
        }
        con.commit();
      } finally {
        upd.freeStatement(st);
      }
      return true;
    } catch (java.sql.SQLException ex) {
      throw new SysException(ex);
    } catch (IOException ex) {
      throw new SysException(ex);
    } finally {
      Manager.freeConnection(con);
    }
  }

  public void destroy() throws SysException {
    upd.close();
  }
}
