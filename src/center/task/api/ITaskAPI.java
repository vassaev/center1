package center.task.api;

import java.util.Map;
import java.util.Set;
import java.io.File;

import center.task.State;
import ru.vassaev.core.exception.SysException;

/**
 * API по взаимодействию с заданием
 * Соответствующий UML описан
 * @author Vassaev A.V.
 * @version 1.0
 */
public interface ITaskAPI {
  /**
   * Зарегистрировать список классов, которые может обслуживать
   * данный поток выполнения
   *
   * @throws SysException - ошибка регистрации
   */
	public void registerClasses(long subject_id, Set<String> hs) throws SysException;
  public String getAlias()throws SysException;
/**
 * Получить идентификатор субъекта
 */
public long getSubject(long old_subject_id) throws SysException;
public long setParamObject(long task_id, String name, Object value) throws SysException;
public long setParamObject(long task_id, String name, Object value, long wait, long calc, long scall) throws SysException;

/**
 * Создание задания в состояние CREATED
 * @return Identity - идентификатор задания > 0
 */
public long createTask(long subject_id, String cls) throws SysException;
public long createTask(long subject_id, String cls, long parent_task_id) throws SysException;

/**
 *  Получить имя класса задания
 */
public String getClassName(long id) throws SysException;

/**
 *  Проверить задачу на соответствие DNF
 */
/*
public boolean checkDNF(long id, DNF dnf, Object[] flds)
        throws SysException;
//*/
/**
 * Установить время начала выполнения задания и длительность ожидания
 */
public long setTaskDT(long id, String dt, String duration, String block_duration, Double arch_day) throws SysException;

/**
 * Установить параметр задания
 */
public long setTaskError(long id, Throwable e) throws
	SysException;
/**
 * Получить параметр задания
 */
public String getParamTask(long id, String name) throws SysException;

public Map<String, Object> getGroupParams(long id, String group) throws
        SysException;
/*
public Reader getTaskParamLong(long id, String name) throws
        SysException;
//*/
/**
 * Получить текущее состояние задания
 */
public State getState(long id) throws SysException;

/**
 * Установить текущее состояние задания
 */
public long setState(long subject_id, long id, State st) throws SysException;
public long setState(long subject_id, long id, State st, Long processor_id) throws SysException;

/**
 * Установить текущее состояние задания в READY
 */
public long setReady(long subject_id, long id) throws SysException;
public long setReady(long subject_id, long id, Long processor_id) throws SysException;

/**
 * Установить текущее состояние задания в BLOCKED
 */
public long setBlocked(long subject_id, long id) throws SysException;
/**
 * Получить задание
 * @param subject_id
 * @param wait - период ожидания задания в секундах
 * @return 
 * @throws SysException
 */
public TaskCI getBlocked(long subject_id, long wait) throws SysException;

/**
 * Установить текущее состояние задания в PROCESSING
 */
public long setProcessing(long subject_id, long id) throws SysException;

public long setTimeout(long subject_id, long id) throws SysException;

public long setScheduled(long subject_id, long id) throws SysException;
/**
 * Установить текущее состояние задания в BREAKING
 */
public long setBreaking(long subject_id, long id) throws SysException;

/**
 * Установить текущее состояние задания в BROKEN
 */
public long setBroken(long subject_id, long id) throws SysException;

/**
 * Установить текущее состояние задания в CANCELED
 */
public long setCanceled(long subject_id, long id) throws SysException;

/**
 * Установить текущее состояние задания в DONE_ERR
 */
public long setDoneErr(long subject_id, long id) throws SysException;

/**
 * Установить текущее состояние задания в DONE_OK
 */
public long setDoneOk(long subject_id, long id) throws SysException;

/**
 * Ожидание завершения задания
 */
public State waitFinished(long subject_id, long id, long msec) throws SysException;

/**
 * Записать информацию по заданию в лог
 */
public long log(long subject_id, long id, String info, boolean base) throws SysException;
public long log(long subject_id, long id, String info) throws SysException;

/**
 * Записать лог в файл
 */
public void writeLogToFile(long id, File file, String charset) throws SysException;
public Object getParam(long id, String fullname) throws SysException;
}
