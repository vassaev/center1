package center.format;

import javax.swing.table.TableModel;

/**
 * Интерфейс преобразования данных
 * @author Vassaev A.V.
 * @version 1.0
 */
public interface IRowFormat {
    /**
     * Форматирует данные из таблицы в строку
     * @param tm TableModel
     * @param row int
     * @return String
     */
    public String format(TableModel tm, int row);

    /**
     * Загружает данные из строки в таблицу
     * @param tm TableModel
     * @param row int
     * @return String
     */
    public void load(TableModel tm, int row, String str);

    /**
     * Форматирует данные из таблицы в строку, если задан список полей таблицы
     * @param tm TableModel
     * @param row int
     * @return String
     */
    public String format(TableModel tm, int row, int[] clmns);

    /**
     * Загружает данные из строки в таблицу, если задано соответствие
     * между порядком загружаемых полей и порядком полей в таблице
     * @param tm TableModel
     * @param row int
     * @return String
     */
    public void load(TableModel tm, int row, String str, int[] clmns);
}
