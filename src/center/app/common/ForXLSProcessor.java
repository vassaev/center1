package center.app.common;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Set;

import ru.vassaev.core.base.Null;
import ru.vassaev.core.exception.SysException;

import center.task.AProcessor;
import center.task.Context;
import center.task.Record;
import center.task.State;
import center.task.TaskException;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;

/**
 * Выполнение update для каждой строки xls файла по параметрам:
 * SHEET, ROW, CLMN_n
 */
public class ForXLSProcessor extends AProcessor {
	private class ResultSet {
		Workbook wb;
		Sheet[] sheets;
		int current_sheet;
		int current_row;
		int mcsl;
		Record cr = new Record();
		ResultSet(Workbook wb) {
			this.wb = wb;
    	sheets = wb.getSheets();
    	current_sheet = 0;
    	current_row = 0;
    	mcsl = 0;
		}
		
		synchronized boolean next() {
			while (current_sheet < sheets.length) {
				Sheet s = sheets[current_sheet];
				if (current_row == s.getRows()) {
					current_sheet ++;
					current_row = 0;
				} else try {
					cr.clear();
        	Cell[] cs = s.getRow(current_row);
        	cr.setObject("SHEET", current_sheet);
        	cr.setObject("SHEET_NAME", s.getName());
        	cr.setObject("ROW", current_row);
          //Загрузка строки
          for (int k = 0; k < cs.length; k++) 
          	cr.setObject("CLMN_" + (k+1), cs[k].getContents());
          for (int k = cs.length; k < mcsl; k++) 
          	cr.setObject("CLMN_" + (k+1), Null.NULL);
        	if (mcsl < cs.length)
        		mcsl = cs.length;
					return true;
				} finally {
					current_row++;
				}
			}
      return false;
		}
		
		void close() {
			wb.close();
		}
		Record getRow() {
			return (Record)cr.clone();
		}
	}
  private String file;
  private String language;
  private String country;
  private WorkbookSettings ws;
  private Workbook workbook;
  public State paramsValidateAndPrepare(Context cntx) throws SysException, TaskException {
    file = cntx.getPrmString("file");
    language = cntx.getPrmNvl("language", "ru");
    country = cntx.getPrmNvl("country", "RU");
    if (file == null)
    	throw new TaskException(State.DONE_ERR, "Param by name 'file' isn't set");
    File x = new File(file);
    if (!x.exists())
      throw new TaskException(State.DONE_ERR, "There is no file by name "+ file);
    ws = new WorkbookSettings();
    ws.setLocale(new Locale(language, country));

    workbook = null;
    try {
      workbook = Workbook.getWorkbook(x, ws);
    } catch (IOException e) {
      throw new TaskException(State.DONE_ERR, e);
    } catch (BiffException e) {
      throw new TaskException(State.DONE_ERR, e);
    }
  	return null;
	}
  public State process(Context cntx) throws SysException, TaskException {
		State st = paramsValidateAndPrepare(cntx);
		if (st != null)
			return st;
    ru.vassaev.core.thread.Process cur = ru.vassaev.core.thread.Process.currentProcess(); 
    ResultSet rs = new ResultSet(workbook);
    try {
      long rownum = 0;
      boolean b;
      if ((b = rs.next())) {
		    cur.regResourceName(true, "row@first");
  			do {
  				Record r = rs.getRow();
  				Set<String> names = r.getNames();
  				for (String name : names) {
  			  	Object o = r.getObject(name);
  			  	if (o == null)
  			  		o = Null.NULL;
  			    cur.regResourceName(o, "row." + name);
  			  }
  		    cur.regResourceName(r, "row");
  				rownum = rownum + 1;
  		    cur.regResourceName(rownum, "row@id");
  				b = rs.next();
  				if (!b)
    		    cur.regResourceName(true, "row@last");
  				cntx.getPrmByFullName("update");// Расчитать параметр
  		    cur.regResourceName(false, "row@first");
  			} while (b);
      } else {
		    cur.regResourceName(true, "row@notfound");
		    cntx.getPrmByFullName("update_notfound");// Расчитать параметр
      }
    } finally {
    	rs.close();
    }
    return State.DONE_OK;
  }

	@Override
	public Set<String> dependentOn() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<String> loopDependentOn() {
		// TODO Auto-generated method stub
		return null;
	}
}
