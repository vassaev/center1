package center.task.prm.alg;

import center.task.prm.Alg;
import center.task.Context;
import ru.vassaev.core.types.TimeList;
import ru.vassaev.core.types.TimeList.Type;

import java.util.Set;

import org.w3c.dom.Element;

/**
 * Значение XML
 * @author Vassaev A.V.
 * @version 1.0
 */
public final class XMLValue extends Alg {
  private final Element value;
  public XMLValue(center.task.prm.Type tp, String owner, Element value) {
    super(tp, owner);
    if (value == null)
      throw new NullPointerException();
    //this.value = value;
    synchronized(value) {
      this.value = (Element)value.cloneNode(true);
    }
  }

  protected Object calculate(TimeList tl, Context cntx) {
  	try {
  		tl.addPointTime(Type.START);
  	    synchronized(value) {
  		  return value.cloneNode(true);
  	    }
  	} finally {
  		tl.addPointTime(Type.END);
  	}
  }

  public Set<String> depedentOn() {
    return null;
  }

  public Set<String> depedentOnParent() {
    return null;
  }
}
