package center.task.prm.alg;

import java.util.Set;

import org.w3c.dom.Element;

import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.types.TimeList;
import ru.vassaev.core.types.TimeList.Type;
import center.task.AProcessor;
import center.task.ATaskInfo;
import center.task.CalculateException;
import center.task.Context;
import center.task.MemTaskInfo;
import center.task.State;
import center.task.TimeState;
import center.task.prm.Alg;

public class TaskProcessor extends Alg {
  private MemTaskInfo tsk;

  public TaskProcessor(center.task.prm.Type tp, String owner, Element e, ATaskInfo parent) throws SysException {
    super(tp, owner);
    tsk = new MemTaskInfo();
    tsk.load(parent, e);
  }

  protected Object calculate(TimeList tl, Context cntx) throws CalculateException {
    try {
    	tl.addPointTime(Type.START);
    	Context res = new Context(cntx, cntx.id_subject, 0, cntx.ta, tsk);
      AProcessor po = tsk.newProcessorInstance(res);
			try {
        res.info.calculate(TimeState.getInstance(TimeState.Time.before, State.PROCESSING), res);
			} catch (Throwable e) {
				e.printStackTrace();
			}
			State st;
			try {
				st = po.process(res);
				res.setPrmByFullName("tsk", "STATUS_ID", st, false);
        res.info.calculate(TimeState.getInstance(TimeState.Time.after, State.PROCESSING), res);
			} catch (Throwable e) {
				st = State.DONE_ERR;
				res.setPrmByFullName("tsk", "STATUS_ID", st, false);
				throw new CalculateException(owner, e);
			} 
			return res;
    } catch (SysException e) {
      throw new CalculateException(owner, e);
    } finally {
    	tl.addPointTime(Type.END);
    }
  }

  public Set<String> depedentOn() {
    return null;
  }

  public Set<String> depedentOnParent() {
    return null;
  }
}