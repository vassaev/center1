package center.task;

import java.sql.*;

import org.w3c.dom.*;

import ru.vassaev.core.thread.Processed;
import ru.vassaev.core.util.*;
import ru.vassaev.core.container.AppInfo;
import ru.vassaev.core.db.Manager;
import ru.vassaev.core.exception.SysException;

import center.task.NewTaskInfo;
import center.task.api.ITaskAPI;
import center.task.api.db.TaskAPI;

/**
 * Зарегистрировать задание
 * 
 * @author Vassaev A.V.
 * @version 1.0
 */
public class TaskCreator implements Processed<AppInfo> {
	private NewTaskInfo child;
	private String dbAlias;
	private AppInfo ai;
	private long id_processor;

	public void loadFromXML(ATaskInfo parent, Element root) throws SQLException,
			SysException {
		if (root.getTagName().equals("exec")) {
			dbAlias = root.getAttribute("alias");

			Element e = (Element) Strings.getXMLObject(root, "task");
			child = new NewTaskInfo();
			child.load(parent, null, e);
		}
	}

	public void processing(AppInfo prms) throws Throwable {
		ai = prms;
		Manager.setContainer(ai.getContainer());
		Element el = ai.getDocInfo();
		loadFromXML(null, el);

		ITaskAPI ta = TaskAPI.getInstance(dbAlias);
		id_processor = ta.getSubject(0);
		child.createAndReadyTask(ta, null, id_processor, null, null);
	}
}
