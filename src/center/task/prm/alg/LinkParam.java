package center.task.prm.alg;

import org.w3c.dom.Element;

import ru.vassaev.core.types.TimeList;
import ru.vassaev.core.exception.SysException;

import center.task.CalculateException;
import center.task.Context;
import center.task.prm.Alg;
import center.task.prm.Prm;

import java.util.Set;
import java.util.HashSet;

/**
 * Параметр - ссылка на другой параметр Используется в типизованных параметрах и
 * в поточных преобразованиях <from source="param"
 * parent="true/false">...</from>
 * 
 * @author Vassaev A.V.
 */
public class LinkParam extends Alg {
	private final Prm link;

	public LinkParam(center.task.prm.Type tp, String owner, Element e) {
		super(tp, owner);
		link = Prm.getPrmOwner(e);
	}

	public Object calculate(TimeList tl, Context cntx) throws CalculateException {
		tl.addPointTime(TimeList.Type.START);
		try {
			tl.addPointTime(TimeList.Type.WAIT);
			return link.getObject(cntx);
			// if (link.isParent && cntx.parent != null)
			// return cntx.parent.getPrmByFullName(link.fullname);
			// return cntx.getPrmByFullName(link.fullname);
		} catch (SysException e) {
			throw new CalculateException(owner, e);
		} finally {
			tl.addPointTime(TimeList.Type.END);
		}
	}

	public Set<String> depedentOn() {
		if (link.isParent)
			return null;
		Set<String> s = new HashSet<String>();
		s.add(link.fullname);
		return s;
	}

	public Set<String> depedentOnParent() {
		if (!link.isParent)
			return null;
		Set<String> s = new HashSet<String>();
		s.add(link.fullname);
		return s;
	}
}
