package center.app.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import org.w3c.dom.Element;

import ru.vassaev.core.base.Null;
import ru.vassaev.core.db.Manager;
import ru.vassaev.core.db.RsField;
import ru.vassaev.core.db.Sttmnt;
import ru.vassaev.core.exception.SysException;

import center.task.AProcessor;
import center.task.Context;
import center.task.Record;
import center.task.State;
import center.task.TaskException;

/**
 * Процессор обработки по циклу на основании запроса в базе данных
 * 
 * @author Vassaev A.V.
 * @version 1.1
 */
public class ForSelectProcessor extends AProcessor {
	public ForSelectProcessor() {
	}

	private Sttmnt select = null;
	private String db = null;

	public State paramsValidateAndPrepare(Context cntx) throws SysException {
		Object sel = cntx.getPrmByFullName("select");
		if (Null.equ(sel))
			throw new SysException("Parameter \"select\" isn't set");
		db = cntx.getPrmString("db");
		if (db == null)
			throw new SysException("Parameter \"db\" isn't set");
		if (!cntx.isPresentPrm("prm", "update")) {
			throw new SysException("Parameter \"update\" isn't declared");
		}
		if (sel instanceof Element) {
			select = new Sttmnt();
			select.setSQLExpression((Element) sel, "i", "b", "o");
		} else {
			select = new Sttmnt();
			select.setSQLExpression(sel.toString(), "i", "b", "o");
		}
		// System.out.println("!!!" + cntx.id_task + " - " +
		// select.getSQLExpression());
		select.prepare(db);
		return null;
	}

	public State process(Context cntx) throws TaskException, SysException,
			InterruptedException {
		State st = paramsValidateAndPrepare(cntx);
		if (st != null)
			return st;
		String sql = select.getSQLExpression();
		cntx.log(false, "SELECT: ", sql);

		Set<String> list = select.getInParamNames();
		if (list != null) {
			Iterator<String> e = list.iterator();
			while (e.hasNext()) {
				String key = e.next();
				select.setParam(key, cntx.getPrmByFullName(key), Types.OTHER);
			}
		}
		Connection con = null;
		try {
			con = Manager.getConnection(db);
			PreparedStatement pst = select.getStatement(con);
			ru.vassaev.core.thread.Process cur = ru.vassaev.core.thread.Process
					.currentProcess();
			try {
				ResultSet rs = pst.executeQuery();
				ArrayList<RsField> flds = Manager.getTypes(con).getOLFlds(pst);
				long rownum = 0;
				Record r = new Record();
				boolean b;
				if ((b = rs.next())) {
					cur.regResourceName(true, "row@first");
					do {
						for (int i = 0; i < flds.size(); i++) {
							RsField f = flds.get(i);
							Object o = rs.getObject(f.FIELD_NAME);
							if (o == null)
								o = Null.NULL;
							cur.regResourceName(o, "row." + f.FIELD_NAME);
							r.setObject(f.FIELD_NAME, o);
						}
						cur.regResourceName(r, "row");
						rownum = rownum + 1;
						cur.regResourceName(rownum, "row@id");
						b = rs.next();
						if (!b)
							cur.regResourceName(true, "row@last");
						cntx.getPrmByFullName("update");// Расчитать параметр
						cur.regResourceName(false, "row@first");
					} while (b);
				} else {
					cur.regResourceName(true, "row@notfound");
					cntx.getPrmByFullName("update_notfound");// Расчитать
																// параметр
				}
			} catch (SQLException e) {
				cntx.log(false, e.getMessage(), " for SQL:", sql);
				throw new TaskException(State.DONE_ERR, e);
			} finally {
				select.freeStatement(pst);
			}
			return State.DONE_OK;
		} finally {
			Manager.freeConnection(con);
		}
	}

	@Override
	public Set<String> dependentOn() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<String> loopDependentOn() {
		// TODO Auto-generated method stub
		return null;
	}
}
