package center.task.prm.tp;

import org.w3c.dom.Element;

import ru.vassaev.core.exception.SysException;

import center.task.prm.Type;
import center.task.ATaskInfo;

/**
 * Параметр с отсутствием преобразования типа
 * @author Vassaev A.V.
 * @version 1.0
 */
public final class From extends Type {
	public From(Element e, ATaskInfo owner) throws SysException {
		super(e, owner);
	}
}
