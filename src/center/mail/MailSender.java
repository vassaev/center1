package center.mail;

import java.util.Properties;
import java.io.*;

import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.MessagingException;
import javax.activation.*;

import center.system.*;
import ru.vassaev.core.util.Strings;
import ru.vassaev.core.base.Null;
import ru.vassaev.core.exception.SysException;

/**
 * @author Vassaev A.V.
 * @version 1.0
 */
public class MailSender extends Processor {
    public static final class SimpleLetter {
        public String recipient;
        public String h_recipient;
        public String sender;
        public String subject;
        public String body;
        public File[] files;
        public String[] descriptions;
        public String[] filenames;
        public String charset = "UTF-8";
    }

    private Session session = null;

    public MailSender() {
        Properties props = getInitProperties();
        Null n = Null.getInstance();
        props.put("mail.smtp.host", n);
        props = getStepProperties();
        props.put("charset", "UTF-8");
        props.put("body", n);
        props.put("files", new File[0]);
        props.put("descriptions", new String[0]);
        props.put("recipient", n);
        props.put("h_recipient", n);
        props.put("sender", n);
        props.put("subject", n);
    }
    private String host;
    private String port;
    private String user;
    private String pwd;
    protected void initialize() throws SysException {
        Properties props = getInitProperties();
        session = Session.getInstance(props);
        
        host = props.getProperty("mail.smtp.host");
        port = props.getProperty("mail.smtp.port");
        user = props.getProperty("mail.smtp.user");
        pwd = props.getProperty("mail.smtp.pwd");
    }

    protected boolean execute() throws SysException {
        try {
            Properties props = getStepProperties();
            SimpleLetter l = new SimpleLetter();
            l.body = props.getProperty("body");
            l.charset = props.getProperty("charset");
            l.files = (File[])props.get("files");
            l.descriptions = (String[])props.get("descriptions");
            l.filenames = (String[])props.get("filenames");
            l.recipient = props.getProperty("recipient");
            l.h_recipient = props.getProperty("h_recipient");
            l.sender = props.getProperty("sender");
            l.subject = props.getProperty("subject");

            // create a message
            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(l.sender));
            ///*
            String[] bcc = new String[0];
            if (l.h_recipient != null)
                bcc = Strings.parseXVSLine(l.h_recipient, '\\', ',');
            String[] to = new String[0];
            if (l.recipient != null)
                to = Strings.parseXVSLine(l.recipient, '\\', ',');
            if (bcc.length == 1 && bcc[0].length() == 0)
                bcc = new String[0];
            if (to.length == 1 && to[0].length() == 0)
                to = new String[0];
            try {
                InternetAddress[] address = new InternetAddress[to.length];
                for (int i = 0; i < address.length; i++) {
                    address[i] = new InternetAddress(to[i]);
                }
                msg.setRecipients(Message.RecipientType.TO, address);
            } catch (Exception ex) {
                throw new SysException(ex);
            }

            InternetAddress[] address = new InternetAddress[bcc.length + to.length];
            try {
                for (int i = 0; i < address.length; i++) {
                    if (i < bcc.length)
                        address[i] = new InternetAddress(bcc[i]);
                    else
                        address[i] = new InternetAddress(to[i - bcc.length]);
                }
                msg.setRecipients(Message.RecipientType.BCC, address);
            } catch (Exception ex) {
                throw new SysException(ex);
            }
//*/
            msg.setSubject(l.subject, l.charset);
            msg.setSentDate(new java.util.Date());
            // create and fill the first message part
            Multipart mp = new MimeMultipart();

            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setText(l.body, l.charset);
            mp.addBodyPart(mbp1);
            for (int i = 0; i < l.files.length; i++) {
                MimeBodyPart mbp12 = new MimeBodyPart();
                mbp12.setDataHandler(new DataHandler(
                         new FileDataSource(l.files[i])));
                if((l.descriptions != null) && (l.descriptions.length > i))
                    mbp12.setDescription(l.descriptions[i]);
                if((l.filenames != null) && (l.filenames.length > i))
                    mbp12.setFileName(l.filenames[i]);
                else
                    mbp12.setFileName(l.files[i].getName());
                mp.addBodyPart(mbp12);
            }
            // add the Multipart to the message
            msg.setContent(mp);
            synchronized (Transport.class) {
                Transport t;
                t = session.getTransport("smtp");
                t.connect(host, Integer.parseInt(port), user, pwd);
                t.send(msg, address);
                t.close();
            }
            return true;
        } catch (MessagingException ex) {
            ex.printStackTrace();
            throw new SysException(ex);
        }
    }

}
