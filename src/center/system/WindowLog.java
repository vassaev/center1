package center.system;

import ru.vassaev.core.exception.SysException;

import java.util.LinkedList;

/**
 * Окно лога - используюется для временного вывода информации
 * @author Vassaev A.V.
 * @version 1.0 18/02/2009
 */
public class WindowLog {
  private LinkedList<LineLog> lst = new LinkedList<LineLog>();
  private int width;
  private int current_index = 0;
  private LineLog current_line = null;
  private static class LineLog {
    private char[] line = null;
    private String str =  null;
    private int len;
    private LineLog(int width) {
      line = new char[width];
      len = 0;
    }

    public String toString() {
        if (str == null) {
          str = new String(line, 0, len);
        }
      return str;
    }

    public boolean isFull() {
      return (line.length == len);
    }

    public boolean add(char ch) {
      if (len == line.length)
        return false;
      line[len] = ch;
      len++;
      str = null;
      return true;
    }

    public void clear() {
      if (len > 0) {
        len = 0;
        str = null;
      }
    }
  }

  public WindowLog(int width, int height) throws SysException {
    super();
    if (width < 1 || height < 1)
      throw new SysException("A window's log mustn't be less 1x1");
    this.width = width;
    for (int i = 0; i < height; i++) {
      lst.addLast(new LineLog(width));
    }
    current_line = lst.getFirst();
    current_index = 0;
  }

  public int getHeight() {
    return lst.size();
  }

  public int getWidth() {
    return width;
  }

  public int getFullHeight() {
    return current_index;
  }

  public synchronized String getLine(int index) {
    return lst.get(index).toString();
  }
  private long scroll;

  public long scrollLine() {
    return scroll;
  }

  private synchronized void nextLine() {
    if (current_index == lst.size() - 1) {
      current_line = lst.removeFirst();
      current_line.clear();
      lst.addLast(current_line);
      scroll++;
    } else {
      current_index++;
      current_line = lst.get(current_index);
    }
  }

  public void add(char ch) {
    if (current_line.isFull()) {
      nextLine();
    }
    if (ch == '\n')
      nextLine();
    else
      current_line.add(ch);
  }

}
