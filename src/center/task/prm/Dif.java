package center.task.prm;

/**
 * Сложный тип
 * @author Vassaev A.V.
 *
 */
public interface Dif {
	/**
	 * Функция получения части данных по индексу
	 * @param value
	 * @param index
	 * @return Значение по индексу
	 */
	public Object getValueByIndex(Object value, String index);
}
