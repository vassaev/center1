package center.task;

import ru.vassaev.core.container.AppInfo;
import ru.vassaev.core.container.Container;
import ru.vassaev.core.util.Strings;
import ru.vassaev.core.util.Xml;
import ru.vassaev.core.exception.SysException;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.Map;
import java.util.TreeMap;
import java.util.ArrayList;

/**
 * Загрузчик конфигурационного файла.
 * @author Vassaev A.V.
 */
public class TaskApplicationInfo {
  private AppInfo ai;
  protected String dbAlias;
  protected String prcpool_name;
  protected long block_wait;
  private Map<String, TaskInfo> hs = new TreeMap<String, TaskInfo>();
  private ArrayList<NewTaskInfo> starts = new ArrayList<NewTaskInfo>();
  public TaskApplicationInfo(AppInfo ai) {
    this.ai = ai;
  }

  public void loadFromXML(Element root) {
    if (root.getTagName().equals("exec")) {
      dbAlias = root.getAttribute("alias");
      prcpool_name = root.getAttribute("prcpool");
      block_wait = Strings.parseIntegerNvl(root.getAttribute("block_wait"), 3000);
      //Загрузить информацию по всем задачам
      NodeList nl = Xml.getElementsByTagName(root, "task");
      Container cntr = ai.getContainer();
      for (int i = 0; i < nl.getLength(); i++) {
        try {
          Element e = (Element) nl.item(i);
          System.out.print("Loading task " + e.getAttribute("name") + " ...");
          String extend = e.getAttribute("extends");
          TaskInfo ti_extend = null;
          if (extend != null && !"".equals(extend)) {
            ti_extend = hs.get(extend);
            if (ti_extend == null)
              new SysException("There is no extends \"" + extend + "\" for task " + e.getAttribute("cls"))
              .printStackTrace();
          }
          TaskInfo ti = new TaskInfo();
          ti.load(null, ti_extend, e);
          hs.put(ti.clsName, ti);
          System.out.println("\tOk");
        } catch (Exception e) {
          System.out.println("\t" + e.getMessage());
          e.printStackTrace();
        }
      }
      //Загрузить информацию по всем запускам задач
      nl = Xml.getElementsByTagName(root, "start");
      for (int i = nl.getLength() - 1; i >= 0; i--)
        try {
          Element e = (Element) nl.item(i);
          NewTaskInfo nt = new NewTaskInfo();
          nt.load(null, null, e);
          starts.add(nt);
        } catch (Exception e) {
          e.printStackTrace();
        }
    }
  }
}
