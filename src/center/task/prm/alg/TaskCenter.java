package center.task.prm.alg;

import center.task.prm.Alg;
import center.task.CalculateException;
import center.task.Context;
import center.task.NewTaskInfo;
import center.task.ATaskInfo;

import java.util.Set;

import org.w3c.dom.Element;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.exception.SysRuntimeException;
import ru.vassaev.core.util.Strings;
import ru.vassaev.core.types.TimeList;
import ru.vassaev.core.types.TimeList.Type;

/**
 * Параметр - задание в системе
 * @author Vassaev A.V
 * @version 1.0 30/08/2011.
 */
public final class TaskCenter extends Alg {
  private NewTaskInfo tsk;
  private boolean nowait;
  private long wait;

  public TaskCenter(center.task.prm.Type tp, String owner, Element e, ATaskInfo parent) throws SysException {
    super(tp, owner);
    tsk = new NewTaskInfo();
    tsk.load(parent, null, e);
    Object x = Strings.getXMLObject(e, "nowait");
    nowait = (x != null);
    if (!nowait)
    	wait = Strings.parseIntegerNvl(Strings.getXMLValue(e, "wait"), 30000);
  }

  protected Object calculate(TimeList tl, Context cntx) throws CalculateException {
    try {
    	tl.addPointTime(Type.START);
      Context res = tsk.createAndReadyTask(cntx.ta, cntx, cntx.id_task, null, null);
      if (res == null || res.id_task < 0)
      	return res;
      tl.addPointTime(TimeList.Type.WAIT);
      if (!nowait) {
        res.ta.waitFinished(cntx.id_task, res.id_task, wait);
        tsk.delete_files(res);
      }
      tl.addPointTime(TimeList.Type.END_WAIT);
      return res;
    } catch (SysException e) {
      throw new CalculateException(owner, e);
    } finally {
    	tl.addPointTime(Type.END);
    }
  }

  public Set<String> depedentOn() {
    return tsk.depedentOnParent();
  }

  public Set<String> depedentOnParent() {
    return null;
  }
}
