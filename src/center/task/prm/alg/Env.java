package center.task.prm.alg;

import org.w3c.dom.Element;

import center.task.CalculateException;
import center.task.Context;
import center.task.prm.Alg;
import center.task.prm.Prm;
import ru.vassaev.core.types.TimeList;
import ru.vassaev.core.types.TimeList.Type;
import ru.vassaev.core.base.Null;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.exception.SysRuntimeException;

import java.util.Set;
import java.util.HashSet;


/**
 * Параметр для получения значения enviroment
 * @author Vassaev A.V.
 * @version 1.1 03/08/2011
 */
public final class Env extends Alg {
	
	private Object env_name;
	public Env(center.task.prm.Type tp, String owner, String env_name) {
		super(tp, owner);
		if (env_name == null)
			throw new SysRuntimeException("The enviroment param hasn't name");
		this.env_name = env_name;
	}
	
	public Env(center.task.prm.Type tp, String owner, Element e) throws SysException {
    super(tp, owner);
		env_name = Prm.getPrm(e);
		if (Null.equ(env_name))
			throw new SysException("The environment in param '" + owner + "' hasn't name");
	}

  /**
   * Получить имя enviroment параметра
   * @param cntx - контекст
   * @return имя enviroment
   */
	public String getName(TimeList tl, Context cntx) throws CalculateException {
  	tl.addPointTime(Type.START);
  	try {
  		if (env_name instanceof String)
  			return (String)env_name;
  		if (env_name instanceof Prm)
        try {
    	  	tl.addPointTime(Type.WAIT);
          return ((Prm)env_name).getString(cntx);
        } catch (SysException e) {
          throw new CalculateException(owner, e);
        } finally {
    	  	tl.addPointTime(Type.END_WAIT);
        }
      return null;
  	} finally {
	  	tl.addPointTime(Type.END);
  	}
	}
  
	public Object calculate(TimeList tl, Context cntx) throws CalculateException {
		try {
	  	tl.addPointTime(Type.START);
			return System.getenv(getName(tl, cntx));
		} finally {
	  	tl.addPointTime(Type.END);
		}
	}

  /**
   * Список параметров, от которых зависит данный параметр
   * @return список параметров
   */
	public Set<String> depedentOn() {
    // Если имя enviroment определяется по параметру, то существует зависимость
		if (env_name instanceof Prm && !((Prm)env_name).isParent) {
      Set<String> s = new HashSet<String>();
      s.add(((Prm)env_name).fullname);
			return s;
    }
		return null;
	}

  public Set<String> depedentOnParent() {
    // Если имя enviroment определяется по параметру, то существует зависимость
    if (env_name instanceof Prm && ((Prm)env_name).isParent) {
      Set<String> s = new HashSet<String>();
      s.add(((Prm)env_name).fullname);
      return s;
    }
    return null;
  }
}