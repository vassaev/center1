package center.system;

import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.ClassExpert;

import java.util.ArrayList;
import java.io.File;
import java.io.IOException;

/**
 * Дизъюнктивная нормальная форма
 *
 * @author Vassaev A.V.
 * @version 1.0
 */
public class DNF<E> extends ArrayList<DNF.Conjunct> {
  public DNF() {
    super();
  }

  public static class Conjunct extends ArrayList<Object> {
    public synchronized String toString() {
      StringBuffer sb = new StringBuffer();
      int l = size();
      for (int j = 0; j < l; j++) {
        String xVal = get(j).toString();
        if (j > 0) {
          sb.append(" AND ");
        }
        sb.append(xVal);
      }
      return sb.toString();
    }

    @Override
    public Object clone() {
      File x = null;
      try {
        x = File.createTempFile("clone", "dnf");
        return ClassExpert.getClassExpert(Conjunct.class).cloneByStream(this, x);
      } catch (SysException ex) {
        return super.clone();
      } catch (IOException e) {
        return super.clone();
      }
      finally {
        if (x != null) {
          x.deleteOnExit();
          x.delete();
        }
      }
    }
  }

  public synchronized String toString() {
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < size(); i++) {
      Conjunct xOr = get(i);
      if (i > 0) {
        sb.append(" OR ");
      }
      int l = xOr.size();
      if (l > 1) {
        sb.append('(');
      }
      for (int j = 0; j < l; j++) {
        String xVal = xOr.get(j).toString();
        if (j > 0) {
          sb.append(" AND ");
        }
        sb.append(xVal);
      }
      if (l > 1) {
        sb.append(')');
      }
    }
    return sb.toString();
  }

  public final int getLength() {
    int res = 0;
    for (int i = size() - 1; i >= 0; i--) {
      res += get(i).size();
    }
    return res;
  }

  public final Object getItem(int i) {
    int l = size();
    for (int j = 0; j < l; j++) {
      Conjunct c = get(j);
      int s = c.size();
      i -= s;
      if (i < 0) {
        return c.get(i + s);
      }
    }
    return null;
  }

  public final void setItem(int i, Object o) {
    int l = size();
    for (int j = 0; j < l; j++) {
      Conjunct c = get(j);
      int s = c.size();
      i -= s;
      if (i < 0) {
        c.set(i + s, o);
        return;
      }
    }
  }

  /**
   * Возвращает контекст, ассоциированный с данным DNF
   *
   * @return Object
   */
  public E getContext() {
    return null;
  }

  public Object clone() {
    File x = null;
    try {
      x = File.createTempFile("clone", "dnf");
      return ClassExpert.getClassExpert(DNF.class).cloneByStream(this, x);
    } catch (SysException ex) {
      return super.clone();
    } catch (IOException e) {
      return super.clone();
    }
    finally {
      if (x != null) {
        x.deleteOnExit();
        x.delete();
      }
    }
  }
}
