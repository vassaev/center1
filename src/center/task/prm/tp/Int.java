package center.task.prm.tp;

import org.w3c.dom.Element;

import center.task.CalculateException;
import center.task.Context;
import center.task.ATaskInfo;

import ru.vassaev.core.util.Strings;
import ru.vassaev.core.types.TimeList;
import ru.vassaev.core.base.Null;
import ru.vassaev.core.exception.SysException;

/**
 * Параметр типа int64
 *
 * @author Vassaev A.V.
 * @version 1.0 
 */
public final class Int extends Str {

  public Int(Element e, ATaskInfo owner) throws SysException {
    super(e, owner);
  }

  public Object calculate(TimeList tl, Context cntx) throws CalculateException {
  	try {
  		tl.addPointTime(TimeList.Type.START);
      Object o = super.calculate(tl, cntx);
      if (Null.NULL.equals(o)) {
        return o;
      }
      Long v = Strings.parseLong(o);
      return v;
  	} finally {
  		tl.addPointTime(TimeList.Type.END);
  	}
  }
}
