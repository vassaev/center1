package center.system.dukpt;

import java.io.UnsupportedEncodingException;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.util.Strings;

public class DUKPT {
	private static final String ucHexToChar = "0123456789ABCDEF";

	public static final String toHexString(byte[] in) {
		int lc = in.length;
		StringBuffer out = new StringBuffer(lc << 1);
		for (int i = 0; i < lc; i++) {
			out.append(ucHexToChar.charAt((0xff & in[i]) >> 4));
			out.append(ucHexToChar.charAt(in[i] & 0x0f));
		}
		return out.toString();
	}

	public static final String toHexString(byte[] in, int pos) {
		int lc = in.length;
		StringBuffer out = new StringBuffer((lc - pos) << 1);
		for (int i = pos; i < lc; i++) {
			out.append(ucHexToChar.charAt((0xff & in[i]) >> 4));
			out.append(ucHexToChar.charAt(in[i] & 0x0f));
		}
		return out.toString();
	}

	public static final String toHexString(byte[] in, String split) {
		int lc = in.length;
		StringBuffer out = new StringBuffer(lc << 1);
		for (int i = 0; i < lc; i++) {
			out.append(ucHexToChar.charAt((0xff & in[i]) >> 4));
			out.append(ucHexToChar.charAt(in[i] & 0x0f));
			out.append(split);
		}
		return out.toString();
	}

	public static final byte[] parseHexStr2Byte(String hex) {
		byte[] result = new byte[hex.length() >> 1];
		for (int i = 0; i < result.length; i++)
			result[i] = (byte) Integer.parseInt(
					hex.substring((i << 1), (i << 1) + 2), 16);
		return result;
	}

	public static final void pan_count(long[] count_num) {
		long tmp_count, j;
		tmp_count = count_num[0];
		while (true) {
			j = 0;
			if (count_num[0] > 0x1fffff) {
				count_num[0] = 1;
				return;
			}
			for (int i = 0; i < 21; i++) {
				if ((tmp_count & 0x01) == 1)
					j++;
				tmp_count = tmp_count >> 1;
			}
			if (j < 11)
				return;
			tmp_count = count_num[0];
			j = 1;
			for (int i = 0; i < 21; i++) {
				if (((tmp_count >> i) & 0x01) == 1) {
					j <<= i;
					break;
				}
			}
			count_num[0] += j;
			tmp_count = count_num[0];
		}
	}

	public static final long unsigned4BytesToInt(byte[] buf, int pos) {
		int firstByte = 0;
		int secondByte = 0;
		int thirdByte = 0;
		int fourthByte = 0;
		int index = pos;
		firstByte = (0x000000FF & ((int) buf[index]));
		secondByte = (0x000000FF & ((int) buf[index + 1]));
		thirdByte = (0x000000FF & ((int) buf[index + 2]));
		fourthByte = (0x000000FF & ((int) buf[index + 3]));
		index = index + 4;
		return ((long) (firstByte << 24 | secondByte << 16 | thirdByte << 8 | fourthByte)) & 0xFFFFFFFFL;
	}

	private static byte[] XOR_DATA = { (byte) 0xC0, (byte) 0xC0, (byte) 0xC0,
			(byte) 0xC0, 0x00, 0x00, 0x00, 0x00, (byte) 0xC0, (byte) 0xC0,
			(byte) 0xC0, (byte) 0xC0, 0x00, 0x00, 0x00, 0x00 };

	public DUKPT() {
		super();
	}

	public static void comb_KSN(byte[] tmp_KSN, long count_num) {
		long temp;
		tmp_KSN[7] &= 0xe0;
		tmp_KSN[8] = 0;
		tmp_KSN[9] = 0;
		temp = (count_num >> 16) & 0x1f;
		tmp_KSN[7] = (byte) ((0xff & (long) tmp_KSN[7]) + temp);
		temp = (count_num >> 8) & 0xff;
		tmp_KSN[8] = (byte) ((0xff & (long) tmp_KSN[8]) + temp);
		temp = count_num & 0xff;
		tmp_KSN[9] = (byte) ((0xff & (long) tmp_KSN[9]) + temp);
	}

	public static final long getCount(byte[] KSN) {
		byte[] buf = new byte[4];
		for (int i = 0; i < 4; i++)
			buf[i] = KSN[i + 6];
		buf[0] = 0;
		buf[1] = (byte) (buf[1] & 0x1F);
		return unsigned4BytesToInt(buf, 0);
	}

	public static void xor_cl(byte[] result, byte[] source1, byte[] source2,
			int pos2, int length) {
		for (int i = 0; i < length; i++) {
			result[i] = (byte) (source1[i] ^ source2[i + pos2]);
		}
	}

	public static void generate_key(byte[] now_key, byte[] tmpKSN, int pos2) {
		byte[] cr1 = new byte[8];
		byte[] cr2 = new byte[8];
		// memcpy(cr1,tmpKSN,8);
		System.arraycopy(tmpKSN, pos2, cr1, 0, 8);
		xor_cl(cr2, cr1, now_key, 8, 8);
		DES.Des_string(cr2, 8, now_key, 8, cr2, DES.DES_ENCRYPT);
		xor_cl(cr2, cr2, now_key, 8, 8);
		xor_cl(now_key, XOR_DATA, now_key, 0, 16);
		xor_cl(cr1, cr1, now_key, 8, 8);
		DES.Des_string(cr1, 8, now_key, 8, cr1, DES.DES_ENCRYPT);
		xor_cl(cr1, cr1, now_key, 8, 8);
		// memcpy(now_key,cr1,8);
		System.arraycopy(cr1, 0, now_key, 0, 8);
		// memcpy(&now_key[8],cr2,8);
		System.arraycopy(cr2, 0, now_key, 8, 8);
	}

	public static final byte[] getKey(byte[] initKey, byte[] KSNdata) {
		long[] cnt = { getCount(KSNdata) };
		pan_count(cnt);
		byte[] key = new byte[16];
		get_now_key(key, initKey, cnt, KSNdata);
		return key;
	}

	public static void get_now_key(byte[] now_key, byte[] init_key,
			long count_num[], byte[] KSNdata) {
		long tmp_count, k, x;
		int j = 0;
		// memcpy(now_key,init_key,16);
		System.arraycopy(init_key, 0, now_key, 0, 16);
		tmp_count = count_num[0];
		for (int i = 0; i < 21; i++) {
			if ((tmp_count & 0x01L) != 0)
				j++;
			tmp_count = tmp_count >> 1;
		}
		tmp_count = count_num[0];
		k = 0;
		x = 0x100000;
		while (j-- > 0) {
			for (int i = 0; i < 21; i++) {
				if ((tmp_count & 0x100000) > 0)
					break;
				tmp_count <<= 1;
				x >>= 1;
			}
			k += x;
			tmp_count <<= 1;
			x >>= 1;
			comb_KSN(KSNdata, k);
			generate_key(now_key, KSNdata, 2);
		}
	}

	public static String decryptBCD(String bcdInitKey, String bcdKSN,
			String bcdData) throws UnsupportedEncodingException {
		byte[] initKey = DUKPT.parseHexStr2Byte(bcdInitKey);
		byte[] data = DUKPT.parseHexStr2Byte(bcdData);
		byte[] KSN = DUKPT.parseHexStr2Byte(bcdKSN);
		byte[] key = DUKPT.getKey(initKey, KSN);
		byte[] result = new byte[data.length];
		DES.Des_string(data, data.length, key, 16, result, DES.TDES_DECRYPT);
		return DUKPT.toHexString(result);
	}

	public static String decryptStr(String bcdInitKey, String bcdKSN,
			String bcdData, String charset) throws UnsupportedEncodingException {
		byte[] initKey = DUKPT.parseHexStr2Byte(bcdInitKey);
		byte[] data = DUKPT.parseHexStr2Byte(bcdData);
		byte[] KSN = DUKPT.parseHexStr2Byte(bcdKSN);
		byte[] key = DUKPT.getKey(initKey, KSN);
		byte[] result = new byte[data.length];
		DES.Des_string(data, data.length, key, 16, result, DES.TDES_DECRYPT);

		return new String(result, charset);
	}

	public static String decryptStr2(String bcdInitKey, String bcdKSN,
			String bcdData, String charset) throws UnsupportedEncodingException {
		byte[] initKey = DUKPT.parseHexStr2Byte(bcdInitKey);
		byte[] data = DUKPT.parseHexStr2Byte("0000000000000000" + bcdData);
		byte[] KSN = DUKPT.parseHexStr2Byte(bcdKSN);
		byte[] key = DUKPT.getKey(initKey, KSN);
		byte[] result = new byte[data.length];
		DES.Des_string(data, data.length, key, 16, result, DES.TDES_DECRYPT);
		int i = 0;
		for (; i < data.length; i++)
			if (result[i] != 0x00)
				break;
		return new String(result, i, result.length - i, charset);
	}

	public static String decryptBCD2(String bcdInitKey, String bcdKSN,
			String bcdData) throws UnsupportedEncodingException {
		byte[] initKey = DUKPT.parseHexStr2Byte(bcdInitKey);
		byte[] data = DUKPT.parseHexStr2Byte("0000000000000000" + bcdData);
		byte[] KSN = DUKPT.parseHexStr2Byte(bcdKSN);
		byte[] key = DUKPT.getKey(initKey, KSN);
		byte[] result = new byte[data.length];
		DES.Des_string(data, data.length, key, 16, result, DES.TDES_DECRYPT);
		int i = 0;
		for (; i < data.length; i++)
			if (result[i] != 0x00)
				break;
		return DUKPT.toHexString(result, i);
	}

	public static String encryptStr2(String bcdInitKey, String bcdKSN,
			String strData, String charset) throws UnsupportedEncodingException {
		byte[] initKey = DUKPT.parseHexStr2Byte(bcdInitKey);
		int l = ("00000000" + strData).getBytes(charset).length;
		byte[] data = (Strings.lpad(strData, l + (8 - l % 8), (char) 0))
				.getBytes(charset);
		byte[] KSN = DUKPT.parseHexStr2Byte(bcdKSN);
		byte[] key = DUKPT.getKey(initKey, KSN);
		byte[] result = new byte[data.length];
		DES.Des_string(data, data.length, key, 16, result, DES.TDES_ENCRYPT);
		int i = 0;
		for (; i < data.length; i++)
			if (result[i] != 0x00)
				break;
		String r = DUKPT.toHexString(result).substring(i * 2);
		return r;
	}

}