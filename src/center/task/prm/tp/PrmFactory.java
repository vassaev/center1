package center.task.prm.tp;

import java.util.Iterator;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import ru.vassaev.core.Expression;
import ru.vassaev.core.base.Null;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.util.Strings;

import center.task.ATaskInfo;

/**
 * Фабрика параметров
 * @author Vassaev
 *
 */
public class PrmFactory {
	public Object getParam(Element e, ATaskInfo owner) throws SysException {
		String type = Strings.getXMLValue(e, "#type");
		if (type == null) {
			return new Str(e, owner);
		} else if ("string".equals(type)) {
			return new Str(e, owner);
		} else if ("int".equals(type)) {
			return new Int(e, owner);
		} else if ("from".equals(type)) {
			return new From(e, owner);
		} else if ("xml".equals(type)) {
			return new Xml(e, owner);
		} else {
			return Null.NULL;
		}
	}

  public Element getParamDBTS(Element e) {
    String name = e.getAttribute("name");
    //String grp = e.getAttribute("grp");
    String index = e.getAttribute("index");
    boolean nondb = Strings.parseBooleanNvl(e.getAttribute("nondb"), false);
    Boolean cache = Strings.parseBoolean(e.getAttribute("cache"));
    if (cache == null)
    	cache = Strings.parseBoolean(e.getAttribute("cash"));
    if (cache == null)
    	cache = false;
    boolean saved = Strings.parseBooleanNvl(e.getAttribute("saved"), false);
    boolean init_first = Strings.parseBooleanNvl(e.getAttribute("init_first"),false);

  	Element t = e.getOwnerDocument().createElement("prm");
  	{
  		t.setAttribute("name", name);
  		t.setAttribute("grp", name);
  		//<priority>only_memory</priority>
  		if (nondb) {
  			Element ch = e.getOwnerDocument().createElement("priority");
  			ch.setTextContent("only_memory");
  			t.appendChild(ch);
  		}
  		if (cache) {
  			Element ch = e.getOwnerDocument().createElement("cache");
  			ch.setTextContent("true");
  			t.appendChild(ch);
  		}
  		if (index != null) {
  			Element ch = e.getOwnerDocument().createElement("index");
  			ch.setTextContent(index);
  			t.appendChild(ch);
  		}
  		if (saved) {
  			Element ch = e.getOwnerDocument().createElement("save");
  			ch.setAttribute("state","PROCESSING");
  			if (init_first)
  				ch.setAttribute("time", "before");
  			else
  				ch.setAttribute("time", "after");
  			t.appendChild(ch);
  		}
  	}
    String from = e.getAttribute("from");
    NodeList nl1 = e.getChildNodes();
    int l = nl1.getLength();
    if (l > 1) {
      if ("switch".equals(from)) {
      	t.setAttribute("type", "from");
      	Element ch = e.getOwnerDocument().createElement("from");
      	t.appendChild(ch);
      	ch.setAttribute("source", "switch");
      	String swname=e.getAttribute("swname");
      	Element ch1 = e.getOwnerDocument().createElement("name");
      	ch1.setTextContent(swname);
      	ch.appendChild(ch1);
      	for(int i = 0; i < nl1.getLength(); i++)
      		ch.appendChild(nl1.item(i));
      	return t;
      } else {
      	t.setAttribute("type", "xml");
      	Element ch = e.getOwnerDocument().createElement("value");
        t.appendChild(ch);
        ch.appendChild(e);
        return t;
      }
    } else if (l == 1) {
      if (nl1.item(0).getNodeType() == Node.ELEMENT_NODE) {
      	t.setAttribute("type", "xml");
      	Element ch = e.getOwnerDocument().createElement("value");
        t.appendChild(ch);
        ch.appendChild(e);
        return t;
      }/* else {
        String type = Strings.getXMLValue(e, "#type");
        String val = e.getTextContent();
        if ("param".equals(from)) {
          if ("xml".equals(type)) {
            prm_.put(fn, new ParamAsXMLDocument(e));
          } else {
            prm_.put(fn, e);
          }
        } else if ("parent".equals(from)) {
          if ("saved".equals(type)) {
            prm_.put(fn, new SavedParam(e, val, prefix));
          } else {
            if ("ordinal".equals(type)) {
              val = val + ((suffix == null) ? "" : (prefix + suffix));
            }
            prm_.put(fn, new ParamFromParent(e, val, prefix));
          }
        } else if ("select".equals(from)) {
          prm_.put(fn, new ParamFromSelect(e, val, prefix));
        } else if ("task".equals(from)) {
          prm_.put(fn, new ParamFromTask(e, val, prefix));
        } else
          prm_.put(fn, new StringParam(e, val));
      }
    } else {
      prm_.put(fn, "");
    }*/
    }
    return null;
	}

	private static void _x(Element e, String part) {
		Document doc = e.getOwnerDocument();
		Expression exp = new Expression(part, "::");
		Iterator<Object> i = exp.getElements();
		while(i.hasNext()) {
			Object p = i.next();
			if (p instanceof String) {
				e.appendChild(doc.createTextNode((String)p));
			} else {
				Element el = doc.createElement("p");
				Expression.ParamInfo prm = (Expression.ParamInfo)p;
				el.setNodeValue(prm.getName());
			}
		}
	}
	public static void convertParamFromSelect(Element e, Element t) {
		t.setAttribute("type", "from");
		Document doc = t.getOwnerDocument();
		String sql = e.getTextContent();
		Element from = doc.createElement("from");
		from.setAttribute("source", "select");

		Expression exp = new Expression(sql, "$$");
		Iterator<Object> i = exp.getElements();
		while(i.hasNext()) {
			Object p = i.next();
			if (p instanceof String) {
				_x(from, p.toString());
			} else {
				Element el = doc.createElement("i");
				Expression.ParamInfo prm = (Expression.ParamInfo)p;
				el.setNodeValue(prm.getName());
			}
		}
		String index = e.getAttribute("index");
		if (index != null) {
			Element idx = doc.createElement("index");
			t.appendChild(idx);
			idx.setNodeValue(index);
		}
	}
	
	public static void convertParamFromParent(Element e, Element t) {
		t.setAttribute("type", "from");
		Document doc = t.getOwnerDocument();
		String paramname = e.getTextContent();
		Element from = doc.createElement("from");
		from.setAttribute("source", "param");
		int i = paramname.indexOf('/');
		if (i >= 0) {
			String pgrp = paramname.substring(0, i);
			Element grp = doc.createElement("grp");
			from.appendChild(grp);
			grp.setNodeValue(pgrp);
			paramname = paramname.substring(i + 1, paramname.length() - 1);
		}
		Element nm = doc.createElement("name");
		from.appendChild(nm);
		nm.setNodeValue(paramname);
		t.appendChild(from);
		String index = e.getAttribute("index");
		if (index != null) {
			Element idx = doc.createElement("index");
			t.appendChild(idx);
			idx.setNodeValue(index);
		}
	}

	public static void convertParamAsXMLDocumentFromParam(Element e, Element t) {
		t.setAttribute("type", "xml");
		Document doc = t.getOwnerDocument();
		String header = e.getAttribute("header");
		Element hr = doc.createElement("header");
		hr.setNodeValue((Null.equ(header)) ? "FALSE" : "TRUE");
		t.appendChild(hr);

		String encoding = e.getAttribute("encoding");
		if (encoding == null)
			encoding = "UTF-8";
		Element enc = doc.createElement("header");
		enc.setNodeValue(encoding);
		t.appendChild(enc);

		String paramname = e.getTextContent();
		Element from = doc.createElement("from");
		from.setAttribute("source", "param");
		int i = paramname.indexOf('/');
		if (i >= 0) {
			String pgrp = paramname.substring(0, i);
			Element grp = doc.createElement("grp");
			from.appendChild(grp);
			grp.setNodeValue(pgrp);
			paramname = paramname.substring(i + 1, paramname.length() - 1);
		}
		Element nm = doc.createElement("name");
		from.appendChild(nm);
		nm.setNodeValue(paramname);
		t.appendChild(from);
	}

	public static void convertParamXMLBlock(Element e, Element t) {
		t.setAttribute("type", "xml");
		Element value = t.getOwnerDocument().createElement("value");
		t.appendChild(value);
		value.appendChild(e);
	}
	public static void convertParamString(Element e, Element t) {
		t.setAttribute("type", "string");
		String value = e.getTextContent();
		Element val = t.getOwnerDocument().createElement("value");
		val.setNodeValue(value);
		t.appendChild(val);
	}
	public static void convertParamFromSwitch(Element e, Element  t) {
		t.setAttribute("type", "from");
		Element from = t.getOwnerDocument().createElement("from");
		t.appendChild(from);
		from.setAttribute("source", "switch");
		
		String swName = Strings.getXMLValue(e, "#swname");
		Element name = t.getOwnerDocument().createElement("name");
		name.setNodeValue(swName);
		from.appendChild(name);

		NodeList le = ru.vassaev.core.util.Xml.getElementsByTagName(e, "case");
		for (int i = le.getLength() - 1; i >= 0; i--) {
			Element c = (Element) le.item(i);
			String key = c.getAttribute("value");
			String val = c.getTextContent();
			Element eq = t.getOwnerDocument().createElement("eq");
			eq.setNodeValue(key);
			Element th = t.getOwnerDocument().createElement("then");
			th.setNodeValue(val);
			Element cs = t.getOwnerDocument().createElement("case");
			cs.appendChild(eq);
			cs.appendChild(th);
			from.appendChild(cs);
		}
		String els = Strings.getXMLValue(e, "else");
		Element els1 = t.getOwnerDocument().createElement("else");
		els1.setNodeValue(els);
		from.appendChild(els1);
		String isnull = Strings.getXMLValue(e, "isnull");
		Element isnull1 = t.getOwnerDocument().createElement("isnull");
		isnull1.setNodeValue(isnull);
		from.appendChild(isnull1);
	}
	
	public Element convert(Element e) {
		Element t = e.getOwnerDocument().createElement("prm");
		{// Модификация параметра кеширования
			String cache = Strings.getXMLValue(e, "#cache");
			if (cache == null)
				cache = Strings.getXMLValue(e, "#cash");
			Strings.setXMLValue(t, "cache", 
					Boolean.toString(Strings.parseBooleanNvl(cache, false)));
		}
		{// Модификация имени параметра 
			String name = Strings.getXMLValue(e, "#name");
			String grp = Strings.getXMLValue(e, "#grp");
//			String fn = (grp == null || grp.length() == 0) ? name : grp + "/" + name;
			t.setAttribute("name", name);
			t.setAttribute("grp", grp);
		}
		{//Параметр всегда имеет тип from
			
		}
		String from = Strings.getXMLValue(e, "#from");

		NodeList nl1 = e.getChildNodes();
		int l = nl1.getLength();
		if (l > 1) {
			if ("switch".equals(from)) {
				convertParamFromSwitch(e, t);
			} else {
				//convertParamXMLBlockList(e, t);
			}
		} else if (l == 1) {
			if (nl1.item(0).getNodeType() == Node.ELEMENT_NODE) {
				convertParamXMLBlock(e, t);
			} else {
				String type = Strings.getXMLValue(e, "#type");
				if ("param".equals(from)) {
					if ("xml".equals(type)) {
						convertParamAsXMLDocumentFromParam(e, t);
					} else {
						convertParamString(e, t);
					}
				} else if ("parent".equals(from)) {
					convertParamFromParent(e, t);
				} else if ("select".equals(from)) {
					convertParamFromSelect(e, t);
				} else if ("task".equals(from)) {
					//convertParamFromTask(e, t);
				} else
					convertParamString(e, t);
			}
		} else {
			convertParamString(e, t);
		}
		return t;
	}
}
