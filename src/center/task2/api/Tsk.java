package center.task2.api;

import java.util.Date;

import ru.vassaev.core.exception.SysException;

import center.task.State;

public class Tsk {
	// Свойство формируется Брокером в момент создания задачи Инициатором и далее не изменяется
	Long id;
	// Свойство формируется Брокером в момент создания задачи Инициатором и далее не изменяется
	long dt_created;
	// Целевая дата выполнения задания
	long dt_goal;
	// Начало выполнения
	long dt_started;
	// Задача получила конечный статус
	long dt_finished;
	// Текущее состояние задачи
	State status_id;
	// Имя класса задачи
	String classname;
	// Идентификатор инициатора
	final long initiator_id;
	// Идентификатор исполнителя
	Long processor_id;
	// Программа исполнителя
	String program;
	// Внешний идентификатор задачи - устанавливается при блокировании задания исполнителем
	String opid;
	// Период ожидания блокировки задания для исполнения (мсек). Далее - брокер попытается отменить задание
	long block_duration;
	// Период ожидания исполнения задания (мсек). Далее - брокер попытается отменить задание
	long finish_duration;
	// Период в днях, после истечения которого, данные по задаче должны быть перенесены в архив
	double arch_day;
	// Время последнего обращения к задаче
	long last_ping;
	// Информация об ошибке
	String errs;
	Throwable errp;
	// Идентификатор родительской задачи
	final long parent_id;
	// Дата-время окончания ожидания блокировки
	long dt_end_block;
	// Назначенное дата-время переноса в архив
	long dt_arch;
  /**
   * Создать шаблон задачи
   * @param initiator_id - идентификатор Инициатора (см. ITaskAPI2.getSubject)
   * @param parent_task_id - идентификатор родительской задачи в данном Брокере
   * @throws SysException
   */
	Tsk(long initiator_id, long parent_task_id) throws SysException {
		this.initiator_id = initiator_id;
		this.parent_id = parent_task_id;
	}

	private static java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(
			"dd.MM.yyyy HH:mm:ss.S GMT+HH:mm");

	public synchronized final String getFieldByName(String name) {
		if (name == null)
			return null;
		if (name.equalsIgnoreCase("id"))
			return String.valueOf(id);
		if (name.equalsIgnoreCase("dt_created"))
			return sdf.format(new Date(dt_created));
		if (name.equalsIgnoreCase("dt_goal"))
			return sdf.format(new Date(dt_goal));
		if (name.equalsIgnoreCase("dt_started"))
			return sdf.format(new Date(dt_started));
		if (name.equalsIgnoreCase("dt_finished"))
			return sdf.format(new Date(dt_finished));
		if (name.equalsIgnoreCase("status_id"))
			return status_id.name();
		if (name.equalsIgnoreCase("classname"))
			return classname;
		if (name.equalsIgnoreCase("initiator_id"))
			return String.valueOf(initiator_id);
		if (name.equalsIgnoreCase("processor_id"))
			return String.valueOf(processor_id);
		if (name.equalsIgnoreCase("program"))
			return program;
		if (name.equalsIgnoreCase("opid"))
			return opid;
		if (name.equalsIgnoreCase("block_duration"))
			return String.valueOf(block_duration);
		if (name.equalsIgnoreCase("finish_duration"))
			return String.valueOf(finish_duration);
		if (name.equalsIgnoreCase("arch_day"))
			return String.valueOf(arch_day);
		if (name.equalsIgnoreCase("last_ping"))
			return sdf.format(new Date(last_ping));
		if (name.equalsIgnoreCase("errs"))
			return errs;
		if (name.equalsIgnoreCase("parent_id"))
			return String.valueOf(parent_id);
		if (name.equalsIgnoreCase("dt_end_block"))
			return sdf.format(new Date(dt_end_block));
		if (name.equalsIgnoreCase("dt_arch"))
			return sdf.format(new Date(dt_arch));
		return null;
	}
}
