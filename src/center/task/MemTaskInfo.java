package center.task;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import center.task.prm.tp.PrmFactory;
import center.task.prm.tp.Str;

import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.util.Strings;

public class MemTaskInfo extends ATaskInfo {

	protected Str processor_name;
	protected Str st;
  private Map<String, Object> prms_parent;

  public MemTaskInfo() throws SysException {
  	super();
  }
  public void load(ATaskInfo parent, Element e) throws SysException {
		super.load(parent, null, e);
		prms = new TreeMap<String, Object>();
		prms_parent = new TreeMap<String, Object>();
		loadNewParams(prms, prms_parent, e);
		Element processor = (Element) Strings.getXMLObject(e, "processor");
		if (processor != null) this.processor_name = new Str(processor, this);
		st = new Str();
	}

  public final AProcessor newProcessorInstance(Context cntx) throws SysException {
    try {
    	ClassLoader CL = ClassLoader.getSystemClassLoader();
    	String processor = Strings.getString(processor_name.getValue(cntx));
    	Class<?> cls = Class.forName(processor, true, CL);
      Object o = cls.newInstance();
      if (!(o instanceof AProcessor))
        throw new SysException("The class by name '" + processor + "' is not AProcessor");
      return (AProcessor) o;
    } catch (IllegalAccessException e) {
      throw new SysException(e);
    } catch (InstantiationException e) {
      throw new SysException(e);
    } catch (ClassNotFoundException e) {
      throw new SysException(e);
		} catch (CalculateException e) {
      throw new SysException(e);
		}
  }
	protected Map<String, Object> getPrmsBySpace(String space) {
		Map<String, Object> prms = super.getPrmsBySpace(space);
		if (prms != null)
			return prms;
  	if ("processor".equals(space)) {
  		prms = new HashMap<String, Object>();
  		prms.put("", processor_name);
  		return prms;
  	}
  	if ("tsk".equals(space)) {
  		prms = new HashMap<String, Object>();
  		prms.put("STATUS_ID", st);
  		return prms;
  	}
  	return null;
  }

	public void loadNewParams(Map<String, Object> prm_, Map<String, Object> prm_parent, Element pnl) throws SysException {
		NodeList nl = pnl.getChildNodes();
		PrmFactory fact = new PrmFactory();
		for (int i = nl.getLength() - 1; i >= 0; i--) {
			Node n = nl.item(i);
			if (n.getNodeType() != Node.ELEMENT_NODE)
				continue;
			String x = n.getNodeName();
			Element e = (Element) n;

			String name = Strings.getXMLValue(e, "#name");
			String grp = Strings.getXMLValue(e, "#grp");
			String fn = (grp == null || grp.length() == 0) ? name : grp + "/"
					+ name;
      if (x.equals("prm"))
			  prm_.put(fn, fact.getParam(e, this));
      if (x.equals("parent"))
			  prm_parent.put(fn, fact.getParam(e, this));
		}
	}
}
