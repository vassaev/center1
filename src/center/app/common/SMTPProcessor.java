package center.app.common;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import ru.vassaev.core.base.Null;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.util.Strings;
import center.task.AProcessor;
import center.task.CalculateException;
import center.task.Context;
import center.task.State;
import center.task.TaskException;

public class SMTPProcessor extends AProcessor {
	private String FROM;
	private String TO;
	private String CC;
	private String BCC;
	private String SUBJECT;
	private String CHARSET;

	private class DS implements DataSource {
		private ru.vassaev.core.io.OutputByteBuffer out = null;
		private String mime_type = null;
		private String name = null;
    public DS() {}
		public String getContentType() {
			return (mime_type == null) ? "text/html" : mime_type;
		}
		public InputStream getInputStream() throws IOException {
			if (out == null)
				return null;
			return out.getIS();
		}
		public String getName() {
			return name;
		}
		public OutputStream getOutputStream() throws IOException {
			return out;
		}
	}

	public State paramsValidateAndPrepare(Context cntx) throws SysException {
		FROM = cntx.getPrmString("msg/FROM");
		if (FROM == null)
			throw new SysException("There is not set param by name 'msg/CC'");
		TO = cntx.getPrmString("msg/TO");
		if (TO == null)
			throw new SysException("There is not set param by name 'msg/TO'");
		CC = cntx.getPrmString("msg/CC");
		BCC = cntx.getPrmString("msg/BCC");
		SUBJECT = cntx.getPrmString("msg/SUBJECT");
		CHARSET = cntx.getPrmNvl("msg/CHARSET", "utf8");
		return null;
	}

	private class Authenticator extends javax.mail.Authenticator {
		private final String user;
		private final String pwd;

		public Authenticator(String user, String pwd) {
			this.user = user;
			this.pwd = pwd;
		}

		protected PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(user, pwd);
		}
	}

	public State process(Context cntx) throws TaskException, SysException,
			InterruptedException {
		State st = paramsValidateAndPrepare(cntx);
		if (st != null)
			return st;
		Properties props = new Properties();
		Map<String, Object> prms = cntx.getGroupParams("mail.smtp");
		for (Map.Entry<String, Object> e : prms.entrySet()) {
			Object v = e.getValue();
			if (Null.equ(v))
				continue;
			String[] fn = cntx.getFullName(e.getKey());
			
			if (v instanceof center.task.prm.Type)
				try {
					v = ((center.task.prm.Type)v).getValue(cntx);
				} catch (CalculateException e1) {
					e1.printStackTrace();
				}
			v = Strings.getString(v);
			if (fn[1] == null || Null.equ(v))
				continue;
			props.put("mail.smtp." + fn[1], v);
		}
		String user = props.getProperty("mail.smtp.user");
		String pwd = props.getProperty("mail.smtp.pwd");

		Session session = Session.getDefaultInstance(props, new Authenticator(user,
				pwd));

		try {
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(FROM));
			msg.setSender(new InternetAddress(FROM));

			String[] to = new String[0];
			if (TO != null)
				to = Strings.parseXVSLine(TO, '\\', ',');
			String[] cc = new String[0];
			if (CC != null)
				cc = Strings.parseXVSLine(CC, '\\', ',');
			String[] bcc = new String[0];
			if (BCC != null)
				bcc = Strings.parseXVSLine(BCC, '\\', ',');

			for (int i = 0; i < to.length; i++) 
				msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to[i]));

			for (int i = 0; i < cc.length; i++) 
				msg.addRecipient(Message.RecipientType.CC, new InternetAddress(cc[i]));

			for (int i = 0; i < bcc.length; i++) 
				msg.addRecipient(Message.RecipientType.BCC, new InternetAddress(bcc[i]));

			msg.setSubject(SUBJECT, CHARSET);
			msg.setSentDate(new java.util.Date());
			Multipart mp = new MimeMultipart();

			Map<Integer, DS> body = new HashMap<Integer, DS>();
			//--------------------------
			Map<String, Object> msgs = cntx.getGroupParams("msg"); 
			for (Map.Entry<String, Object> ent : msgs.entrySet()) { 
				Object v = ent.getValue(); 
				if (v == null) 
					continue;
				String k = ent.getKey();
				String s = "msg/BODY[";
				if (k.indexOf(s) == 0) {
					int l = s.length();
					int j = k.indexOf(']', l);
					Integer index = Strings.parseInteger(k.substring(l, j));
					if (index == null)
						continue;
					s = k.substring(j);
					DS ds = body.get(index);
					if ("].mime".equals(s)) {
						if (ds == null) {
							ds = new DS();
							body.put(index, ds);
						}
						ds.mime_type = cntx.getString(v);
					} else if ("].data".equals(s)) {
						if (ds == null) {
							ds = new DS();
							body.put(index, ds);
						}
						ds.out = cntx.getOutputByteBuffer(v, CHARSET);
					} else if("].name".equals(s)) {
						if (ds == null) {
							ds = new DS();
							body.put(index, ds);
						}
						ds.name = cntx.getString(v);
					}
				}
			}
			for (Map.Entry<Integer, DS> ent : body.entrySet()) {
				MimeBodyPart mbp = new MimeBodyPart();
				DS ds = ent.getValue();
				mbp.setDataHandler(new DataHandler(ds));
				String n = ds.getName();
				if (n != null && n.length() > 0) {
					mbp.setDescription(n);
					mbp.setFileName(n);
				}
				mp.addBodyPart(mbp);
			}
			//--------------------------
			msg.setContent(mp);
			synchronized (Transport.class) {
				Transport t = session.getTransport("smtp");
				Transport.send(msg);
				t.close();
			}
		} catch (Exception ex) {
			throw new SysException(ex);
		}
		return State.DONE_OK;
	}

	public Set<String> dependentOn() {
		// TODO Auto-generated method stub
		return null;
	}

	public Set<String> loopDependentOn() {
		// TODO Auto-generated method stub
		return null;
	}

}
