package center.db;

import java.sql.DatabaseMetaData;

/**
 *
 * @author Vassaev A.V.
 * @version 1.0 01/01/2005
 */
public class DBInfo {
    public DBInfo() {
    }
    //*
    public static final void showStaticConnectionInfo (DatabaseMetaData dm) {
      StringBuffer sb = new StringBuffer();
      sb.append("Retrieves whether the current user can call all " +
              "the procedures returned by the method getProcedures.\t");
      try {
        sb.append(dm.allProceduresAreCallable());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether the current user can use all the " +
              "tables returned by the method getTables in a SELECT statement.\t");
      try {
        sb.append(dm.allTablesAreSelectable());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether a data definition statement within a " +
              "transaction forces the transaction to commit.\t");
      try {
        sb.append(dm.dataDefinitionCausesTransactionCommit());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database ignores a data definition " +
              "statement within a transaction.\t");
      try {
        sb.append(dm.dataDefinitionIgnoredInTransactions());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether the return value for the method " +
              "getMaxRowSize includes the SQL data types " +
              "LONGVARCHAR and LONGVARBINARY.\t");
      try {
        sb.append(dm.doesMaxRowSizeIncludeBlobs());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the String that this database uses as the separator " +
              "between a catalog and table name.\t");
      try {
        sb.append(dm.getCatalogSeparator());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the database vendor's " +
              "preferred term for \"catalog\".\t");
      try {
        sb.append(dm.getCatalogTerm());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the major version number of " +
              "the underlying database.\t");
      try {
        sb.append(dm.getDatabaseMajorVersion());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the minor version number of the " +
              "underlying database.\t");
      try {
        sb.append(dm.getDatabaseMinorVersion());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the name of this database product.\t");
      try {
        sb.append(dm.getDatabaseProductName());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the version number of this database product.\t");
      try {
        sb.append(dm.getDatabaseProductVersion());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves this database's default transaction " +
              "isolation level.\t");
      try {
        sb.append(dm.getDefaultTransactionIsolation());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves this JDBC driver's major version number.\t");
      try {
        sb.append(dm.getDriverMajorVersion());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves this JDBC driver's minor version number.\t");
      try {
        sb.append(dm.getDriverMinorVersion());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the name of this JDBC driver.\t");
      try {
        sb.append(dm.getDriverName());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the version number of this " +
              "JDBC driver as a String.\t");
      try {
        sb.append(dm.getDriverVersion());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves all the \"extra\" characters that can be used in" +
         " unquoted identifier names (those beyond a-z, A-Z, 0-9 and _).\t");
      try {
        sb.append(dm.getExtraNameCharacters());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the string used to quote SQL identifiers.\t");
      try {
        sb.append(dm.getIdentifierQuoteString());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the major JDBC version number for this driver.\t");
      try {
        sb.append(dm.getJDBCMajorVersion());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the minor JDBC version number for this driver.\t");
      try {
        sb.append(dm.getJDBCMinorVersion());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the maximum number of hex characters this database " +
              "allows in an inline binary literal.\t");
      try {
        sb.append(dm.getMaxBinaryLiteralLength());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the maximum number of characters that this database " +
              "allows in a catalog name.\t");
      try {
        sb.append(dm.getMaxCatalogNameLength());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the maximum number of characters this database " +
              "allows for a character literal.\t");
      try {
        sb.append(dm.getMaxCharLiteralLength());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the maximum number of characters this database " +
              "allows for a column name.\t");
      try {
        sb.append(dm.getMaxColumnNameLength());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the maximum number of columns this database " +
              "allows in a GROUP BY clause.\t");
      try {
        sb.append(dm.getMaxColumnsInGroupBy());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the maximum number of columns this database " +
              "allows in an index.\t");
      try {
        sb.append(dm.getMaxColumnsInIndex());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the maximum number of columns this database " +
              "allows in an ORDER BY clause.\t");
      try {
        sb.append(dm.getMaxColumnsInOrderBy());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the maximum number of columns this database " +
              "allows in a SELECT list.\t");
      try {
        sb.append(dm.getMaxColumnsInSelect());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the maximum number of columns this database " +
              "allows in a table.\t");
      try {
        sb.append(dm.getMaxColumnsInTable());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the maximum number of concurrent connections to " +
              "this database that are possible.\t");
      try {
        sb.append(dm.getMaxConnections());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the maximum number of characters that this " +
              "database allows in a cursor name.\t");
      try {
        sb.append(dm.getMaxCursorNameLength());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the maximum number of bytes this database " +
              "allows for an index, including all of the parts of the index.\t");
      try {
        sb.append(dm.getMaxIndexLength());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the maximum number of characters that this database " +
              "allows in a procedure name.\t");
      try {
        sb.append(dm.getMaxProcedureNameLength());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the maximum number of bytes this database " +
              "allows in a single row.\t");
      try {
        sb.append(dm.getMaxRowSize());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the maximum number of characters that this " +
              "database allows in a schema name.\t");
      try {
        sb.append(dm.getMaxSchemaNameLength());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the maximum number of characters this database " +
              "allows in an SQL statement.\t");
      try {
        sb.append(dm.getMaxStatementLength());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the maximum number of active statements to this " +
              "database that can be open at the same time.\t");
      try {
        sb.append(dm.getMaxStatements());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the maximum number of characters this database " +
              "allows in a table name.\t");
      try {
        sb.append(dm.getMaxTableNameLength());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the maximum number of tables this database " +
              "allows in a SELECT statement.\t");
      try {
        sb.append(dm.getMaxTablesInSelect());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the maximum number of characters this database " +
              "allows in a user name.\t");
      try {
        sb.append(dm.getMaxUserNameLength());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves a comma-separated list of math functions " +
              "available with this database.\t");
      try {
        sb.append(dm.getNumericFunctions());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the database vendor's " +
              "preferred term for \"procedure\".\t");
      try {
        sb.append(dm.getProcedureTerm());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the default holdability of this " +
              "ResultSet object.\t");
      try {
        sb.append(dm.getResultSetHoldability());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the database vendor's preferred term " +
              "for \"schema\".\t");
      try {
        sb.append(dm.getSchemaTerm());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the string that can be used to " +
              "escape wildcard characters.\t");
      try {
        sb.append(dm.getSearchStringEscape());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves a comma-separated list of all of this " +
              "database's SQL keywords that are NOT also SQL92 keywords.\t");
      try {
        sb.append(dm.getSQLKeywords());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Indicates whether the SQLSTATE returned by " +
              "SQLException.getSQLState is X/Open (now known as Open Group) " +
              "SQL CLI or SQL99.\t");
      try {
        sb.append(dm.getSQLStateType());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves a comma-separated list of string functions available " +
              "with this database.\t");
      try {
        sb.append(dm.getStringFunctions());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves a comma-separated list of system functions available " +
              "with this database.\t");
      try {
        sb.append(dm.getSystemFunctions());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the URL for this DBMS.\t");
      try {
        sb.append(dm.getURL());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves the user name as known to this database.\t");
      try {
        sb.append(dm.getUserName());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether a catalog appears at the start " +
              "of a fully qualified table name.\t");
      try {
        sb.append(dm.isCatalogAtStart());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database is in read-only mode.\t");
      try {
        sb.append(dm.isReadOnly());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Indicates whether updates made to a LOB are " +
              "made on a copy or directly to the LOB.\t");
      try {
        sb.append(dm.locatorsUpdateCopy());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports concatenations " +
              "between NULL and non-NULL values being NULL.\t");
      try {
        sb.append(dm.nullPlusNonNullIsNull());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether NULL values are sorted " +
              "at the end regardless of sort order.\t");
      try {
        sb.append(dm.nullsAreSortedAtEnd());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether NULL values are sorted at the start " +
              "regardless of sort order.\t");
      try {
        sb.append(dm.nullsAreSortedAtStart());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether NULL values are sorted high.\t");
      try {
        sb.append(dm.nullsAreSortedHigh());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether NULL values are sorted low.\t");
      try {
        sb.append(dm.nullsAreSortedLow());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database treats mixed " +
              "case unquoted SQL identifiers as case insensitive " +
              "and stores them in lower case.\t");
      try {
        sb.append(dm.storesLowerCaseIdentifiers());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database treats mixed " +
              "case quoted SQL identifiers as case insensitive " +
              "and stores them in lower case.\t");
      try {
        sb.append(dm.storesLowerCaseQuotedIdentifiers());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database treats mixed " +
              "case unquoted SQL identifiers as case insensitive and " +
              "stores them in mixed case.\t");
      try {
        sb.append(dm.storesMixedCaseIdentifiers());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database treats mixed case " +
              "quoted SQL identifiers as case insensitive and " +
              "stores them in mixed case.\t");
      try {
        sb.append(dm.storesMixedCaseQuotedIdentifiers());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database treats mixed case unquoted SQL " +
              "identifiers as case insensitive and stores them in upper case.\t");
      try {
        sb.append(dm.storesUpperCaseIdentifiers());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database treats mixed case quoted SQL " +
              "identifiers as case insensitive and stores them in upper case.\t");
      try {
        sb.append(dm.storesUpperCaseQuotedIdentifiers());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports ALTER TABLE with " +
              "add column.\t");
      try {
        sb.append(dm.supportsAlterTableWithAddColumn());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports ALTER TABLE with " +
              "drop column.\t");
      try {
        sb.append(dm.supportsAlterTableWithDropColumn());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports the ANSI92 entry " +
              "level SQL grammar.\t");
      try {
        sb.append(dm.supportsANSI92EntryLevelSQL());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports the ANSI92 full SQL " +
              "grammar supported.\t");
      try {
        sb.append(dm.supportsANSI92FullSQL());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports the ANSI92 " +
              "intermediate SQL grammar supported.\t");
      try {
        sb.append(dm.supportsANSI92IntermediateSQL());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports batch updates.\t");
      try {
        sb.append(dm.supportsBatchUpdates());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether a catalog name can be used in a data " +
              "manipulation statement.\t");
      try {
        sb.append(dm.supportsCatalogsInDataManipulation());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether a catalog name can be used in an index " +
              "definition statement.\t");
      try {
        sb.append(dm.supportsCatalogsInIndexDefinitions());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether a catalog name can be used in a privilege " +
              "definition statement.\t");
      try {
        sb.append(dm.supportsCatalogsInPrivilegeDefinitions());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether a catalog name can be used in a procedure " +
              "call statement.\t");
      try {
        sb.append(dm.supportsCatalogsInProcedureCalls());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether a catalog name can be used in a table " +
              "definition statement.\t");
      try {
        sb.append(dm.supportsCatalogsInTableDefinitions());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports column aliasing.\t");
      try {
        sb.append(dm.supportsColumnAliasing());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports the CONVERT " +
              "function between SQL types.\t");
      try {
        sb.append(dm.supportsConvert());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports the ODBC Core " +
              "SQL grammar.\t");
      try {
        sb.append(dm.supportsCoreSQLGrammar());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports correlated " +
              "subqueries.\t");
      try {
        sb.append(dm.supportsCorrelatedSubqueries());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports both data definition " +
              "and data manipulation statements within a transaction.\t");
      try {
        sb.append(dm.supportsDataDefinitionAndDataManipulationTransactions());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports only data " +
              "manipulation statements within a transaction.\t");
      try {
        sb.append(dm.supportsDataManipulationTransactionsOnly());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether, when table correlation names are " +
              "supported, they are restricted to being different from " +
              "the names of the tables.\t");
      try {
        sb.append(dm.supportsDifferentTableCorrelationNames());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports expressions in " +
              "ORDER BY lists.\t");
      try {
        sb.append(dm.supportsExpressionsInOrderBy());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports the ODBC Extended " +
              "SQL grammar.\t");
      try {
        sb.append(dm.supportsExtendedSQLGrammar());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports full nested " +
              "outer joins.\t");
      try {
        sb.append(dm.supportsFullOuterJoins());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether auto-generated keys can be retrieved " +
              "after a statement has been executed.\t");
      try {
        sb.append(dm.supportsGetGeneratedKeys());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports some form of " +
              "GROUP BY clause.\t");
      try {
        sb.append(dm.supportsGroupBy());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports using columns not " +
              "included in the SELECT statement in a GROUP BY clause provided " +
              "that all of the columns in the SELECT statement are included " +
              "in the GROUP BY clause.\t");
      try {
        sb.append(dm.supportsGroupByBeyondSelect());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports using a column that " +
              "is not in the SELECT statement in a GROUP BY clause.\t");
      try {
        sb.append(dm.supportsGroupByUnrelated());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports the SQL Integrity " +
              "Enhancement Facility.\t");
      try {
        sb.append(dm.supportsIntegrityEnhancementFacility());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports specifying a " +
              "LIKE escape clause.\t");
      try {
        sb.append(dm.supportsLikeEscapeClause());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database provides limited " +
              "support for outer joins.\t");
      try {
        sb.append(dm.supportsLimitedOuterJoins());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports the ODBC " +
              "Minimum SQL grammar.\t");
      try {
        sb.append(dm.supportsMinimumSQLGrammar());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database treats mixed case unquoted " +
              "SQL identifiers as case sensitive and as a result stores them " +
              "in mixed case.\t");
      try {
        sb.append(dm.supportsMixedCaseIdentifiers());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database treats mixed case quoted SQL " +
              "identifiers as case sensitive and as a result stores " +
              "them in mixed case.\t");
      try {
        sb.append(dm.supportsMixedCaseQuotedIdentifiers());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether it is possible to have multiple " +
              "ResultSet objects returned from a CallableStatement " +
              "object simultaneously.\t");
      try {
        sb.append(dm.supportsMultipleOpenResults());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports getting " +
              "multiple ResultSet objects from a single call to the " +
              "method execute.\t");
      try {
        sb.append(dm.supportsMultipleResultSets());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database allows having multiple " +
              "transactions open at once (on different connections).\t");
      try {
        sb.append(dm.supportsMultipleTransactions());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports named parameters " +
              "to callable statements.\t");
      try {
        sb.append(dm.supportsNamedParameters());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether columns in this database may be defined " +
              "as non-nullable.\t");
      try {
        sb.append(dm.supportsNonNullableColumns());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports keeping cursors open " +
              "across commits.\t");
      try {
        sb.append(dm.supportsOpenCursorsAcrossCommit());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports keeping cursors " +
              "open across rollbacks.\t");
      try {
        sb.append(dm.supportsOpenCursorsAcrossRollback());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports keeping statements " +
              "open across commits.\t");
      try {
        sb.append(dm.supportsOpenStatementsAcrossCommit());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports keeping " +
              "statements open across rollbacks.\t");
      try {
        sb.append(dm.supportsOpenStatementsAcrossRollback());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports using a column " +
              "that is not in the SELECT statement in an ORDER BY clause.\t");
      try {
        sb.append(dm.supportsOrderByUnrelated());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports some " +
              "form of outer join.\t");
      try {
        sb.append(dm.supportsOuterJoins());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports " +
              "positioned DELETE statements.\t");
      try {
        sb.append(dm.supportsPositionedDelete());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports positioned UPDATE statements.\t");
      try {
        sb.append(dm.supportsPositionedUpdate());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports savepoints.\t");
      try {
        sb.append(dm.supportsSavepoints());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether a schema name can be used in " +
              "a data manipulation statement.\t");
      try {
        sb.append(dm.supportsSchemasInDataManipulation());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether a schema name can be used in an index " +
              "definition statement.\t");
      try {
        sb.append(dm.supportsSchemasInIndexDefinitions());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether a schema name can be used in a privilege " +
              "definition statement.\t");
      try {
        sb.append(dm.supportsSchemasInPrivilegeDefinitions());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether a schema name can be used in a procedure " +
              "call statement.\t");
      try {
        sb.append(dm.supportsSchemasInProcedureCalls());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether a schema name can be used in a table " +
              "definition statement.\t");
      try {
        sb.append(dm.supportsSchemasInTableDefinitions());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports " +
              "SELECT FOR UPDATE statements.\t");
      try {
        sb.append(dm.supportsSelectForUpdate());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports statement pooling.\t");
      try {
        sb.append(dm.supportsStatementPooling());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports stored " +
              "procedure calls that use the stored procedure escape syntax.\t");
      try {
        sb.append(dm.supportsStoredProcedures());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports subqueries in " +
              "comparison expressions.\t");
      try {
        sb.append(dm.supportsSubqueriesInComparisons());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports subqueries in " +
              "EXISTS expressions.\t");
      try {
        sb.append(dm.supportsSubqueriesInExists());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports subqueries " +
              "in IN statements.\t");
      try {
        sb.append(dm.supportsSubqueriesInIns());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports subqueries in " +
              "quantified expressions.\t");
      try {
        sb.append(dm.supportsSubqueriesInQuantifieds());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports table " +
              "correlation names.\t");
      try {
        sb.append(dm.supportsTableCorrelationNames());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports transactions.\t");
      try {
        sb.append(dm.supportsTransactions());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports SQL UNION.\t");
      try {
        sb.append(dm.supportsUnion());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database supports SQL UNION ALL.\t");
      try {
        sb.append(dm.supportsUnionAll());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database uses a file for each table.\t");
      try {
        sb.append(dm.usesLocalFilePerTable());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
      sb.append("Retrieves whether this database stores " +
              "tables in a local file.\t");
      try {
        sb.append(dm.usesLocalFiles());
      } catch(Throwable ex) {
        sb.append(ex.toString());
      }
      System.out.println(sb.toString()); sb.setLength(0);
    }
    //*/
}
