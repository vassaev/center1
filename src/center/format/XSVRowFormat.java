package center.format;

import javax.swing.table.TableModel;
import ru.vassaev.core.util.Strings;

/**
 * @author Vassaev A.V.
 * @version 1.0
 */
public class XSVRowFormat implements IRowFormat {
    private char sys = '\\';
    private char div;
    public XSVRowFormat(char div) {
        this.div = div;
    }
    public String format(TableModel tm, int row) {
        StringBuffer sb = new StringBuffer();
        int l = tm.getColumnCount();
        for (int j = 0; j < l; j++) {
            Object obj = tm.getValueAt(row, j);
            if (obj != null)
                Strings.addXVSFieldTo(sb, obj.toString(), sys, div);
            else
                Strings.addXVSFieldTo(sb, null, sys, div);
        }
        return sb.toString();
    }

    public void load(TableModel tm, int row, String str) {
        String[] vals = Strings.parseXVSLine(str, sys, div);
        int l = vals.length;
        for (int j = 0; j < l; j++) {
            tm.setValueAt(vals[j], row, j);
        }
    }

    public String format(TableModel tm, int row, int[] clmns) {
        StringBuffer sb = new StringBuffer();
        int l = clmns.length;
        for (int j = 0; j < l; j++) {
            Object obj = tm.getValueAt(row, clmns[j]);
            if (obj != null)
                Strings.addXVSFieldTo(sb, obj.toString(), sys, div);
            else
                Strings.addXVSFieldTo(sb, null, sys, div);
        }
        return sb.toString();
    }

    public void load(TableModel tm, int row, String str, int[] clmns) {
        String[] vals = Strings.parseXVSLine(str, sys, div);
        int l = vals.length;
        for (int j = 0; j < l; j++) {
            tm.setValueAt(vals[j], row, clmns[j]);
        }
    }
}
