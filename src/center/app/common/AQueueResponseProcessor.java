package center.app.common;

import ru.vassaev.core.CallBackInterface;
import ru.vassaev.core.Pool;
import ru.vassaev.core.Queue;
import ru.vassaev.core.base.Null;
import ru.vassaev.core.container.AppInfo;
import ru.vassaev.core.container.ApplicationManager;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.thread.Processed;
import center.task.AProcessor;
import center.task.Context;
import center.task.State;
import center.task.TaskException;

import java.util.Set;
import java.util.HashSet;

public abstract class AQueueResponseProcessor<T> extends AProcessor {

	private String poolProcessName = null;
	private Pool<ru.vassaev.core.thread.Process> prcs = null;
	private String dateFormat = null;

	/**
	 * Проверка установки параметров
	 * @throws SysException 
	 */
	public void paramsValidateAndPrepare(Context cntx) throws SysException {
		// Имя пула потоков, которое используется для разбора очереди
		poolProcessName = cntx.getPrmString("process_pool");
		if (Null.equ(poolProcessName))
			throw new SysException("Parameter process_pool isn't set");
		ru.vassaev.core.thread.Process prc = 
			ru.vassaev.core.thread.Process.currentProcess();
		AppInfo ai = ApplicationManager.getCurrentAppInfo();
		Pool<?> poolprc = ai.getContainer().getRM().getPool(poolProcessName);
		if (Null.equ(poolprc))
			throw new SysException("There is no pool by name " + poolProcessName);
		try {
			prcs = (Pool<ru.vassaev.core.thread.Process>) poolprc;
		} catch(Throwable e) {
			throw new SysException("The pool by name " + poolProcessName + " dosn't contain process' object");
		}
		
		dateFormat = cntx.getPrmString("date_format");
		if (dateFormat == null)
			dateFormat = "dd.MM.yyyy HH:mm:ss.SSS";
	}
	
	public State process(Context cntx) throws TaskException, SysException,
			InterruptedException {
		paramsValidateAndPrepare(cntx);
		//processing_read_objects_and_put_queue(q, prms);
		return State.DONE_OK;
	}

	private class TargetUpdate implements Processed<T>
	, CallBackInterface<ru.vassaev.core.thread.Process, Object> {

		/**
		 * Обработка в потоке
		 */
		public void processing(T prms) throws Throwable {
			processing_update(prms);
		}

		/**
		 * Вернуть в пул поток после его завершения
		 */
		public Object callBack(ru.vassaev.core.thread.Process prc) {
			prcs.free(prc);
			return null;
		}
		
	}
	/**
	 * Чтение и запись в очередь объектов
	 * Метод вызывается в отдельном потоке
	 * @param q - очередь
	 * @param prms - параметры
	 */
	public void processing_read_objects_and_put_queue(Queue<T> q, Object prms) {
	}

	public void processing_update(T prms) {
		
	}
  public Set<String> dependentOn() {
    Set<String> s = new HashSet<String>();
    return s;
  }

  public Set<String> loopDependentOn() {
    Set<String> s = new HashSet<String>();
    return s;
  }
}
