package center.task.api.http;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import ru.vassaev.core.base.Null;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.io.OutputByteBuffer;
import ru.vassaev.core.xml.XMLFileWaiter;

public final class Handler {

	public static Object getObjectByElement(String e) throws SysException {
		String type = e.substring(0, 3);
		if ("<n>".equals(type))// NULL
			return Null.getInstance();
		String s = e.substring(3);
		if ("<s>".equals(type))// String
			return ru.vassaev.core.util.Util.parseBase64String(s, "utf-8");
		if ("<d>".equals(type)) {// Double
			return Double.longBitsToDouble(Long.parseLong(s, 16));
		}
		if ("<f>".equals(type)) {// Float
			return Float.intBitsToFloat(Integer.parseInt(s, 16));
		}
		if ("<l>".equals(type)) {// Long
			return Long.parseLong(s, 16);
		}
		if ("<i>".equals(type)) {// Integer
			return Integer.parseInt(s, 16);
		}
		if ("<t>".equals(type)) {// Date, Timestamp
			int i = s.indexOf('.');
			if (i >= 0) {
				Long t = Long.parseLong(s.substring(0, i), 16);
				Timestamp d = new Timestamp(t);
				Integer n = Integer.parseInt(s.substring(i + 1), 16);
				d.setNanos(n);
				return d;
			} else {
				Long t = Long.parseLong(s, 16);
				Date d = new Date(t);
				return d;
			}
		}
		if ("<b>".equals(type)) { // Stream
			OutputByteBuffer out = new OutputByteBuffer(1024*1024);
			try {
				byte[] r = ru.vassaev.core.util.Util.parseBase64(s);
				out.write(r);
				out.close();
				return out;
			} catch (SysException e1) {
				throw new SysException(e1);
			} catch (IOException e1) {
				throw new SysException(e1);
			}
		}
		if ("<u>".equals(type)) { // Unknown
			OutputByteBuffer out = new OutputByteBuffer(1024*1024);
			try {
				byte[] r = ru.vassaev.core.util.Util.parseBase64(s);
				//if (r.length > 8000)
				//	System.out.println("!!! LENGTH = " + r.length);
				out.write(r);
				out.close();
				return getObject(out);
			} catch (SysException e1) {
				throw new SysException(e1);
			} catch (IOException e1) {
				throw new SysException(e1);
			}
		}
		if ("<x>".equals(type)) { // Stream
			OutputByteBuffer out = new OutputByteBuffer(1024*1024);
			try {
				byte[] r = ru.vassaev.core.util.Util.parseBase64(s);
				out.write(r);
				out.close();
				Document d = XMLFileWaiter.getDocument(out.getIS(), "utf-8");
				return d.getDocumentElement();
			} catch (IOException e1) {
				throw new SysException(e1);
			}
		}
		return null;
	}

	public static StringBuffer getElementByObject(StringBuffer e, Object o)
			throws SysException {
		Class<?> c;
		if (Null.equ(o)) {
			e = e.append("<n>");
		} else if (o instanceof String || o instanceof StringBuffer) {
			e = e.append("<s>");
			e.append(ru.vassaev.core.util.Util.getBase64String(o.toString(), "utf-8"));
		} else if (Double.class.equals((c = o.getClass()))) {
			long v = Double.doubleToLongBits((Double) o);
			e = e.append("<d>");
			e.append(Long.toHexString(v));
		} else if (Float.class.equals(c)) {
			int v = Float.floatToIntBits((Float) o);
			e = e.append("<f>");
			e.append(Integer.toHexString(v));
		} else if (Long.class.equals(c)) {
			Long v = (Long) o;
			e = e.append("<l>");
			e.append(Long.toHexString(v));
		} else if (Integer.class.equals(c)) {
			Integer v = (Integer) o;
			e = e.append("<i>");
			e.append(Integer.toHexString(v));
		} else if (Timestamp.class.equals(c)) {
			Timestamp v = (Timestamp) o;
			e = e.append("<t>");
			e.append(Long.toHexString(v.getTime()));
			if (v.getNanos() > 0)
				e.append(".").append(Integer.toHexString(v.getNanos()));
		} else if (Date.class.equals(c)) {
			Date v = (Date) o;
			e = e.append("<t>");
			e.append(Long.toHexString(v.getTime()));
		} else if (OutputByteBuffer.class.equals(c))
			try {
				OutputByteBuffer v = (OutputByteBuffer) o;
				InputStream is = v.getIS();
				OutputByteBuffer os = new OutputByteBuffer(
						(int) (v.getLength() * 4.0 / 3.0 + 5.0));
				ru.vassaev.core.util.Util.getBase64String(is, os);
				os.close();
				is.close();
				StringBuffer sb = new StringBuffer();
				is = os.getIS();
				int cnt;
				byte[] b = new byte[8 * 1024];
				while ((cnt = is.read(b)) != -1) {
					sb.append(new String(b, 0, cnt, "utf-8"));
				}
				is.close();
				e = e.append("<b>");
				e.append(sb);
			} catch (IOException e1) {
				throw new SysException(e1);
			}
		else if (o instanceof Element) try {
			Element val = (Element)o;
			OutputByteBuffer v = new OutputByteBuffer(1024*1024);
			XMLFileWaiter.putStream(val, v, "utf-8");
			v.close();
			InputStream is = v.getIS();
			InputStream ib;
			OutputStream ob;
//			if (v.getLength() > 10000) {
//			  File f = new File("F:\\PROJECTS\\CENTER\\MA\\tmp\\" + System.currentTimeMillis() + ".xml");
//			  f.createNewFile();
//			  ob = new FileOutputStream(f);
//			  ib = new FileInputStream(f);
//			} else 
			{
				OutputByteBuffer os = new OutputByteBuffer(
					(int) (1024*1024));
			  ob = os;
				ib = os.getIS();
			}
			ru.vassaev.core.util.Util.getBase64String(is, ob);
			ob.close();
			is.close();
			StringBuffer sb = new StringBuffer();
			int cnt;
			byte[] b = new byte[8 * 1024];
			while ((cnt = ib.read(b)) != -1) {
				sb.append(new String(b, 0, cnt, "utf-8"));
			}
			is.close();
			e = e.append("<x>");
			e.append(sb);
		} catch (IOException e1) {
			throw new SysException(e1);
		}
		else
			try {
				OutputByteBuffer v = getStream(o);
				InputStream is = v.getIS();
				OutputByteBuffer os = new OutputByteBuffer(
						(int) (v.getLength() * 4.0 / 3.0 + 5.0));
				ru.vassaev.core.util.Util.getBase64String(is, os);
				os.close();
				is.close();
				StringBuffer sb = new StringBuffer();
				is = os.getIS();
				int cnt;
				byte[] b = new byte[8 * 1024];
				while ((cnt = is.read(b)) != -1) {
					sb.append(new String(b, 0, cnt, "utf-8"));
				}
				is.close();
				e = e.append("<u>");
				e.append(sb);
				//if (sb.length() > 8000) {
				//	System.out.println("!!! LENGTH = " + sb.length());
				//	System.out.println("!!! CLASS = " + o.getClass());
				//}
			} catch (IOException e1) {
				throw new SysException(e1);
			}
		return e;
	}
	
	private static OutputByteBuffer getStream(Object obj) throws SysException {
		OutputByteBuffer obb = new OutputByteBuffer(1024*1024);
		ObjectOutputStream oos = null;
		try {
			oos = new ObjectOutputStream(obb);
			oos.writeObject(obj);
			return obb;
		} catch (IOException e) {
			throw new SysException(e);
		} finally {
			if (oos != null) {
				try {
					oos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			try {
				obb.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	private static Object getObject(OutputByteBuffer obj) throws SysException {
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(obj.getIS());
			return ois.readObject();
		} catch (IOException e) {
			throw new SysException(e);
		} catch (ClassNotFoundException e) {
			throw new SysException(e);
		} finally {
			if (ois != null) {
				try {
					ois.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
  public static Object[] getObjects(StringBuffer sb) throws SysException {
    int s = -1;
    ArrayList<Object> r = new ArrayList<Object>();
    for (int i = 0; i < sb.length(); i++) {
    	if (s == -1) {
    		if ('<' == sb.charAt(i)) {
    			s = i;
    			continue;
    		}
    	}
  		if ('<' == sb.charAt(i)) {
  			r.add(getObjectByElement(sb.substring(s, i)));
  			s = i;
  			continue;
  		}
    }
    if (s >= 0)
      r.add(getObjectByElement(sb.substring(s)));
    return r.toArray();
  }
  public static void main(String args[]) throws SysException {
  	Document x = XMLFileWaiter.getDocument(new File("F:\\PROJECTS\\CENTER\\MA\\modules\\common\\java.ma.open_session2.xml"), "cp1251");
  	Element y = x.getDocumentElement();
  	StringBuffer sb = new  StringBuffer();
  	Handler.getElementByObject(sb, y);
  	Element z = (Element) Handler.getObjectByElement(sb.toString());
  	
  }
}
