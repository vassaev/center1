package center.app.common;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Set;

import ru.vassaev.core.Pool;
import ru.vassaev.core.PrmInterface;
import ru.vassaev.core.TimeoutInputStream;
import ru.vassaev.core.base.Null;
import ru.vassaev.core.container.ApplicationManager;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.io.ByteMsg;
import ru.vassaev.core.thread.PoolThread;
import ru.vassaev.core.thread.Process;
import ru.vassaev.core.thread.Processed;
import ru.vassaev.core.util.Strings;
import center.task.AProcessor;
import center.task.Context;
import center.task.Record;
import center.task.State;
import center.task.TaskException;

public class ByteMsgFlowQueryProcessor extends AProcessor {

	private final Object sync = new Object();
	private PoolThread prcs;
	private Pool<ByteMsg> poolmsg;
	private String host;
	private int port;

	/**
	 * Чтение параметров и проверка параметров
	 * 
	 * @param cntx
	 *          - текущий контекст
	 * @throws SysException
	 *           - в случае недостатка параметров или невалидных значений
	 */
	public State paramsValidateAndPrepare(Context cntx) throws SysException {
		String poolProcessName = cntx.getPrmString("process_pool");
		if (Null.equ(poolProcessName))
			throw new SysException("Parameter process_pool isn't set");
		prcs = (PoolThread) ApplicationManager.getPoolByName(poolProcessName,
				Process.class);
		// Определение пула контейнеров сообщений
//*
		String poolMessageName = cntx.getPrmString("message_pool");
		if (Null.equ(poolMessageName))
			throw new SysException("Parameter message_pool isn't set");
		poolmsg = (Pool<ByteMsg>) ApplicationManager.getPoolByName(poolMessageName,
				ByteMsg.class);
		if (poolmsg == null)
			throw new SysException("There is no message pool by name '"
					+ poolMessageName + "'");
//*/
		host = cntx.getPrmString("host");
		port = Integer.parseInt(cntx.getPrmString("port"));
		return null;
	}

	private long getTimeout(Context cntx) throws SysException {
		long timeout = cntx.getPrmNvl("timeout", 30000L);
		if (timeout < 0)
			timeout = 30000;
		return timeout;
	}
	
	private class ReadMsg implements Processed<InputStream> {
		private final ByteMsg msg;
		private ByteMsg rmsg;

		public ReadMsg(ByteMsg msg) {
			super();
			this.msg = msg;
		}

		public void processing(InputStream is) throws Throwable {
			ByteMsg.receiveMsg(is, msg);
			synchronized (ReadMsg.this) {
				rmsg = msg;
			}
			synchronized (sync) {
				sync.notifyAll();
			}
		}

		public ByteMsg getResultMsg() {
			synchronized (ReadMsg.this) {
				return rmsg;
			}
		}
	}

	public State process(Context cntx) throws TaskException, SysException,
			InterruptedException {
		State st = paramsValidateAndPrepare(cntx);
		if (st != null)
			return st;
		Socket s = null;
		int catch_wait = 0;
		int catch_done = 0;
		int send_wait = 0;
		int send_done = 0;
		int connect = 0;
		boolean need_catch = true;
		boolean need_send = true;
		Object v = null;
		ru.vassaev.core.thread.Process prcr = null;
		TimeoutInputStream tis = null;
		ByteMsg msg = null;
		try {
			ru.vassaev.core.thread.Process prc = Process.currentProcess();
			// Переменные потока
			prc.regResourceName(catch_wait, "catch_wait");
			prc.regResourceName(send_wait, "send_wait");
			need_catch = cntx.getPrmNvl("need_catch", true);
			if (!need_catch) {
				need_send = cntx.getPrmNvl("need_send", true);
				if (!need_send)
					return State.DONE_OK;
			}
			msg = poolmsg.occupyOrNew();
			//C02 Установить соединение с хостом
			s = new Socket(host, port);
			connect = 1;
			// Переменные потока
			prc.regResourceName(connect, "connect");
			while (need_catch || need_send) {
			  //{C03}
				need_catch = cntx.getPrmNvl("need_catch", true);
				if (need_catch) {
				  //{C05}
					catch_wait++;
					prc.regResourceName(catch_wait, "catch_wait");
					InputStream is = s.getInputStream();
					//TPTPMsg msg = new TPTPMsg();
					msg.reset();
/*
					try{
						prcr = prcs.occupyOrNew();
						tis = new TimeoutInputStream(prcr);
						tis.setTimeout(getTimeout(cntx));
						tis.startReadSource(is);
//*/
						msg = msg.receiveMsg(is, msg);
/*					} finally {
						prcs.free(prcr);
					}
//*/
				  //{C06}
					Record r = new Record();
					PrmInterface prm = msg.getPrmInterface();
					for (String n : prm.getFieldNames()) {
						v = prm.getField(n);
						n = "msg." + n;
						cntx.log(false, n, "=", v);
						r.setObject(n, v);
						prc.regResourceName(v, n);
					}
					prc.regResourceName(r, "msg.*");
					catch_done++;
					prc.regResourceName(catch_done, "catch_done");
				  //{C12}
					cntx.getPrmByFullName("update");// Расчитать
				}
			  //{C07}
				need_send = cntx.getPrmNvl("need_send", true);
				if (need_send) {
					send_wait++;
					prc.regResourceName(send_wait, "send_wait");
					//*
				  //{C08}
					String msg_grp_in = cntx.getPrmNvl("msg_grp_in", "in");
					Map<String, Object> prms = cntx.getGroupParams(msg_grp_in);
					msg.reset();
					//TPTPMsg msg = new TPTPMsg();
					try {
						PrmInterface prmi = msg.getPrmInterface();
						for (Map.Entry<String, Object> prm : prms.entrySet()) {
							String key = prm.getKey();
							String[] fn = cntx.getFullName(key);
							prmi.setField(fn[1], Strings.getString(cntx.getPrmString(key)));
						}
					  //{C10}
						msg.sendTo(s.getOutputStream());

						//msg.sendTo(System.out);
						//System.out.println("\n");
					} finally {
						//poolmsg.free(msg);
					}
					//*/
					send_done++;
					prc.regResourceName(send_done, "send_done");
				}
			}
		} catch (UnknownHostException e) {
			throw new TaskException(State.DONE_ERR, e);
		} catch (IOException e) {
			throw new TaskException(State.DONE_ERR, e);
		} finally {
			poolmsg.free(msg);
			ru.vassaev.core.util.Sockets.close(s);
		}
		return State.DONE_OK;
	}

	@Override
	public Set<String> dependentOn() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<String> loopDependentOn() {
		// TODO Auto-generated method stub
		return null;
	}

}