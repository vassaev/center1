package center.task.prm.alg;


import org.w3c.dom.Element;

import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.exception.SysRuntimeException;
import ru.vassaev.core.util.Strings;
import ru.vassaev.core.types.TimeList;
import center.task.CalculateException;
import center.task.Context;
import center.task.api.ITaskAPI;
import center.task.prm.Alg;
import center.task.prm.Prm;

import java.util.Set;
import java.util.HashSet;

/**
 * Параметр задания - ссылка на другой параметр (Возможно, в другой задаче)
 * Используется в типизованных параметрах
 * @author Vassaev A.V.
 * @version 1.1 18.07.2012
 */
public final class TaskParam extends Alg {

	private Object name; //Имя в таблице задач
	private Object id;   //Идентификатор задачи
	// Возможные значения:
	// --<current/>
	// <NULL/>
	// <parent/>
	// <prm....
	
	public TaskParam(center.task.prm.Type tp, String owner, Element e) {
		super(tp, owner);
		name = Strings.getXMLObject(e, "name");
		if (name instanceof Element) {
			Element el = (Element)name;
			name = Prm.getPrm(el);
		}
		id = Strings.getXMLObject(e, "id");
		if ((id != null) && id instanceof Element) {
			Element el = (Element)id;
			id = Prm.getPrm(el);
			if (id == null) {
				Object val = Strings.getXMLObject(e, "parent");
				if (val != null) {
					id = 0;
				}
			}
		}
	}

	public Object calculate(TimeList tl, Context cntx)  throws CalculateException {
    try {
      tl.addPointTime(TimeList.Type.START);
			ITaskAPI ta = cntx.ta;
			Long tid = null;
      tl.addPointTime(TimeList.Type.WAIT);
			if (id != null)
				if (id instanceof Prm)
					tid = Long.getLong(((Prm) id).getString(cntx));
				else
					tid = Long.getLong(Strings.getString(id));
			if (tid == null)
				tid = cntx.id_task;
			String nm = Strings.getString(name);
      tl.addPointTime(TimeList.Type.END_WAIT);
			if ("id".equalsIgnoreCase(nm))// Если идентификатор задачи, то к сервису не обращаемся 
				return tid;
			if (tid == cntx.id_task)
				try {
					return cntx.getPrm("tsk", nm, 0);
				} catch (CalculateException e) {
					throw e;
				} catch (Throwable e) {
					throw new CalculateException(owner, e);
				}
			return ta.getParamTask(tid, nm);
		} catch (SysException e) {
			throw new CalculateException(owner, e);
    } finally {
      tl.addPointTime(TimeList.Type.END);
    }
	}

	public Set<String> depedentOn() {
    Set<String> s = null;
    if ((id != null) && (id instanceof Prm) && !((Prm)id).isParent) {
      s = new HashSet<String>();
      s.add(((Prm)id).fullname);
    }
    if ((name != null) && (name instanceof Prm) && !((Prm)name).isParent) {
      if (s == null)
        s = new HashSet<String>();
      s.add(((Prm)name).fullname);
    }
		return s;
	}

  public Set<String> depedentOnParent() {
    Set<String> s = null;
    if (id != null && id instanceof Prm && ((Prm)id).isParent) {
      s = new HashSet<String>();
      s.add(((Prm)id).getFullName());
    }
    if (name != null && name instanceof Prm && ((Prm)name).isParent) {
      if (s == null)
        s = new HashSet<String>();
      s.add(((Prm)name).getFullName());
    }
		return s;
  }

}
