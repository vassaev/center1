package center.task;

import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

import ru.vassaev.core.container.ApplicationManager;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.util.Strings;
import ru.vassaev.core.util.Xml;
import ru.vassaev.core.thread.PoolThread;

import center.task.prm.Prm;
/**
 * Информация о задании - базовый класс
 * @author Vassaev A.V.
 * @version 1.1
 */
public abstract class ATaskInfo {
	protected boolean log = true;
  protected ATaskInfo parent;
  protected ATaskInfo extend;
	public Map<String, Object> prms;
	public boolean owndnf = true;
	public String processor = null;

	public void load(ATaskInfo parent, ATaskInfo extend, Element e) throws SysException {
    this.parent = parent;
    this.extend = extend;
    loadCalc(e);
    log = Strings.parseBooleanNvl(e.getAttribute("log"), true);
  }
  public ATaskInfo() throws SysException {
    super();
  }

  public final Set<Prm> getCalculateTimeState(TimeState ts) {
    return calc.get(ts);
  }
  
  public final Set<?> calculate(TimeState ts, Context cntx) throws SysException {
    Set<Prm> prms = calc.get(ts);
    if (prms == null)
      return null;
    Context pcntx = cntx.parent;
    PoolThread prcs = calc_parallel.get(ts);
    Set<Object> ps = new HashSet<Object>();
    if (prcs != null) {
      for(Prm e : prms) {
        Context.WaitValue v = (e.isParent) ? pcntx.getPrmAsync(prcs, e.fullname)
                : cntx.getPrmAsync(prcs, e.fullname);
        ps.add(v);
      }
    } else {
      for(Prm e : prms) {
        Object v = (e.isParent) ? pcntx.getPrmByFullName(e.fullname)
                : cntx.getPrmByFullName(e.fullname);
        ps.add(v);
      }
    }
    return ps;
  }

  private final Map<TimeState, Set<Prm>> calc = new HashMap<TimeState, Set<Prm>>();
  private final Map<TimeState, PoolThread> calc_parallel = new HashMap<TimeState, PoolThread>();
  
  private void loadCalc(Element p) throws SysException {
  	if (p == null)
  		return;
    if (extend != null)
      for(Map.Entry<TimeState, Set<Prm>> e : extend.calc.entrySet()) {
        calc.put(e.getKey(), new HashSet<Prm>(e.getValue()));
      }
    NodeList nl = p.getChildNodes();
    for (int i = nl.getLength() - 1; i >= 0; i--) {
      Node n = nl.item(i);
      if (n.getNodeType() != Node.ELEMENT_NODE)
        continue;
      if (!n.getNodeName().equals("calculate"))
        continue;
      Element e = (Element) n;
      String timestate = e.getAttribute("timestate");
      boolean replace = Strings.parseBooleanNvl(e.getAttribute("replace"), false);
      TimeState ts;
      if (timestate == null) {
        String time = e.getAttribute("time");
        String state = e.getAttribute("state");
        ts = TimeState.getInstance(time, state);
      } else {
        ts = TimeState.getInstance(timestate);
      }
      if (ts == null)
        continue;
      Set<Prm> calc_prms = calc.get(ts);
      if (calc_prms == null) {
        calc_prms = new HashSet<Prm>();
        calc.put(ts, calc_prms);
      } else {
        if (replace) {
          calc_prms = new HashSet<Prm>();
          calc.put(ts, calc_prms);
        }
      }
      String sprcpool = Strings.getXMLValue(e, "parallel#prcpool");
      PoolThread prcpool;
      if (sprcpool != null) {
        prcpool = (PoolThread) ApplicationManager.getPoolByName(sprcpool, ru.vassaev.core.thread.Process.class);
        calc_parallel.put(ts, prcpool);
      }
      NodeList nlp = Xml.getElementsByTagName(e, "prm");

      for (int j = nlp.getLength() - 1; j >= 0; j--) {
        Element ep = (Element)nlp.item(j);
        Prm o = Prm.getPrmOwner(ep);
        calc_prms.add(o);
      }
    }

  }
  protected Map<String, Object> getPrmsBySpace(String space) {
  	if ("prm".equals(space))
  		return prms;
  	//if ("prm".equals(space))
  	//	return prms;
  	return null;
  }
  public final Object getPrmInfo(String global_name) {
  	if (global_name == null)
  		return null;
  	int i = global_name.indexOf('.');
  	String space = "";
  	String fullname = "";
  	if (i >= 0) {
  		space = global_name.substring(0, i);
    	fullname = global_name.substring(i + 1);
  	} else {
  		space = global_name;
  		fullname = "";
  	}
  	Map<String, Object> m = getPrmsBySpace(space);
  	if (m == null)
  		return null;
  	return m.get(fullname);
  }
}
