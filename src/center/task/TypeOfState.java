package center.task;

/**
 * Состояние задачи может быть начальным, конечным и промежуточным
 */
public enum TypeOfState {
    FIRST, LAST, MIDDLE
}
