package center.task;

/**
 * Список возможных состояний задания
 */
public enum State {
    UNKNOWN {
            public void init () {
                    to = new State[] {};
                    from = new State[] {};
            }
    },
    CREATED {
            public TypeOfState getType() {
                return TypeOfState.FIRST;
            }
            public void init () {
                to = new State[] {READY, CANCELED};
                from = new State[] {UNKNOWN};
            }
    },
    READY {
            public void init () {
                to = new State[] {BLOCKED, CANCELED};
                from = new State[] {CREATED, BLOCKED};
            }
    },
    BLOCKED {
            public void init () {
                to = new State[] {PROCESSING, BREAKING, DONE_ERR};
                from = new State[] {READY};
            }
    },
    PROCESSING {
            public void init () {
                to = new State[] {BREAKING, DONE_ERR, DONE_OK};
                from = new State[] {BLOCKED, SCHEDULED};
            }
    },
    SCHEDULED {
            public void init () {
                to = new State[] {BREAKING, DONE_ERR};
                from = new State[] {BLOCKED};
            }
    },
    BREAKING {
            public void init () {
                from = new State[] {BLOCKED, PROCESSING, SCHEDULED};
                to = new State[] {BROKEN, DONE_ERR, DONE_OK, CANCELED};
            }
    },
    BROKEN {
            public TypeOfState getType() {
                return TypeOfState.LAST;
            }
            public void init () {
                to = new State[] {};
                from = new State[] {BREAKING, BLOCKED};
            }
    },
    CANCELED {
            public TypeOfState getType() {
                return TypeOfState.LAST;
            }
            public void init () {
                to = new State[] {};
                from = new State[] {CREATED, READY};
            }
    },
    DONE_ERR {
            public TypeOfState getType() {
                return TypeOfState.LAST;
            }
            public void init () {
                to = new State[] {};
                from = new State[] {PROCESSING, BREAKING, BLOCKED};
            }
    },
    DONE_TOUT {
            public TypeOfState getType() {
                return TypeOfState.LAST;
            }
            public void init () {
                to = new State[] {};
                from = new State[] {PROCESSING, BREAKING, BLOCKED};
            }
    },
    DONE_OK {
            public TypeOfState getType() {
                return TypeOfState.LAST;
            }
            public void init () {
                to = new State[] {};
                from = new State[] {PROCESSING, BREAKING};
            }
    } ;
    public abstract void init ();

    public TypeOfState getType() {
        synchronized (this) {
            if (to == null) {
                init();
            }
        }
        if (to.length == 0) {
            if (from.length == 0)
                return TypeOfState.MIDDLE;
            else
                return TypeOfState.LAST;
        } else {
            if (from.length == 0)
                return TypeOfState.FIRST;
            else
                return TypeOfState.MIDDLE;
        }
    }

    public State[] getTo() {
        synchronized (this) {
            if (to == null) {
                init();
            }
        }
        return to;
    }
    public State[] getFrom() {
        synchronized (this) {
            if (from == null) {
                init();
            }
        }
        return from;
    }
    protected State[] to;
    protected State[] from;
}
