package center.task.prm;

import center.task.CalculateException;
import center.task.Context;

import java.util.Set;

import ru.vassaev.core.types.TimeList;

/**
 * Абстакция алгоритма расчета параметра
 * @author Vassaev A.V.
 * @version 1.0 1/04/2011
 */
public abstract class Alg {
	// Имя параметра, для расчёта которого используется алгоритм
  protected final String owner;
  protected final Type tp;
  public Alg(Type tp, String owner) {
    super();
    this.owner = owner;
    this.tp = tp;
  }
  public final String getOwner() {
    return owner;
  }
  /**
   * Функция расчёта значения параметра в указанном контексте
   * @param cntx - Контекст расчёта
   * @return результат расчёта
   */
  public final Object calculate(Context cntx) throws CalculateException {
    TimeList tl = cntx.getTimeList(owner, tp);
    try {
      tl.addPointTime(TimeList.Type.START);
      return calculate(tl, cntx);
    } finally {
      tl.addPointTime(TimeList.Type.END);
    }
  }
  protected abstract Object calculate(TimeList tl, Context cntx) throws CalculateException;
  /**
   * Получить список параметров, от которых зависит расчет по данному алгоритму
   * @return список имен
   */
  public abstract Set<String> depedentOn();
  public abstract Set<String> depedentOnParent();
}
