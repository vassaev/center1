package center.task.prm.alg;


import org.w3c.dom.Element;

import center.task.CalculateException;
import center.task.Context;
import center.task.prm.Alg;
import center.task.prm.Prm;
import ru.vassaev.core.types.TimeList;
import ru.vassaev.core.types.TimeList.Type;
import ru.vassaev.core.base.Null;
import ru.vassaev.core.exception.SysRuntimeException;
import ru.vassaev.core.exception.SysException;

import java.util.Set;
import java.util.HashSet;


/**
 * Параметр для получения значения системного параметра
 *
 * @author Vassaev A.V.
 */
public final class SysProp extends Alg {

  private Object name;

  public SysProp(center.task.prm.Type tp, String owner, String name) {
    super(tp, owner);
    if (name == null)
      throw new SysRuntimeException("The system param hasn't name");
    this.name = name;
  }

  public SysProp(center.task.prm.Type tp, String owner, Element e) {
    super(tp, owner);
    name = Prm.getPrm(e);
    if (Null.equ(name))
      throw new SysRuntimeException("The system param hasn't name");
  }

  private String getName(Context cntx) throws SysException, CalculateException {
    if (name instanceof String)
      return (String) name;
    if (name instanceof Prm)
      return ((Prm) name).getString(cntx);
    return null;
  }

  public Object calculate(TimeList tl, Context cntx)  throws CalculateException {
    try {
    	tl.addPointTime(Type.START);
      return System.getProperty(getName(cntx));
    } catch (SysException e) {
      throw new CalculateException(owner, e);
    } finally {
    	tl.addPointTime(Type.END);
    }
  }

  public Set<String> depedentOn() {
    Set<String> s = null;
    if ((name instanceof Prm) && !((Prm)name).isParent) {
      s = new HashSet<String>();
      s.add(((Prm) name).fullname);
    }
    return s;
  }

  public Set<String> depedentOnParent() {
    Set<String> s = null;
    if ((name instanceof Prm) && ((Prm)name).isParent) {
      s = new HashSet<String>();
      s.add(((Prm) name).fullname);
    }
    return s;
  }
}