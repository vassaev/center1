package center.db;

/**
 * @author Vassaev A.V.
 * @version 1.0
 */
public class IndexKeyColumn {
  public boolean NON_UNIQUE;
  public String INDEX_QUALIFIER;
  public String INDEX_NAME;
  public Short TYPE;
  public Short ORDINAL_POSITION;
  public String COLUMN_NAME;
  public String ASC_OR_DESC;
  public Integer CARDINALITY;
  public Integer PAGES;
  public String FILTER_CONDITION;
}
