package center.task.api.mem;

import java.util.Date;

import center.task.State;

public class TSK {
  Long id;
  long dt_created;
  long dt_goal;
  long dt_started;
  long dt_finished;
  State status_id;
  String classname;
  final long initiator_id;
  Long processor_id;
  String program;
  String opid;
  long block_duration;
  long finish_duration;
  double arch_day;
  long last_ping;
  String errs;
  Throwable errp;
  long parent_id;
  long dt_end_block;
  long dt_arch;
  TSK (long initiator_id, String cls, long parent_task_id) {
  	this.initiator_id = initiator_id;
  	classname = cls;
  	parent_id = parent_task_id; 
  }
  private static java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd.MM.yyyy HH:mm:ss.S");
  public synchronized final String getFieldByName(String name) {
  	if (name == null)
  		return null;
	  if (name.equalsIgnoreCase("id"))
	  	return String.valueOf(id);
	  if (name.equalsIgnoreCase("dt_created"))
	  	return sdf.format(new Date(dt_created));
	  if (name.equalsIgnoreCase("dt_goal"))
	  	return sdf.format(new Date(dt_goal));
	  if (name.equalsIgnoreCase("dt_started"))
	  	return sdf.format(new Date(dt_started));
	  if (name.equalsIgnoreCase("dt_finished"))
	  	return sdf.format(new Date(dt_finished));
	  if (name.equalsIgnoreCase("status_id"))
	  	return status_id.name();
	  if (name.equalsIgnoreCase("classname"))
	  	return classname;
	  if (name.equalsIgnoreCase("initiator_id"))
	  	return String.valueOf(initiator_id);
	  if (name.equalsIgnoreCase("processor_id"))
	  	return String.valueOf(processor_id);
	  if (name.equalsIgnoreCase("program"))
	  	return program;
	  if (name.equalsIgnoreCase("opid"))
	  	return opid;
	  if (name.equalsIgnoreCase("block_duration"))
	  	return String.valueOf(block_duration);
	  if (name.equalsIgnoreCase("finish_duration"))
	  	return String.valueOf(finish_duration);
	  if (name.equalsIgnoreCase("arch_day"))
	  	return String.valueOf(arch_day);
	  if (name.equalsIgnoreCase("last_ping"))
	  	return sdf.format(new Date(last_ping));
	  if (name.equalsIgnoreCase("errs"))
	  	return errs;
	  if (name.equalsIgnoreCase("parent_id"))
	  	return String.valueOf(parent_id);
	  if (name.equalsIgnoreCase("dt_end_block"))
	  	return sdf.format(new Date(dt_end_block));
	  if (name.equalsIgnoreCase("dt_arch"))
	  	return sdf.format(new Date(dt_arch));
	  return null;
  }
}
