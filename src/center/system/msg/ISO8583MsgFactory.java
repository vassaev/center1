package center.system.msg;

import ru.vassaev.core.Factory;
import ru.vassaev.core.util.Strings;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

import java.util.TreeMap;
import java.util.Map;
import java.util.Iterator;

/**
 * Класс инициализации сообщения для протокола ISO8583
 * 
 * @author Vassaev A.V.
 * @version 1.1
 */
public final class ISO8583MsgFactory implements Factory<ISO8583Msg> {
	private Map<Integer, String> prms = new TreeMap<Integer, String>();
	private String keyNames;
	private String headerFormat;

	public void initFactory(Map<String, Object> prms) {
		headerFormat = Strings.getString(prms.get("headerFormat"));
		keyNames = Strings.getString(prms.get("keyNames"));
	}

	private void loadFromXML(Element el) {
		headerFormat = Strings.getString(el.getAttribute("headerFormat"));
		keyNames = Strings.getString(el.getAttribute("keyNames"));
		NodeList nl = el.getChildNodes();
		for (int i = nl.getLength() - 1; i >= 0; i--) {
			Node node = nl.item(i);
			if (node.getNodeType() != Node.ELEMENT_NODE)
				continue;
			if (!"fieldFormat".equals(node.getNodeName()))
				continue;
			Element e = (Element) node;
			String n = e.getAttribute("name");
			String v = e.getTextContent();
			prms.put(ISO8583Msg.getIndexByKey(n), v);
		}
	}

	public ISO8583MsgFactory(Element el) {
		super();
		loadFromXML(el);
	}

	public ISO8583MsgFactory() {
		super();
	}

	public void initialization(ISO8583Msg inst) {
		Iterator<Integer> i = prms.keySet().iterator();
		while (i.hasNext()) {
			Integer key = i.next();
			inst.setFieldFormat(key, prms.get(key));
		}
		inst.setKeyNames(keyNames);
	}

	public boolean reset(ISO8583Msg msg) {
		msg.reset();
		return true;
	}

	public void initFactory(Element el) {
		loadFromXML(el);
	}

	public ISO8583Msg create() {
		return new ISO8583Msg(headerFormat);
	}

	public boolean canUseImmediately(ISO8583Msg inst) {
		return true;
	}

	public boolean isValid(ISO8583Msg iso8583Msg) {
		return true;
	}

	public void destroy(ISO8583Msg inst) {}

	@SuppressWarnings("unchecked")
	public Class<ISO8583Msg> getObjectClass() {
		try {
			return (Class<ISO8583Msg>) Class.forName("center.system.msg.ISO8583Msg", true, ClassLoader.getSystemClassLoader());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Object getProperty(String name) {
		return null;
	}

	public String[] getPropertyNames() {
		return null;
	}
}
