package center.task.api.http;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ru.vassaev.core.base.Null;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.thread.Process;
import ru.vassaev.core.util.Strings;
import center.task.State;
import center.task.api.ITaskAPI;
import center.task.api.TaskCI;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

public final class HttpHandler implements com.sun.net.httpserver.HttpHandler {
	private final Process main_thread;
	private final Server server;

	public HttpHandler(Process main_thread, Server server) {
		this.main_thread = main_thread;
		this.server = server;
	}

	public void handle(HttpExchange httpExchange) throws IOException {
		Process.currentProcess().setParent(main_thread);
		try {
//			InetSocketAddress ra = httpExchange.getRemoteAddress();
//			Process cp = Process.currentProcess();

//			String path = httpExchange.getRequestURI().getPath();
			Map<String, List<String>> hdrs = httpExchange.getRequestHeaders();
			List<String> ls = hdrs.get("ITaskAPI.Alias");
			if (ls == null || ls.size() == 0) {
				System.err.println("Header by name 'ITaskAPI.Alias' isn't set");
				return;
			}
			String alias = ls.get(0);
			ITaskAPI ta = server.getTaskAPI(alias);
			if (ta == null) {
				System.err.println("Undefined ITaskAPI interface by name '" + alias
						+ "'");
				return;
			}
			ls = hdrs.get("ITaskAPI.Method");
			if (ls == null || ls.size() == 0) {
				System.err.println("Header by name 'ITaskAPI.Method' isn't set");
				return;
			}
			String method = ls.get(0);
			//System.out.println("!!! method=" + method);
			ls = hdrs.get("Content-length");
			if (ls == null || ls.size() == 0) {
				System.err.println("Header by name 'Content-length' isn't set");
				return;
			}
			int l = Strings.parseInteger(ls.get(0)) - 2;
			if (l < 0) {
				System.err.println("Incorrect length of the input data!");
				return;
			}
			if  (l > 1024 * 1024) {
				System.err.println("Too long input data!");
				return;
			}
			// Чтение входящего потока
			InputStream is = httpExchange.getRequestBody();
			byte[] bytes = new byte[l];
			try {
				is.read(bytes);
			} finally {
				is.close();
			}
			StringBuffer sbi = new StringBuffer(new String(bytes, "utf-8"));
			Object[] objs = Handler.getObjects(sbi);

			if ("getAlias".equals(method)) {
				getAlias(objs, httpExchange, ta);
			} else if ("getSubject".equals(method)) {
				getSubject(objs, httpExchange, ta);
			} else if ("registerClasses".equals(method)) {
				registerClasses(objs, httpExchange, ta);
			} else if ("log".equals(method)) {
				log(objs, httpExchange, ta);
			} else if ("getBlocked".equals(method)) {
				getBlocked(objs, httpExchange, ta);
			} else if ("setParamObject".equals(method)) {
				setParamObject(objs, httpExchange, ta);
			} else if ("setParamObject2".equals(method)) {
				setParamObject2(objs, httpExchange, ta);
			} else if ("createTask".equals(method)) {
				createTask(objs, httpExchange, ta);
			} else if ("createTask2".equals(method)) {
				createTask2(objs, httpExchange, ta);
			} else if ("getClassName".equals(method)) {
				getClassName(objs, httpExchange, ta);
			} else if ("setTaskDT".equals(method)) {
				setTaskDT(objs, httpExchange, ta);
			} else if ("setTaskError".equals(method)) {
				setTaskError(objs, httpExchange, ta);
			} else if ("getParamTask".equals(method)) {
				getParamTask(objs, httpExchange, ta);
			} else if ("getGroupParams".equals(method)) {
				getGroupParams(objs, httpExchange, ta);
			} else if ("getState".equals(method)) {
				getState(objs, httpExchange, ta);
			} else if ("setState".equals(method)) {
				setState(objs, httpExchange, ta);
			} else if ("setState2".equals(method)) {
				setState2(objs, httpExchange, ta);
			} else if ("setReady".equals(method)) {
				setReady(objs, httpExchange, ta);
			} else if ("setReady2".equals(method)) {
				setReady2(objs, httpExchange, ta);
			} else if ("setBlocked".equals(method)) {
				setBlocked(objs, httpExchange, ta);
			} else if ("setProcessing".equals(method)) {
				setProcessing(objs, httpExchange, ta);
			} else if ("setTimeout".equals(method)) {
				setTimeout(objs, httpExchange, ta);
			} else if ("setScheduled".equals(method)) {
				setScheduled(objs, httpExchange, ta);
			} else if ("setBreaking".equals(method)) {
				setBreaking(objs, httpExchange, ta);
			} else if ("setBroken".equals(method)) {
				setBroken(objs, httpExchange, ta);
			} else if ("setCanceled".equals(method)) {
				setCanceled(objs, httpExchange, ta);
			} else if ("setDoneErr".equals(method)) {
				setDoneErr(objs, httpExchange, ta);
			} else if ("setDoneOk".equals(method)) {
				setDoneOk(objs, httpExchange, ta);
			} else if ("waitFinished".equals(method)) {
				waitFinished(objs, httpExchange, ta);
			} else if ("writeLogToFile".equals(method)) {
				writeLogToFile(objs, httpExchange, ta);
			} else if ("getParam".equals(method)) {
				getParam(objs, httpExchange, ta);
			} else
				httpExchange.sendResponseHeaders(503, 0);
		} catch (IOException e) {
			e.printStackTrace();
			e.printStackTrace(new PrintStream(httpExchange.getResponseBody()));
			httpExchange.sendResponseHeaders(503, 0);
		} catch (SysException e) {
			e.printStackTrace();
			e.printStackTrace(new PrintStream(httpExchange.getResponseBody()));
			httpExchange.sendResponseHeaders(503, 0);
		} finally {
			httpExchange.close();
		}
	}

	// //////////////////////////////
	@SuppressWarnings("unchecked")
	public void registerClasses(Object[] objs, HttpExchange httpExchange,
			ITaskAPI ta) throws SysException, IOException {
		ta.registerClasses(((Long) (objs[0])).longValue(), (Set<String>) (objs[1]));
		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(2)));
		httpExchange.sendResponseHeaders(200, 2);
		OutputStream w = httpExchange.getResponseBody();
    w.write("\r\n".getBytes());
		w.close();
	}

	public void getAlias(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		String r = ta.getAlias();
		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void getSubject(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
	  long old_subject_id = ((Long) (objs[0])).longValue();
		long r = ta.getSubject(old_subject_id); 
		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");
		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));
		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void log(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		long r = ta.log(((Long) (objs[0])).longValue(),
				((Long) (objs[1])).longValue(), (String) (objs[2]));

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void getBlocked(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		TaskCI r = ta.getBlocked(((Long) (objs[0])).longValue(),
				((Long) (objs[1])).longValue());

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void setParamObject(Object[] objs, HttpExchange httpExchange,
			ITaskAPI ta) throws SysException, IOException {
		String name = (Null.equ(objs[1])) ? null : (String) (objs[1]);
		long r = ta.setParamObject(((Long) (objs[0])).longValue(),
				name, objs[2]);

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void setParamObject2(Object[] objs, HttpExchange httpExchange,
			ITaskAPI ta) throws SysException, IOException {
		long r = ta.setParamObject(((Long) (objs[0])).longValue(),
				(String) (objs[1]), objs[2], ((Long) (objs[3])).longValue(),
				((Long) (objs[4])).longValue(), ((Long) (objs[5])).longValue());

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void createTask(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		long r = ta.createTask(((Long) (objs[0])).longValue(), (String) (objs[1]));

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void createTask2(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		long r = ta.createTask(((Long) (objs[0])).longValue(), (String) (objs[1]),
				((Long) (objs[2])).longValue());

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void getClassName(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		String r = ta.getClassName(((Long) (objs[0])).longValue());

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void setTaskDT(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		long id = (Long) (objs[0]);
		String dt = (Null.equ(objs[1])) ? null : (String) (objs[1]);;
		String duration = (Null.equ(objs[2])) ? null : (String) (objs[2]);
		String block_duration = (Null.equ(objs[3])) ? null : (String) (objs[3]);
		Double arch_day = (Null.equ(objs[4])) ? null : (Double) (objs[4]);;
		long r = ta.setTaskDT(id, dt, duration, block_duration, arch_day);

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void setTaskError(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		Throwable tr = (Null.equ(objs[1])) ? null : (Throwable) (objs[1]);

		long r = ta.setTaskError(((Long) (objs[0])).longValue(), tr);

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	// *
	public void getParamTask(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		String name = (Null.equ(objs[1])) ? null : (String) (objs[1]);

		String r = ta.getParamTask(((Long) (objs[0])).longValue(), name);

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void getGroupParams(Object[] objs, HttpExchange httpExchange,
			ITaskAPI ta) throws SysException, IOException {
		String grp = (Null.equ(objs[1])) ? null : (String) (objs[1]);

		Map<String, Object> r = ta.getGroupParams(((Long) (objs[0])).longValue(), grp);

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	// public Reader getTaskParamLong(long id, String name) throws
	// RemoteException;

	public void getState(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		State r = ta.getState(((Long) (objs[0])).longValue());

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void setState(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		long r = ta.setState(((Long) (objs[0])).longValue(),
				((Long) (objs[1])).longValue(), ((State) (objs[2])));

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void setState2(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		long subject = ((Long) (objs[0])).longValue();
		long task = ((Long) (objs[1])).longValue();
		State st = (Null.equ(objs[2])) ? null : ((State) (objs[2]));
		Long processor = (Null.equ(objs[3])) ? null : ((Long) (objs[3]));
		long r = ta.setState(subject, task, st, processor);

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void setReady(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		Long processor = (Null.equ(objs[1])) ? null : ((Long) (objs[1]));
		long r = ta.setReady(((Long) (objs[0])).longValue(), processor);

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void setReady2(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		long r = ta.setReady(((Long) (objs[0])).longValue(), ((Long) (objs[1])),
				((Long) (objs[2])));

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void setBlocked(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		long r = ta.setBlocked(((Long) (objs[0])).longValue(), ((Long) (objs[1])));

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void setProcessing(Object[] objs, HttpExchange httpExchange,
			ITaskAPI ta) throws SysException, IOException {
		long r = ta.setProcessing(((Long) (objs[0])).longValue(),
				((Long) (objs[1])));

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void setTimeout(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		long r = ta.setTimeout(((Long) (objs[0])).longValue(), ((Long) (objs[1])));

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void setScheduled(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		long r = ta
				.setScheduled(((Long) (objs[0])).longValue(), ((Long) (objs[1])));

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void setBreaking(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		long r = ta.setBreaking(((Long) (objs[0])).longValue(), ((Long) (objs[1])));

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void setBroken(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		long r = ta.setBroken(((Long) (objs[0])).longValue(), ((Long) (objs[1])));

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void setCanceled(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		long r = ta.setCanceled(((Long) (objs[0])).longValue(), ((Long) (objs[1])));

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void setDoneErr(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		long r = ta.setDoneErr(((Long) (objs[0])).longValue(), ((Long) (objs[1])));

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void setDoneOk(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		long r = ta.setDoneOk(((Long) (objs[0])).longValue(), ((Long) (objs[1])));

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void waitFinished(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		State r = ta.waitFinished(((Long) (objs[0])).longValue(),
				((Long) (objs[1])), ((Long) (objs[2])));

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		httpExchange.sendResponseHeaders(200, rb.length + 2);
		OutputStream w = httpExchange.getResponseBody();
		w.write(rb);
    w.write("\r\n".getBytes());
		w.close();
	}

	public void writeLogToFile(Object[] objs, HttpExchange httpExchange,
			ITaskAPI ta) throws SysException, IOException {
		ta.writeLogToFile(((Long) (objs[0])).longValue(), ((File) (objs[1])),
				((String) (objs[2])));

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(2)));
		httpExchange.sendResponseHeaders(200, 2);
		OutputStream w = httpExchange.getResponseBody();
    w.write("\r\n".getBytes());
		w.close();
	}

	public void getParam(Object[] objs, HttpExchange httpExchange, ITaskAPI ta)
			throws SysException, IOException {
		String name = (Null.equ(objs[1])) ? null : (String) (objs[1]);
		Object r = ta
				.getParam(((Long) (objs[0])).longValue(), name);

		StringBuffer sb = new StringBuffer();
		sb = Handler.getElementByObject(sb, r);
		byte[] rb = sb.toString().getBytes("utf-8");
		//if (rb.length > 8000) {
		//	System.out.println("!!! l = " + rb.length);
		//	Object o = Handler.getObjectByElement(sb.toString());
		//	System.out.println("!!! object.class = " + o.getClass());
		//}

		Headers hd = httpExchange.getResponseHeaders();
		hd.put("Content-Type", Arrays.asList("text/plain; charset=utf-8"));
		hd.put("Content-Length", Arrays.asList(Long.toString(rb.length + 2)));

		OutputStream w = httpExchange.getResponseBody();
		httpExchange.sendResponseHeaders(200, rb.length + 2);
		w.write(rb);
    w.write("\r\n".getBytes());
    w.flush();
		w.close();
	}
	// */
}
