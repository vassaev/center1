package center.task.prm;

import ru.vassaev.core.exception.SysException;
import center.task.Context;
import center.task.State;

/**
 * Интерфейс параметра, который может быть сохранен в базе данных
 * @author Андрей
 *
 */
public interface Saved {
	enum TIME {before, after};
	/**
	 * Функция позволяет определить, установлено ли свойство сохранения для параметра
	 * @return Состояние задачи, при котором необходимо сохранить параметр
	 * null - параметр сохранять не надо
	 */
	public State stateSaved();
	/**
	 * Функция возвращает момент времени по отношению к параметру stateSaved, 
	 * в который необходимо сохранить параметр 
	 * @return before/after
	 */
	public TIME timeSaved();
	/**
	 * Метод сохранения параметра
	 * @param cntx - контекст
	 * @throws SysException
	 */
	public void save(Context cntx) throws SysException;
	/**
	 * @return порядок сохранения
	 */
	public int priority();
}
