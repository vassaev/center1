package center.db;

/**
 * @author Vassaev A.V.
 * @version 1.0
 */
public class PrimaryKeyColumn {
  public String COLUMN_NAME;
  public Short KEY_SEQ;
  public String PK_NAME;
}
