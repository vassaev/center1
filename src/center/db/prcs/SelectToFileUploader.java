package center.db.prcs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import center.system.*;
import ru.vassaev.core.util.Classes;
import center.db.*;
import center.format.*;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.ClassTableModel;
import ru.vassaev.core.base.Null;
import ru.vassaev.core.db.Manager;
import ru.vassaev.core.db.Types;
import ru.vassaev.core.db.IDataHelper;
import ru.vassaev.core.db.DataHelper;


/**
 * @author Vassaev A.V.
 * @version 1.0
 */
public class SelectToFileUploader extends Processor {

  public SelectToFileUploader() {
    Properties props = getInitProperties();
    Null n = Null.getInstance();
    props.put("sql", n);
    props.put("connect", n);
    props.put("package", n);
    props.put("row.class", n);
    props.put("row.dh", n);
    props = getStepProperties();
    props.put("charset", "UTF-8");
    props.put("file", n);
  }

  protected String conName = null;
  protected String sql = null;
  protected String pkg = null;
  protected String row_class = null;
  protected String row_dh = null;

  protected Class rowClass = null;
  protected Class dataClass = null;
  protected ClassTableModel ctm = null;
  protected IRowFormat rf = null;
  protected Types tp_src = null;

  public void setRowFormat(IRowFormat rf) {
    this.rf = rf;
  }

  protected void initialize() throws SysException {
    Properties props = getInitProperties();

    conName = props.getProperty("connect");
    sql = props.getProperty("sql");
    row_class = props.getProperty("row.class");
    row_dh = props.getProperty("row.dh");
    pkg = props.getProperty("package");

    Select qr = null;
    Connection con = null;
    try {
      con = Manager.getConnection(conName);
      tp_src = Manager.getTypes(con);

      File src_path = new File(Manager.getSourcePath(con));
      File cls_path = new File(Manager.getClassPath(con));

      try {
        rowClass = Class.forName(pkg + "." + row_class);
      } catch (Exception ex) {
        if (qr == null)
          qr = Select.getInstance(con, sql);
        rowClass = Classes.getCodeClass(src_path
                                        , cls_path
                                        , pkg
                                        , row_class, "Object", null
                                        , qr.getDeclareDataClassBody());
      }

      try {
        dataClass = Class.forName(pkg + "." + row_dh);
      } catch (Exception ex) {
        if (qr == null)
          qr = Select.getInstance(con, sql);
        dataClass = Classes.getCodeClass(src_path
                                         , cls_path
                                         , pkg
                                         , row_dh, "ru.vassaev.core.db.DataHelper", null
                                         , "\tpublic " + row_dh + "() " +
                                           "throws ru.vassaev.core.exception.SysException {}\n" +
                                           qr.getDeclareHelperClassBody(rowClass.getCanonicalName()
                                                                        , false));
      }
      ctm = new ClassTableModel(rowClass);
    } catch (java.sql.SQLException ex) {
      throw new SysException(ex);
    } finally {
      if (qr != null)
        try {
          qr.destroy();
        } catch (SQLException e) {
          e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
      Manager.freeConnection(con);
    }
  }

  public static boolean createNewFile(File fl) throws IOException {
    String path = fl.getAbsolutePath();
    int l = path.length();
    StringBuffer name = new StringBuffer();
    for (int i = 0; i < l; i++) {
      char ch = path.charAt(i);
      switch (ch) {
        case '\\':
        case '/':
          File f = new File(name.toString());
          if (!f.exists()) {
            if (!f.mkdir()) {
              return false;
            }
          } else {
            if (!f.isDirectory())
              return false;
          }
          break;
      }
      name.append(ch);
    }
    if (!fl.exists())
      return fl.createNewFile();
    return true;
  }

  protected boolean execute() throws SysException {
    StatementNew qr = new StatementNew();
    qr.prepare(conName, sql);
    Connection con = null;
    try {
      con = Manager.getConnection(conName);
      PreparedStatement st = qr.getStatement(con);
      ResultSet rs = st.executeQuery();

      IDataHelper dh = DataHelper.getInstance(dataClass);

      Properties props = getStepProperties();
      String charset = props.getProperty("charset");
      String fileName = props.getProperty("file");

      File fl = new File(fileName);
      if (!createNewFile(fl))
        throw new SysException("Cannot create file");
      FileOutputStream fls = new FileOutputStream(fl.getCanonicalPath());
      IRowFormat rf1 = rf;
      if (rf1 == null)
        rf1 = new DSVRowFormat();
      int i = 0;
      Object o = rowClass.newInstance();
      while (rs.next()) {
        dh.loadFromRs(tp_src, rs, o);
        ctm.addRowAt(o, 0);
        String s = rf1.format(ctm, 0);
        fls.write(s.getBytes(charset));
        fls.write("\r\n".getBytes(charset));
        ctm.delRowAt(0);
        i++;
      }
      rs.close();
      fls.close();
    } catch (java.sql.SQLException ex) {
      throw new SysException(ex);
    } catch (IllegalAccessException ex) {
      throw new SysException(ex);
    } catch (InstantiationException ex) {
      throw new SysException(ex);
    } catch (IOException ex) {
      throw new SysException(ex);
    } finally {
      Manager.freeConnection(con);
      if (qr != null)
        qr.close();
    }

    return true;
  }

  public void destroy() throws SysException {
  }
}
