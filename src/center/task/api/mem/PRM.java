package center.task.api.mem;

import center.task.api.ATaskAPI;

public final class PRM {
	private static long last_id = 0;
	public final long id;
	public final String fullname;
	public final String grp;
	public final String name;
	private Object value;
	public PRM(String fullname, Object value) {
		this.fullname = fullname;
		String[] fn = ATaskAPI.getName(fullname);
		this.grp = fn[0];
		this.name = fn[1];
		this.id = (last_id++);
		this.value = value;
	}
	public synchronized Object getValue() {
		return value;
	}
	public synchronized void setValue(Object obj) {
		value = obj;
	}
	private long wait = 0;
	private long calc = 0;
	private long scall = 0;
	public synchronized void setTime(long wait, long calc, long scall) {
		this.wait = wait;
		this.calc = calc;
		this.scall = scall;
	}
	public synchronized long[] setTime() {
		return new long[]{wait, calc, scall};
	}
}
