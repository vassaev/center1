package center.task;

import org.w3c.dom.Element;

import center.task.prm.tp.Int;
import center.task.prm.tp.Str;

import ru.vassaev.core.base.Null;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.util.Strings;

public class TaskSchedulerInfo extends NewTaskInfo {
	public enum Action {START, SKIP, STOP, ONES}
	private Str action;
	private Int pcs;
	private Str wait_finished;
	public TaskSchedulerInfo() throws SysException {
		super();
	}
	public void load(ATaskInfo parent, ATaskInfo extend, Element e) throws SysException {
		super.load(parent, extend, e);
		Element action = (Element) Strings.getXMLObject(e, "action");
		if (action == null)
			throw new SysException("Undefined element \"action\"");
		this.action = new Str(action, this);
		Element pcs = (Element) Strings.getXMLObject(e, "period-check-sec");
		if (pcs == null)
			throw new SysException("Undefined element \"period-check-sec\"");
		this.pcs = new Int(pcs, this);
		Element wait_finished = (Element) Strings.getXMLObject(e, "wait-finished");
		if (wait_finished == null)
			this.wait_finished = null;
		else
			this.wait_finished = new Str(wait_finished, this);
		name = e.getAttribute("name");
	}
	private String name;
	
	public String getName() {
		return name;
	}
	
	public Action getAction(Context cntx) throws CalculateException {
    String action = Strings.getString(this.action.getValue(cntx));
    if (Null.equ(action))
    		return Action.SKIP;
    Action act = Action.valueOf(action);
    if (Null.equ(action))
    	return Action.SKIP;
    return act;
	}
	public long getPCS(Context cntx) throws CalculateException {
    long r = Strings.parseIntegerNvl(this.pcs.getValue(cntx), 10L);
  	if (r < 1L)
  		return 1L;
    return r;
	}
	public boolean getWaitFinished(Context cntx) throws CalculateException {
    return Strings.parseBooleanNvl(this.wait_finished.getValue(cntx), true);
	}
}
