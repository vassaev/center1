package center.task;

import java.util.HashMap;
import java.util.Map;

/**
 * Класс, определяющий момент времени состояния задачи
 * @author Vassaev A.V.
 * @version 1.0 23/09/2011
 */
public final class TimeState {
  private static final Map<String, TimeState> inst = new HashMap<String, TimeState>();
  public static enum Time {before, after}
  public final Time time;
  public final State state;
  private TimeState(Time time, State state) {
    this.time = time;
    this.state = state;
  }

  public static TimeState getInstance(Time time, State state) {
    TimeState instance;
    String key = time.name() + " " + state.name();
    synchronized(inst) {
      instance = inst.get(key);
      if (instance != null)
        return instance;
      instance = new TimeState(time, state);
      inst.put(key, instance);
    }
    return instance;
  }

  public static TimeState getInstance(String timestate) {
    int i = timestate.indexOf(' ');
    if (i <= 0)
      return null;
    String time = timestate.substring(0, i);
    Time t = Time.valueOf(time);
    if (t == null)
      return null;
    String state = timestate.substring(i + 1);
    State s = State.valueOf(state);
    if (s == null)
      return null;
    return getInstance(t, s);
  }

  public static TimeState getInstance(String time, String state) {
    Time t = Time.valueOf(time);
    if (t == null)
      return null;
    State s = State.valueOf(state);
    if (s == null)
      return null;
    return getInstance(t, s);
  }

  public String toString() {
    return time.name() + " " + state.name();
  }
}
