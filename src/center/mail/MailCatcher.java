package center.mail;

import java.io.File;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.Properties;

import javax.mail.Session;
import javax.mail.Message;
import javax.mail.Flags;
import javax.mail.MessagingException;
import javax.mail.Folder;
import javax.mail.Store;
import javax.mail.NoSuchProviderException;
import javax.mail.BodyPart;
import javax.mail.internet.MimeMultipart;

import center.system.*;
import javax.mail.Address;
import sun.misc.BASE64Decoder;
import ru.vassaev.core.base.Null;
import ru.vassaev.core.exception.SysException;

public class MailCatcher extends Processor {
    public MailCatcher() {
        Properties props = getInitProperties();
        Null n = Null.getInstance();
        props.put("mail.pop3.host", n);
        props.put("mail.pop3.username", n);
        props.put("mail.pop3.password", n);
        props = getStepProperties();
        props.put("dir", n);
        props.put("sender", n);
    }

    private Session session = null;
    private String pop3host = null;
    private String username = null;
    private String password = null;
    private Store pop3Store = null;
    private BASE64Decoder t = new BASE64Decoder();

    protected void initialize() throws SysException{
        session = Session.getDefaultInstance(getInitProperties(), null);
        try {
            pop3Store = session.getStore("pop3");
        } catch (NoSuchProviderException ex) {
            throw new SysException(ex);
        }
        Properties props = getInitProperties();
        pop3host = props.getProperty("mail.pop3.host");
        username = props.getProperty("mail.pop3.username");
        password = props.getProperty("mail.pop3.password");
    }


    protected boolean execute() throws SysException {
        try {
            Properties props = getStepProperties();
            File dir = (File)props.get("dir");
            String sender = props.getProperty("sender");
            pop3Store.connect(pop3host, username, password);

            Folder folder = pop3Store.getFolder("INBOX");
            folder.open(Folder.READ_WRITE);
            Message message[] = folder.getMessages();
            if (message.length > 0) {
                int i = 0;
//            for (int i = 0; i < message.length; i++) {
                Address[] adrs = message[i].getFrom();
                for (int j = 0; j < adrs.length; j++) {
                    System.out.println(adrs[j].toString());
                }
                Object cnt = message[i].getContent();
                if(cnt instanceof MimeMultipart) {
                    MimeMultipart mm = (MimeMultipart) cnt;
                    for (int j = mm.getCount() - 1; j >= 0; j--) {
                        BodyPart bp = mm.getBodyPart(j);
                        String fn = bp.getFileName();
                        if ((fn != null) && (!fn.equals(""))) {
                            InputStream is = bp.getInputStream();
                            try {
                                int b = fn.indexOf("=?");
                                int e = fn.indexOf("?=");
                                if ((b == 0) && (e == fn.length() - 2)) {
                                    fn = fn.substring(b + 2, e);
                                    e = fn.indexOf('?');
                                    String encode = fn.substring(0, e);
                                    fn = fn.substring(e + 3);
                                    fn = new String(t.decodeBuffer(fn)
                                            , encode);
                                }
                            } catch(Exception ex) {
                                fn = bp.getFileName();
                            }
                            FileOutputStream fileos = new FileOutputStream(
                                new File(dir, "" + is.available()
                                + "." + System.currentTimeMillis()));
                            byte[] buf = new byte[4096];
                            int len = 0;
                            while ((len = is.read(buf)) > 0) {
                                fileos.write(buf, 0, len);
                            }
                            is.close();
                            fileos.close();
                        }
                    }
                }
                message[i].setFlag(Flags.Flag.DELETED, true);
            }

            folder.close(true);
            return true;
        } catch (MessagingException ex) {
            throw new SysException(ex);
        } catch (FileNotFoundException ex) {
            throw new SysException(ex);
        } catch (IOException ex) {
            throw new SysException(ex);
        }
    }
}
