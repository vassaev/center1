package center.system.msg;

import java.util.Map;

import org.w3c.dom.Element;

import ru.vassaev.core.Factory;

public class TPTPMsgFactory implements Factory<TPTPMsg>{

	public void initFactory(Element el) {
	}

	public void initFactory(Map<String, Object> prms) {
	}

	public TPTPMsg create() {
		return new TPTPMsg();
	}

	public void initialization(TPTPMsg inst) {
		inst.reset();
	}

	public boolean reset(TPTPMsg inst) {
		inst.reset();
		return true;
	}

	public boolean canUseImmediately(TPTPMsg inst) {
		return true;
	}

	public boolean isValid(TPTPMsg inst) {
		return true;
	}

	public void destroy(TPTPMsg inst) {
	}

	public Class<TPTPMsg> getObjectClass() {
		try {
			return (Class<TPTPMsg>) Class.forName("center.system.msg.TPTPMsg", true, ClassLoader.getSystemClassLoader());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String[] getPropertyNames() {
		return null;
	}

	public Object getProperty(String name) {
		return null;
	}

}
