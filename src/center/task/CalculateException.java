package center.task;
/**
 * Исключение, возникающее в процессе расчёта параметра
 * @version 1.0 
 * @author Vassaev A.V.
 */
public class CalculateException extends Exception {
	private static final long serialVersionUID = -5090547467457063333L;
	private String paramName;
	
	public CalculateException(String paramName, Throwable cause) {
		super(cause);
		this.paramName = paramName;
	}
	
	public CalculateException(String text) {
		super(text);
		this.paramName = null;
	}

	public CalculateException(String paramName, String text, Throwable cause) {
		super(text, cause);
		this.paramName = paramName;
	}

	public CalculateException(String paramName, String text) {
		super(text);
		this.paramName = paramName;
	}
	
	public final String getParamName() {
		return paramName;
	}
}
