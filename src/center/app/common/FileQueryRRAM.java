package center.app.common;

import center.system.msg.FileQuery;
import center.system.msg.PrmInterfaceInst;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.util.Strings;
import ru.vassaev.core.util.Bytes;
import ru.vassaev.core.PrmInterface;

import java.io.*;
import java.net.Socket;

/**
 */
public class FileQueryRRAM implements FileQuery {
  public File sendFile(File query, PrmInterface prms, File to) throws SysException {
    return null;  //To change body of implemented methods use File | Settings | File Templates.
  }

  public File sendFile(byte[] query, PrmInterface prms, File to) throws SysException {
    try {
      String server = prms.getField("@server");
      int port = Integer.parseInt(Strings.getString(prms.getField("@port")));
      Socket s = new Socket(server, port);
      OutputStream os = s.getOutputStream();
      byte[] len = Bytes.longToByteRevers(query.length, 4);
      os.write(len);
      os.flush();
      os.write(query);
      os.flush();

      InputStream is = s.getInputStream();
      FileOutputStream fos = new FileOutputStream(to.getCanonicalPath());
      try {
        int b;
        int i = 0;
        long l = 0;
        long k = 0;
        boolean needH = false;
        while ((b = is.read()) != -1) {
          if (i < len.length) {
            len[i] = (byte) b;
            i++;
            if (i == len.length)
              k = l = Bytes.byteReversToLong(len);
          } else {
            if (l == k) {
            }
            if (b == 0)
              break;
            fos.write(b);
            l--;
            if (l == 0)
              break;
          }
        }
      } finally {
        is.close();
        fos.close();
      }
      s.close();
      return to;
    } catch (SysException e) {
      throw new SysException(e);
    } catch (FileNotFoundException e) {
      throw new SysException(e);
    } catch (IOException e) {
      throw new SysException(e);
    }
  }
  public static void main(String[] args) throws IOException, SysException {
	  byte val[];
	  val = Bytes.longToByte32(1234567890);
	  
	  System.out.print(Bytes.byteToLong(val));
	  FileQueryRRAM fq = new FileQueryRRAM();
	  PrmInterface prms = new PrmInterfaceInst();
	  prms.setField("@port", args[1]);
	  prms.setField("@server", args[0]);
	  File f = new File(args[2]);
	  long l = f.length();
	  byte[] query = new byte[(int)l];
	  FileInputStream fis = new FileInputStream(f);
	  fis.read(query);
	  File to = File.createTempFile("rram", "res", new File("./temp"));
	  fq.sendFile(query, prms, to);
	  //to = File.createTempFile("rram", "res", new File("C:\\PROJECTS\\CENTER\\RUN\\temp"));
	  //fq.sendFile(query, prms, to);
  }
}
