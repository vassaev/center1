package center.task.api.rmi;

import java.net.InetAddress;
import java.rmi.*;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import ru.vassaev.core.container.AppInfo;
import ru.vassaev.core.thread.Processed;
import ru.vassaev.core.util.Strings;

public class Server implements Processed<AppInfo> {

	public Server() {
		super();
	}

  //private AppInfo ai;
	public void processing(AppInfo ai) throws Throwable {
    //this.ai = ai;
    Element el = ai.getDocInfo();
    NodeList children = el.getChildNodes();
    int port = Strings.parseIntegerNvl(el.getAttribute("port"), Registry.REGISTRY_PORT);
		System.out.println("port = " + port);
    String rmi_name = el.getAttribute("rmi_name");
		System.out.println("rmi_name = " + rmi_name);
    
    int l = children.getLength();
    Node n;
    Element e;
    String alias;
    String alias_native;
    String cls_native;
		System.out.println("Reading TaskAPI elements...");
    for (int i = 0; i < l; i++) {
    	n = children.item(i);
    	if (!(n instanceof Element))
    		continue;
    	e = (Element) n;
    	if (e.getTagName().equals("TaskAPI")) {
    		alias = e.getAttribute("alias");
				cls_native = e.getAttribute("cls_native");
				alias_native = e.getAttribute("alias_native");
				System.out.println("Registering of TaskAPI (alias = " 
						+ alias + ", cls_native = " + cls_native + ", alias_native = " + alias_native + ")");
    		ServerTaskAPI.register(alias
    				, cls_native
    				, alias_native);
    		
    	}
    	
    }
    String hostname = "localhost";
    InetAddress addr = InetAddress.getLocalHost();
    addr.getAddress();
    hostname = addr.getHostName();

    RMISecurityManager security = new RMISecurityManager();
		System.setSecurityManager(security);
		ServerTaskAPI server = new ServerTaskAPI();
		Registry registry = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
		registry.rebind(rmi_name, server);

		System.out.println("Server (rmi://" + hostname + ":" + port + "/" + rmi_name + ") ready...");
	}

}