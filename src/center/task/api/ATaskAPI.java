package center.task.api;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import center.task.State;
import ru.vassaev.core.ApplicationInfo;
import ru.vassaev.core.base.Null;
import ru.vassaev.core.container.AppInfo;
import ru.vassaev.core.container.ApplicationManager;
import ru.vassaev.core.exception.SysException;

abstract public class ATaskAPI implements ITaskAPI {
	private static Map<String, Map<ITaskAPI, Long>> subjects = new HashMap<String, Map<ITaskAPI, Long>>();
	public static Long getApplicationSubject(ITaskAPI ta) {
		String application_name = ApplicationManager.getCurrentAppInfo().getName();
		synchronized(subjects) {
			Map<ITaskAPI, Long> sbs = subjects.get(application_name);
			if (sbs == null) {
				sbs = new HashMap<ITaskAPI, Long>();
				subjects.put(application_name, sbs);
			}
			Long subject_id = sbs.get(ta);
			if (subject_id != null && subject_id > 0L) 
				return subject_id;
		}
		return 0L;
	}
	public static void setApplicationSubject(ITaskAPI ta, Long subject_id) {
		if (subject_id == null || subject_id == 0L) 
			return;
		String application_name = ApplicationManager.getCurrentAppInfo().getName();
		synchronized(subjects) {
			Map<ITaskAPI, Long> sbs = subjects.get(application_name);
			if (sbs == null) {
				sbs = new HashMap<ITaskAPI, Long>();
				subjects.put(application_name, sbs);
			}
			sbs.put(ta, subject_id);
		}
	}
	public static long getApplicationSubject(ITaskAPI ta, long old_subject_id) throws SysException {
		long subject_id = ta.getSubject(old_subject_id);
		String application_name = ApplicationManager.getCurrentAppInfo().getName();
		synchronized(subjects) {
			Map<ITaskAPI, Long> sbs = subjects.get(application_name);
			if (sbs == null) {
				sbs = new HashMap<ITaskAPI, Long>();
				subjects.put(application_name, sbs);
			}
			sbs.put(ta, subject_id);
		}
		return subject_id;
	}
	/**
	 * Распарсить полное имя на части - группу и имя
	 * 
	 * @param fullname
	 * @return
	 */
	public static String[] getName(String fullname) {
		if (Null.NULL.equals(fullname))
			return new String[] { null, null };
		int i = fullname.indexOf('/');
		if (i > 0)
			return new String[] { fullname.substring(0, i), fullname.substring(i + 1) };
		else if (i == 0)
			return new String[] { null, fullname.substring(i + 1) };
		return new String[] { null, fullname };
	}

	public final String getFullName(String grp, String name) {
		String fullname = grp;// grp
		if (fullname != null)
			fullname = fullname + "/" + name;
		else
			fullname = name;// name
		return fullname;
	}

	public final long setState(long id_subject, long id, State st)
			throws SysException {
		return setState(id_subject, id, st, null);
	}

	public final long setState(long id_subject, long id, State st,
			Long processor_id) throws SysException {
		// ITaskHelper.State.BLOCKED
		if (st != null) {
			if (State.BLOCKED.equals(st)) {
				return setBlocked(id_subject, id);
			} else if (State.BREAKING.equals(st)) {
				return setBreaking(id_subject, id);
			} else if (State.BROKEN.equals(st)) {
				return setBroken(id_subject, id);
			} else if (State.CANCELED.equals(st)) {
				return setCanceled(id_subject, id);
			} else if (State.DONE_ERR.equals(st)) {
				return setDoneErr(id_subject, id);
			} else if (State.DONE_OK.equals(st)) {
				return setDoneOk(id_subject, id);
			} else if (State.PROCESSING.equals(st)) {
				return setProcessing(id_subject, id);
			} else if (State.SCHEDULED.equals(st)) {
				return setScheduled(id_subject, id);
			} else if (State.READY.equals(st)) {
				return setReady(id_subject, id, processor_id);
			} else if (State.DONE_TOUT.equals(st)) {
				return setTimeout(id_subject, id);
			} else
				throw new SysException("Unsupported state " + st.name());
		}
		return 0;
	}

	public final long setReady(long subject_id, long id) throws SysException {
		return setReady(subject_id, id, null);
	}

	public final long setParamObject(long task_id, String name, Object value)
			throws SysException {
		return setParamObject(task_id, name, value, 0, 0, 0);
	}

	public static ITaskAPI getInstance(String className, Class<? extends ITaskAPI> defClass, String alias) throws SysException {
    Class<? extends ITaskAPI> cls_task_api = null;
    Method method_inst;
    try {
    	cls_task_api = (Class<? extends ITaskAPI>) Class.forName(className);
    	method_inst = cls_task_api.getMethod("getInstance", new Class[] {String.class});
    } catch (ClassNotFoundException e) {
    	cls_task_api = defClass;
    	try {
				method_inst = cls_task_api.getMethod("getInstance", new Class[] {String.class});
			} catch (NoSuchMethodException e1) {
				throw new SysException(e1);
			} catch (SecurityException e1) {
				throw new SysException(e1);
			}
		} catch (NoSuchMethodException e) {
			throw new SysException(e);
		} catch (SecurityException e) {
			throw new SysException(e);
		}
    try {
			return (ITaskAPI) method_inst.invoke(cls_task_api, new Object[]{alias});
		} catch (IllegalAccessException e1) {
			throw new SysException(e1);
		} catch (IllegalArgumentException e1) {
			throw new SysException(e1);
		} catch (InvocationTargetException e1) {
			throw new SysException(e1);
		}
	}
}
