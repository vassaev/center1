package center.task.api.mem;

import java.util.Set;

final class SBJ {
	final Long id;
	Set<String> classes;
	long last_ping;
	SBJ(long id) {
		this.id = id;
		last_ping = System.currentTimeMillis();
	}
	void ping() {
		last_ping = System.currentTimeMillis();
	}
}	
