package center.task.api.rmi;

import java.io.File;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import ru.vassaev.core.container.Container;
import ru.vassaev.core.exception.SysException;
import center.task.State;
import center.task.api.ITaskAPI;
import center.task.api.TaskCI;

public final class TaskAPI implements ITaskAPI {
	private static Map<String, Map<String, String>> prms = new HashMap<String, Map<String, String>>();

	public static void init(Element el, Container con) throws Throwable {
		NodeList children = el.getChildNodes();
		int l = children.getLength();
		Node n;
		Element e;
		String alias;
		String lookup;
		System.out.println("Reading TaskAPI elements...");
		for (int i = 0; i < l; i++) {
			n = children.item(i);
			if (!(n instanceof Element))
				continue;
			e = (Element) n;
			if (e.getTagName().equals("TaskAPI")) {
				alias = e.getAttribute("alias");
				lookup = e.getAttribute("lookup");
				synchronized (prms) {
					Map<String, String> values = prms.get(alias);
					if (values == null)
						values = new HashMap<String, String>();
					values.put("lookup", lookup);
					prms.put(alias, values);
				}
				System.out.println("Registering of TaskAPI (alias = " + alias
						+ ", lookup = " + lookup + ")");
			}

		}
	}

	private static Map<String, ITaskAPI> helpers = new TreeMap<String, ITaskAPI>();

	public static ITaskAPI getInstance(String alias) throws SysException {
		synchronized (helpers) {
			ITaskAPI obj = helpers.get(alias);
			if (obj != null) {
				return obj;
			} else {
				TaskAPI o = new TaskAPI(alias);
				helpers.put(alias, o);
				return o;
			}
		}
	}

	private InterfaceTaskAPI ta;
	private final String alias;
	private final Object sync;
	private final Map<Long, Set<String>> classes;

	private final void clearInterface() {
		synchronized (sync) {
			ta = null;
		}
	}

	private final InterfaceTaskAPI getInterface() throws SysException {
		synchronized (sync) {
			if (ta != null)
				return ta;
			return initInterface();
		}
	}

	private final InterfaceTaskAPI initInterface() throws SysException {
		String lookup;
		synchronized (prms) {
			Map<String, String> values = prms.get(alias);
			if (values == null)
				throw new SysException("Unregistered TaskAPI by alias '" + alias + "'");
			lookup = values.get("lookup");
		}
		InterfaceTaskAPI obj;
		try {
			obj = (InterfaceTaskAPI) Naming.lookup(lookup);
			synchronized (sync) {
				ta = obj.getInterfaceInstance(alias);
			}
			Set<String> s;
			synchronized(classes) {
				for (Map.Entry<Long, Set<String>> entry : classes.entrySet()) {
				  s = entry.getValue();
					try {
						ta.registerClasses(entry.getKey(), s);
					} catch (RemoteException e1) {
						throw new SysException(e1);
					}
				}
			}
			return ta;
		} catch (MalformedURLException e) {
			throw new SysException(e);
		} catch (RemoteException e) {
			throw new SysException(e);
		} catch (NotBoundException e) {
			throw new SysException(e);
		}
	}

	private TaskAPI(String alias) throws SysException {
		this.alias = alias;
		this.classes = new HashMap<Long, Set<String>>();
		this.sync = new Object();
		initInterface();
	}

	public void registerClasses(long subject_id, Set<String> hs)
			throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			Set<String> s = new HashSet<String>(hs);
			ta.registerClasses(subject_id, s);
			synchronized(classes) {
				classes.put(subject_id, s);
			}
			return;
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public String getAlias() throws SysException {
		return alias;
	}

	public long getSubject(long old_subject_id) throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.getSubject(old_subject_id);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public long setParamObject(long task_id, String name, Object value)
			throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.setParamObject(task_id, name, value);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public long setParamObject(long task_id, String name, Object value,
			long wait, long calc, long scall) throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.setParamObject(task_id, name, value, wait, calc, scall);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public long createTask(long subject_id, String cls) throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.createTask(subject_id, cls);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public long createTask(long subject_id, String cls, long parent_task_id)
			throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.createTask(subject_id, cls, parent_task_id);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public String getClassName(long id) throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.getClassName(id);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	/*
	 * public boolean checkDNF(long id, @SuppressWarnings("rawtypes") DNF dnf,
	 * Object[] flds) throws SysException { try { return ta.checkDNF(id, dnf,
	 * flds); } catch (RemoteException e) { throw new SysException(e); } } //
	 */
	public long setTaskDT(long id, String dt, String duration,
			String block_duration, Double arch_day) throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.setTaskDT(id, dt, duration, block_duration, arch_day);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public long setTaskError(long id, Throwable e) throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.setTaskError(id, e);
		} catch (RemoteException e1) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public String getParamTask(long id, String name) throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.getParamTask(id, name);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public Map<String, Object> getGroupParams(long id, String group)
			throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.getGroupParams(id, group);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}
  /*
	public Reader getTaskParamLong(long id, String name) throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.getTaskParamLong(id, name);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}
  //*/
	public State getState(long id) throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.getState(id);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public long setState(long subject_id, long id, State st) throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.setState(subject_id, id, st);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public long setState(long subject_id, long id, State st, Long processor_id)
			throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.setState(subject_id, id, st, processor_id);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public long setReady(long subject_id, long id) throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.setReady(subject_id, id);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public long setReady(long subject_id, long id, Long processor_id)
			throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.setReady(subject_id, id, processor_id);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public long setBlocked(long subject_id, long id) throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.setBlocked(subject_id, id);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				clearInterface();
				e.printStackTrace();
				attempt--;
				if (attempt > 0) {
					ta = initInterface();
				} else
					throw new SysException(e);
		}
	}

	public TaskCI getBlocked(long subject_id, long wait) throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
			try {
				return ta.getBlocked(subject_id, wait);
			} catch (RemoteException e) {
				clearInterface();
				e.printStackTrace();
				attempt--;
				if (attempt > 0) {
					ta = initInterface();
				} else
					clearInterface();
					e.printStackTrace();
					attempt--;
					if (attempt > 0) {
						ta = initInterface();
					} else
						throw new SysException(e);
			}
	}

	public long setProcessing(long subject_id, long id) throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.setProcessing(subject_id, id);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public long setTimeout(long subject_id, long id) throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.setTimeout(subject_id, id);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public long setScheduled(long subject_id, long id) throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.setScheduled(subject_id, id);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public long setBreaking(long subject_id, long id) throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.setBreaking(subject_id, id);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public long setBroken(long subject_id, long id) throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.setBroken(subject_id, id);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public long setCanceled(long subject_id, long id) throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.setCanceled(subject_id, id);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public long setDoneErr(long subject_id, long id) throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.setDoneErr(subject_id, id);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public long setDoneOk(long subject_id, long id) throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.setDoneOk(subject_id, id);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public State waitFinished(long subject_id, long id, long msec)
			throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.waitFinished(subject_id, id, msec);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public long log(long subject_id, long id, String info, boolean base)
			throws SysException {
		System.out.println(this.alias + " : " + subject_id + " : " + id + " :\t" + info);
		if (base == false)
			return 0;
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.log(subject_id, id, info, base);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public long log(long subject_id, long id, String info) throws SysException {
		System.out.println(this.alias + " : " + subject_id + " : " + id + " :\t" + info);
		return 0;
	}

	public void writeLogToFile(long id, File file, String charset)
			throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			ta.writeLogToFile(id, file, charset);
			return;
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

	public Object getParam(long id, String fullname) throws SysException {
		InterfaceTaskAPI ta = getInterface();
		int attempt = 2;
		while (true)
		try {
			return ta.getParam(id, fullname);
		} catch (RemoteException e) {
			clearInterface();
			e.printStackTrace();
			attempt--;
			if (attempt > 0) {
				ta = initInterface();
			} else
				throw new SysException(e);
		}
	}

}
