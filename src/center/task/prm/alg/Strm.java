package center.task.prm.alg;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.w3c.dom.Element;

import ru.vassaev.core.base.Null;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.io.OutputByteBuffer;
import ru.vassaev.core.thread.Process;
import ru.vassaev.core.types.TimeList;
import ru.vassaev.core.types.TimeList.Type;
import ru.vassaev.core.util.Strings;
import ru.vassaev.core.xml.XMLFileWaiter;
import center.task.CalculateException;
import center.task.Context;
import center.task.Context.WaitValue;
/**
 * Алгоритм преобразования в поток
 * @author Vassaev A.V. 
 * @version 1.0 31.07.2012
 *
 */
public class Strm extends LinkParam {
	private String encode;
	private boolean skip_header_xml;
	private int buf_size;
  public Strm(center.task.prm.Type tp, String owner, Element e) {
		super(tp, owner, e);
		skip_header_xml = Strings.parseBooleanNvl(e.getAttribute("skip-header-xml"), false);
		encode = e.getAttribute("encode");
		if (encode == null || encode.length() == 0)
			encode = "utf-8";
		buf_size = Strings.parseIntegerNvl(e.getAttribute("buf_size"), 8000);
		if (buf_size < 512)
			buf_size = 512;
	}

	public Object calculate(TimeList tl, Context cntx)  throws CalculateException {
		try {
			tl.addPointTime(Type.START);
			Object o = super.calculate(tl, cntx);
			if (Null.equ(o))
				return o;
			if (o instanceof WaitValue)
				try {
					o = ((WaitValue) o).getValueWait();
				} catch (Throwable e1) {
					throw new SysException(e1);
				}
			if (Null.equ(o))
				return o;
			OutputByteBuffer obb = null;
			if (o instanceof Element) {
				String n = ((Element) o).getNodeName();
				if (skip_header_xml) {
	      	try {
	      		obb = Process.getByteBuffer(buf_size, ("<"+n).getBytes(encode));
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	      } else
	      	obb = Process.getByteBuffer(buf_size);
	      XMLFileWaiter.putStream((Element)o, obb, encode);
	      try {
					obb.close();
		      return obb;
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if (o instanceof OutputByteBuffer) {
				return o;
			} else if (o instanceof String) {
				obb = Process.getByteBuffer(buf_size);
				try {
					obb.write(((String) o).getBytes(encode));
					obb.close();
					return obb;
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (o instanceof StringBuffer) {
				obb = Process.getByteBuffer(buf_size);
				try {
					obb.write(((StringBuffer) o).toString().getBytes(encode));
					obb.close();
					return obb;
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (o instanceof File) {
				obb = Process.getByteBuffer(buf_size);
				try {
					FileInputStream is = new FileInputStream(((File)o).getCanonicalPath());
					try {
					  obb.write(is);
					  obb.close();
					} finally {
						is.close();
					}
					return obb;
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return null;
		} catch (SysException e) {
			return null;
		} finally {
			tl.addPointTime(Type.END);
		}
	}

}
