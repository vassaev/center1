package center.task.prm;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import center.task.*;
import center.task.Context.WaitValue;
import center.task.api.ITaskAPI;
import center.task.prm.alg.*;

import ru.vassaev.core.XMLPath;
import ru.vassaev.core.thread.LogEvent;
import ru.vassaev.core.thread.LogLevel;
import ru.vassaev.core.types.TimeList;
import ru.vassaev.core.base.Null;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.util.Strings;
import ru.vassaev.core.util.Xml;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Абстракция параметра с означиванием по параметру cache и типизацией
 *
 * @author Vassaev A.V.
 * @version 1.0 05.04.2010
 */
public abstract class Type extends IDependentParam implements Saved {

	private int priority = 0;
  protected boolean trace = false;
  public final boolean getTrace() {
  	return trace;
  }
  protected boolean log = true;
  public final boolean getLog() {
  	return log;
  }
  protected String name = null;
  protected String grp = null;
  protected String fullname = null;
  protected String gl_name = null;
  protected String space = null;
  protected boolean isCache = false;
  protected boolean isNonDB = false;
  protected Object from;
  protected String index;
  protected boolean nosave = false;
  protected boolean isRootName = false;
  //protected enum ISOLATION {TASK, THREAD};
  //protected ISOLATION isolation = ISOLATION.TASK;
  // Состояние задачи, при котором выполняется сохранение параметра в базе данных
  private State save_state;
  // Момент "времени", когда сохраняется параметр в базе данных
  private TIME save_time;
  // Модификатор
  private Modify modify;
  // Установка значения
  private Map<String, PrmCopy> copyTo = null;

  public boolean isNoSave() {
  	return nosave;
  }
  public Object getFromSection() {
  	return from;
  }
  /**
   * Получение объекта, реализующего алгоритм расчета
   *
   * @return Объект алгоритма
   */
  protected final Alg getAlg() {
    if (Null.equ(from))
      return null;
    if (from instanceof Alg)
      return (Alg) from;
    return null;
  }
  public final String getFullName() {
    return fullname;
  }

  public final String getGLName() {
    return gl_name;
  }

  public Type(Element e, ATaskInfo owner) throws SysException {
    super();
    space = e.getNodeName();
    log = Strings.parseBooleanNvl(e.getAttribute("log"), true); // Запрет логирования
    trace = Strings.parseBooleanNvl(e.getAttribute("trace"), false);
    name = e.getAttribute("name"); // Имя
    grp = e.getAttribute("grp");   // Группа
    // Полное имя
    fullname = (grp == null || grp.length() == 0) ? name : grp + "/" + name;
    gl_name = (fullname == null || fullname.length() == 0) ? space : space + "." + fullname;
    // Кэшируемый параметр или нет
    String cache = null;
    if (e.hasAttribute("cache"))
    	cache = e.getAttribute("cache");
    if (cache == null)
      cache = Strings.getXMLValue(e, "cache");
    if (cache == null)
      cache = Strings.getXMLValue(e, "cash");
    setCache(Strings.parseBooleanNvl(cache, false));
    // Определяем алгоритм расчета
    Element f = (Element) Strings.getXMLObject(e, "from");
    if (f == null) {
      from = new XMLPath("value").getXMLObject(e);
      if (from != null && from instanceof Element)
        from = new XMLValue(this, gl_name, (Element) from);
    } else {
      String source = f.getAttribute("source");
      if (source == null) {
        from = Null.NULL;
      } else if ("switch".equals(source)) {//Выбор варианта
        from = new Switch(this, gl_name, f);
      } else if ("param".equals(source)) {//Ссылка на параметр
        from = new LinkParam(this, gl_name, f);
      } else if ("zip".equals(source)) {//Архивировать/разархивировать значение параметр
        from = new ArchiveLinkParam(this, gl_name, f);
      } else if ("select".equals(source)) {//QML
        from = new Sel(this, gl_name, f);
      } else if ("update".equals(source)) {//DML
        from = new Upd(this, gl_name, f);
      } else if ("task".equals(source)) {//Параметр в таблице задач
        from = new TaskParam(this, gl_name, f);
      } else if ("thread".equals(source)) {//Параметр потока
        from = new TimeThread(this, gl_name, f);
      } else if ("sysprop".equals(source)) {//Системные свойства
        from = new SysProp(this, gl_name, f);
      } else if ("sysenv".equals(source)) {//Системные переменные
        from = new Env(this, gl_name, f);
      } else if ("java_code".equals(source)) {//Код java
        from = new JavaProc(this, gl_name, f);
      } else if ("java_func".equals(source)) {//Готовая функция java
        from = new JavaFunc(this, gl_name, f);
      } else if ("task_sys".equals(source)) {//Задача системы
        from = new TaskCenter(this, gl_name, f, owner);
      } else if ("processor".equals(source)) {//Процессор
        from = new TaskProcessor(this, gl_name, f, owner);
      } else if ("merge".equals(source)) {//Объединение
        from = new Merge(this, gl_name, f);
      } else if ("stream".equals(source)) {//Поток
        from = new Strm(this, gl_name, f);
      } else if ("file".equals(source)) {//Файл
        from = new FileLink(this, gl_name, f);
      } else if ("file_del".equals(source)) {//Удаление файла
        from = new FileDel(this, gl_name, f);
      } else if ("first".equals(source)) {//первый из множества
        from = new TheFirst(this, gl_name, f);
      } else {
        from = Null.NULL;
      }
    }
    String db = null;
    if (e.hasAttribute("priority"))
    		db = e.getAttribute("priority");
    else 
    	db = Strings.getXMLValue(e, "priority");
    setNonDB("only_memory".equals(db));
    if (e.hasAttribute("index"))
  		index = e.getAttribute("index");
    else
    	index = Strings.getXMLValue(e, "index");
    if (Strings.isEmpty(index))
      isRootName = (Strings.getXMLObject(e, "rootname") != null);
    nosave = (Strings.getXMLObject(e, "nosave") != null);

    // Видимость результата расчета внутри потока/задачи
    /*
      String isltn = Strings.getXMLValue(e, "isolation");
      if (ISOLATION.THREAD.name().equals(isltn))
        isolation = ISOLATION.THREAD;
      */

    String save_state_str = Strings.getXMLValue(e, "save#state");
    if (save_state_str != null)
      save_state = State.valueOf(save_state_str);
    String save_time_str = Strings.getXMLValue(e, "save#time");
    if (save_time_str != null)
      save_time = TIME.valueOf(save_time_str);
    priority =  Strings.parseIntegerNvl(Strings.getXMLValue(e, "save#priority"), 0);
    NodeList x = Xml.getElementsByTagName(e, "modify");
    if (x != null && x.getLength() > 0)
    	modify = new Modify((Element)x.item(0));
    x = Xml.getElementsByTagName(e, "copyTo");
    if (x != null) {
    	copyTo = new HashMap<String, PrmCopy>();
    	for (int i = 0; i < x.getLength(); i++) {
    		Element to = (Element)x.item(i);
    		PrmCopy p = new PrmCopy(to);
    		copyTo.put(p.fullname, p);
    	}
    }
    x = Xml.getElementsByTagName(e, "log");
    if (x != null) {
    	for (int i = 0; i < x.getLength(); i++) {
    		Element ll = (Element)x.item(i);
    		String loglevel = ll.getAttribute("loglevel");
    		String events = ll.getAttribute("events");
    		LogLevel p = new LogLevel(loglevel);
    		String[] eventlist = Strings.parseXVSLine(events, '\\', '|');
    		for (String event : eventlist) {
    			LogEvent evnt = LogEvent.valueOf(event);
    			this.setLogLevel(evnt, p);
    		}
    	}
    }
    Element ext = (Element)Strings.getXMLObject(e, "exception");
    if (ext != null)
      whenException = Prm.getPrm(ext);
  }
  /**
   * 
   */
  private static class PrmCopy extends Prm {
  	public final boolean cloneValue;
  	public PrmCopy(Element e) {
  		super(e);
  		cloneValue = Strings.parseBooleanNvl(e.getAttribute("clone"), false);
  	}
  	public boolean setObject(Context cntx, Object obj) throws SysException {
  		/*TODO с родительскими параметрами надо что-то делать*/
      if (isParent)
      	return false; 
      return cntx.setPrmByFullName(fullname, obj, cloneValue);
  	}
  }

  public Type() {
    super();
  }

  //*
  public final Object getValue(Context cntx) throws CalculateException {
    TimeList tl = cntx.getTimeList(gl_name, this);
    tl.addPointTime(TimeList.Type.START);
    try {
      return calculate(tl, cntx);
    } finally {
      tl.addPointTime(TimeList.Type.END);
    }
  }

  //*/
  public final void setCache(boolean isCache) {
    this.isCache = isCache;
  }

  public final boolean isCache() {
    return isCache;
  }

  public final void setNonDB(boolean isNonDB) {
    this.isNonDB = isNonDB;
  }

  public final boolean isNonDB() {
    return isNonDB;
  }
  /**
   * Расчёт без модификации
   * @param tl - временное логирование 
   * @param cntx - контекст
   * @return - результат расчёта
   */
  protected Object calculate0(TimeList tl, Context cntx) throws CalculateException {
    if (Null.equ(from))
      return Null.NULL;
    if (from instanceof String)
      return from;
    Object v;
    if (from instanceof Element) {
      if (isRootName)
        return ((Element)from).getNodeName();
      if (index != null) 
        return Strings.getXMLObject((Element) from, index);
      return from;
    } else if (from instanceof Alg) {
      v = ((Alg) from).calculate(tl, cntx);
    } else if (from instanceof IDependentParam) {
      IDependentParam frm = (IDependentParam) from;
      try {
        v = cntx.getPrmByFullName(frm.getFullName());
      } catch (SysException e) {
        e.printStackTrace();
        v = null;
      }
    } else v = from;

    if (Null.NULL.equals(v))
      return Null.NULL;
    if (v instanceof Document) {
      v = ((Document) v).getDocumentElement();
    }
    if (v instanceof Element) {
      Element e = (Element) v;
      if (isRootName)
        return e.getNodeName();
      if (index != null)
        return Strings.getXMLObject(e, index);
    } else if (v instanceof Record) {
      if (index != null)
        return ((Record) v).getObject(index);
    } else if (v instanceof Map) {
        if (index != null)
            return ((Map<?,?>) v).get(index);
    } else if (v instanceof Context) {
      if (index != null)
        try {
          return ((Context) v).getObjectByName(index);
        } catch (SysException e) {
        	e.printStackTrace();
          return null;
        }
    } 
    return v;
  }
  /**
   * Копирование параметра в параметры
   * @param cntx
   * @param v
   */
  public final void executeCopyTo(Context cntx, Object v) {
  	if (copyTo != null)
  		for (Map.Entry<String, PrmCopy> p : copyTo.entrySet() ) try {
  			p.getValue().setObject(cntx, v);
  		} catch (SysException e) {
  			e.printStackTrace();
  		}
  }
  protected Object modify(TimeList tl, Context cntx, Object v) throws CalculateException {
  	if (modify != null)
  		return modify.exec(tl, cntx, v);
    return v;
  }
  protected Object whenException = null;
  public Object calculate(TimeList tl, Context cntx) throws CalculateException {
  	LogEvent old = setEvent(LogEvent.CALC);
  	try {
  		Object v = calculate0(tl, cntx);
  		if (modify != null)
  			v = modify.exec(tl, cntx, v);
  		return v;
  	} catch(CalculateException e) {
  		if (whenException == null)
  			throw e;
  		else {
  			if (whenException instanceof Prm)
					try {
						return ((Prm)whenException).getObject(cntx);
					} catch (SysException e1) {
						Throwable cause = e1.getCause();
						if (cause instanceof CalculateException)
							throw (CalculateException)cause;
						throw new CalculateException(fullname, e1);
					}
  			return whenException;
  		}
  	} finally {
  		setEvent(old);
  	}
  }

  public final State stateSaved() {
    return save_state;
  }

  public final TIME timeSaved() {
    return save_time;
  }

  public final void save(Context cntx) throws SysException {
    if (gl_name.indexOf("prm.") != 0)
      return;
    ITaskAPI ta = cntx.ta;
    long id_task = cntx.id_task;
    Object val = cntx.getPrmByFullName(fullname);
    if (val != null) {
    	if (val instanceof Prm) {
      	Prm p = (Prm)val;
      	try {
  				val = p.getObject(cntx);
  			} catch (CalculateException e) {
  				// TODO Auto-generated catch block
  				e.printStackTrace();
  			}
    	} else if (val instanceof WaitValue) {
    		WaitValue p = (WaitValue)val;
    		try {
					val = p.getValueWait();
				} catch (Throwable e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    	}
    }
    TimeList tl = cntx.getTimeList(gl_name, this);
    long w = tl.getWaitTime();
    long c = tl.getWorkTime();
    long sc = tl.getSystemCallTime();
    ta.setParamObject(id_task, fullname, val, w, c, sc);
  }

  public Set<String> depedentOn() {
    if (Null.equ(from))
      return null;
    if (from instanceof Alg)
      return ((Alg) from).depedentOn();
    if (from instanceof IDependentParam)
      return ((IDependentParam) from).depedentOn();
    return null;
  }

  public Set<String> depedentOnParent() {
    if (Null.equ(from))
      return null;
    if (from instanceof Alg)
      return ((Alg) from).depedentOnParent();
    if (from instanceof IDependentParam)
      return ((IDependentParam) from).depedentOnParent();
    return null;
  }
  
  public int priority() {
  	return priority;
  }
}
