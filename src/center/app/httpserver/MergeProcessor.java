package center.app.httpserver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Set;

import ru.vassaev.core.base.Null;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.thread.Process;
import ru.vassaev.core.xml.FindStringStream;
import ru.vassaev.core.xml.FindStringStream.Skipped;
import center.task.AProcessor;
import center.task.Context;
import center.task.State;
import center.task.TaskException;

public class MergeProcessor extends AProcessor {
	private Object source;

	public static class Prm {
		String name;

		public Prm(String val) {
		}
	};

	public static class End {
		public End(String val) {
		}
	};

	public State paramsValidateAndPrepare(Context cntx) throws SysException {
		source = cntx.getPrmByFullName("source");
		if (Null.equ(source))
			throw new SysException("Parameter \"source\" isn't set");
		return null;
	}

	public State process(Context cntx) throws TaskException, SysException,
			InterruptedException {
		State st = paramsValidateAndPrepare(cntx);
		if (st != null)
			return st;
		Process prc = Process.currentProcess();
		FindStringStream fss = new FindStringStream();
		fss.add("<%prm.", Prm.class);
		fss.add("%>", End.class);
		fss.setOutSkipped(true);
		// //////
		FileInputStream fis = null;
		java.io.InputStreamReader isr = null;
		try {
			if (source instanceof File) {
				try {
					fis = new FileInputStream(((File) source).getCanonicalPath());
					prc.regResource(fis);
					isr = new java.io.InputStreamReader(fis, "utf-8");
					prc.regResource(isr);
				} catch (FileNotFoundException e) {
					throw new TaskException(State.DONE_ERR, e);
				} catch (UnsupportedEncodingException e) {
					try {
						fis.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					throw new TaskException(State.DONE_ERR, e);
				} catch (IOException e) {
					try {
						fis.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					throw new TaskException(State.DONE_ERR, e);
				}
			}
			StringBuffer sb = new StringBuffer();
			prc.regResourceName(sb, "msg.full");
			int ch;
			Object r;
			Object prev = null;
			Object o = null;
			try {
				while ((ch = isr.read()) != -1) {
					fss.write((char) ch);
					if (fss.available() > 0)
						while ((r = fss.readObject()) != null) {
							if (r instanceof Skipped) {
								if ((prev != null) && (prev instanceof Prm)) {
									((Prm) prev).name = r.toString();
									try {
										o = cntx.getPrmByFullName(((Prm) prev).name);
									} catch(Throwable e) {
										o = e.getMessage();
									}
									String v = o.toString();
									prc.regResourceName(v, "msg.part");
									if (!Null.equ(v))
										sb.append(v);
									prev = null;
								} else {
									String v = ((Skipped) r).getValue();
									prc.regResourceName(v, "msg.part");
									if (!Null.equ(v))
										sb.append(v);
								}
								cntx.getPrmByFullName("update");
							} else if (r instanceof Prm) {
								prev = r;
								// System.out.println(r.toString() + "\t=\t" + r.getClass());
							} else {
								prev = null;
							}
						}
				}
				fss.write(FindStringStream.EOF);

				if (fss.available() > 0)
					while ((r = fss.readObject()) != null)
						if (r instanceof Skipped) {
							if ((prev != null) && (prev instanceof Prm)) {
								((Prm) prev).name = r.toString();
								try {
									o = cntx.getPrmByFullName(((Prm) prev).name);
								} catch(Throwable e) {
									o = e.getMessage();
								}
								String v = o.toString();
								prc.regResourceName(v, "msg.part");
								if (!Null.equ(v))
									sb.append(v);
								prev = null;
							} else {
								String v = ((Skipped) r).getValue();
								prc.regResourceName(v, "msg.part");
								if (!Null.equ(v))
									sb.append(v);
							}
							cntx.getPrmByFullName("update");
						} else if (r instanceof Prm) {
							prev = r;
							// System.out.println(r.toString() + "\t=\t" + r.getClass());
						} else {
							prev = null;
						}
			} catch (IOException e) {
				throw new TaskException(State.DONE_ERR, e);
			}
		} finally {
			if (fis != null)
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			if (isr != null)
				try {
					isr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		// //////
		cntx.getPrmByFullName("finish");
		return State.DONE_OK;
	}

	@Override
	public Set<String> dependentOn() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<String> loopDependentOn() {
		// TODO Auto-generated method stub
		return null;
	}

}
