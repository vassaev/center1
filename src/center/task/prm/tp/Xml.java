package center.task.prm.tp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import ru.vassaev.core.io.OutputByteBuffer;
import ru.vassaev.core.thread.LogEvent;
import ru.vassaev.core.types.TimeList;
import ru.vassaev.core.base.Null;
import ru.vassaev.core.container.Container;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.util.Strings;
import ru.vassaev.core.xml.XMLFileWaiter;
import center.task.CalculateException;
import center.task.Context;
import center.task.Record;
import center.task.ATaskInfo;
import center.task.prm.Dif;
import center.task.prm.Type;

/**
 * Параметр типа xml-документ
 *
 * @author Vassaev A.V.
 * @version 1.0
 */
public class Xml extends Type implements Dif {

  private String encoding;
  private boolean isHeader;

  public Xml(Element e, ATaskInfo owner) throws SysException {
    super(e, owner);
    if (e.hasAttribute("encoding"))
    	encoding = e.getAttribute("encoding");
    else
    	encoding = Strings.getXMLValue(e, "encoding");
    if (encoding == null)
      encoding = "UTF-8";
    isHeader = Strings.parseBooleanNvl(Strings.getXMLValue(e, "header"), false);
  }

	public Object calculate(TimeList tl, Context cntx) throws CalculateException {
  	LogEvent old = setEvent(LogEvent.CALC);
  	try {
  		tl.addPointTime(TimeList.Type.START);
      Object v = super.calculate0(tl, cntx);
      if (Null.NULL.equals(v))
        return null;
      if (v instanceof Document || v instanceof Element) {
      	v = modify(tl, cntx, v); 
        return v;
      }
      if (v instanceof Record) {
        v = Strings.getString(((Record) v).value.get(0));
      }
      if (v instanceof OutputByteBuffer)
      	v = ((OutputByteBuffer)v).getIS();
      else if (v instanceof File) {
      	File f = (File)v;
      	OutputByteBuffer obb = null;
      	try {
        	FileInputStream is = new FileInputStream(f.getCanonicalPath());
        	obb = ru.vassaev.core.thread.Process.getByteBuffer(8000, is);
      		is.close();
      	  obb.close();
        	v = obb.getIS();
      	} catch(IOException e) {
      		throw new CalculateException(gl_name, e);
      	}	
      }
      InputStream dh;
      if (v == null)
        return null;
      if (v instanceof String || v instanceof StringBuffer) {
        String val = v.toString().trim();
        if (val.indexOf("<?") != 0) {
          val = "<?xml version=\"1.0\" encoding=\"" + encoding + "\"?>" + val;
        }
        try {
          dh = new java.io.ByteArrayInputStream(val.getBytes(encoding));
        } catch (UnsupportedEncodingException e) {
          throw new CalculateException(gl_name, e);
        }
      } else if (v instanceof InputStream) {
        if (!isHeader) {
          dh = (InputStream) v;
        } else {
          InputStream h;
          try {
            h = new java.io.ByteArrayInputStream(
                    ("<?xml version=\"1.0\" encoding=\"" + encoding + "\"?>")
                            .getBytes(encoding));
          } catch (UnsupportedEncodingException e) {
            throw new CalculateException(gl_name, e);
          }
          dh = new java.io.SequenceInputStream(h, (InputStream) v);
        }
      } else
        throw new CalculateException(gl_name, "The source's type isn't supported:" + v.getClass().getCanonicalName());
      try {
        Document d = XMLFileWaiter.getDocument(dh, encoding);
        if (d != null)
          return modify(tl, cntx, d.getDocumentElement());
        return null;
      } finally {
      	try {
  				dh.close();
  			} catch (IOException e) {
  				e.printStackTrace();
  			}
      }
  	} finally {
    	setEvent(old);
  		tl.addPointTime(TimeList.Type.END);
  	}
  }
                                        
  public Object getValueByIndex(Object v, String index) {
    return Strings.getXMLObject((Element) v, index);
  }

  public static Object getValue(Object v, String index) {
    return Strings.getXMLObject((Element) v, index);
  }
  
}
