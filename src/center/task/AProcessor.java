package center.task;

import java.io.*;
import java.lang.reflect.Method;
import java.util.*;

import ru.vassaev.core.thread.Process;
import ru.vassaev.core.thread.PoolThread;
import ru.vassaev.core.base.Null;
import ru.vassaev.core.base.WValue;
import ru.vassaev.core.container.AppInfo;
import ru.vassaev.core.container.ApplicationManager;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.exception.SysRuntimeException;
import ru.vassaev.core.Pool;
import ru.vassaev.core.util.Strings;
import center.mail.MailSender;
import center.task.api.ITaskAPI;
import center.task.prm.IDependentParam;
import center.task.prm.Saved;

public abstract class AProcessor implements IProcessor {
	public AProcessor() {
		super();
	}

	public IProcessor newInstance() {
		IProcessor prc;
		try {
			prc = getClass().newInstance();
		} catch (InstantiationException e) {
			throw new SysRuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new SysRuntimeException(e);
		}
		return prc;
	}

	public final void regResourceName(Object resource, String name) {
		ru.vassaev.core.thread.Process prc = ru.vassaev.core.thread.Process
				.currentProcess();
		prc.regResourceName(resource, name);
		try {
			cntx.log(false, "Registered resource by name \'", name, "\':", resource);
		} catch (SysException e) {
			e.printStackTrace();
		}
	}

	public final PoolThread getProcessPoolByName(String name) throws SysException {
		String poolProcessName = Strings.getString(cntx.getPrmString(name));
		if (poolProcessName == null)
			throw new SysException("Parameter \"" + name + "\" isn't set");
		AppInfo ai = ApplicationManager.getCurrentAppInfo();
		Pool<?> poolprc = ai.getContainer().getRM().getPool(poolProcessName);
		if (Null.equ(poolprc))
			throw new SysException("There is no pool by name \"" + poolProcessName
					+ "\"");
		try {
			PoolThread prcs = (PoolThread) poolprc;
			return prcs;
		} catch (ClassCastException e) {
			throw new SysException("The pool by name \"" + poolProcessName
					+ "\" dosn't contain process' object");
		}
	}

	private Context cntx = null;
	private long id_processor;
	private boolean isRun = false;

	public ATaskInfo getTaskInfo() {
		return this.cntx.info;
	}

	public final long getProcessorID() {
		return id_processor;
	}

	public final Map<String, Object> getPRMS() {
		return this.cntx.info.prms;
	}

	public final Context getContext() {
		return this.cntx;
	}

	public final long getTaskID() {
		return this.cntx.id_task;
	}

	public final ITaskAPI getTaskAPI() {
		return this.cntx.ta;
	}

	public final boolean isBusy() {
		return isRun;
	}

	private Pool<Process> prcpool = null;

	public Pool<ru.vassaev.core.thread.Process> getPoolProcess() {
		return prcpool;
	}

	public void setPoolProcess(Pool<ru.vassaev.core.thread.Process> pool) {
		this.prcpool = pool;
	}

	public NewTaskInfo getChild() {
		return ((TaskInfo) getTaskInfo()).child;
	}

	public void start(ru.vassaev.core.thread.Process prc, long id_processor,
			long id_task, ITaskAPI ta, TaskInfo ti) throws SysException {
		synchronized (this) {
			if (isRun)
				throw new SysException("The processor is busy.");
			isRun = true;
		}
		this.id_processor = id_processor;
		cntx = new Context(id_processor, id_task, ta, ti);
		ta.log(id_processor, id_task, "Starting...");
		prc.setName(ti.clsName);
		prc.setTarget(this);
		try {
			prc.start(cntx);
		} catch (RuntimeException e) {
			synchronized (this) {
				isRun = false;
			}
			throw e;
		}
	}

	private void sendLog(State st) throws SysException {
		TaskInfo info = (TaskInfo) getTaskInfo();
		ITaskAPI ta = getTaskAPI();
		long id_task = getTaskID();
		if (info != null && info.mails != null) {
			TaskInfo.MailInfo mi = (TaskInfo.MailInfo) info.mails.get(st.name());
			if (mi != null) {
				File x;
				try {
					x = Process.getTempFile();
				} catch (IOException e) {
					throw new SysException(e);
				}
				try {

					MailSender ms = new MailSender();
					ms.setInitProperty("mail.smtp.host", mi.smtphost);
					ms.setInitProperty("mail.smtp.port", mi.smtpport);
					ms.setInitProperty("mail.smtp.user", mi.smtpuser);
					ms.setInitProperty("mail.smtp.pwd", mi.smtppwd);
					ms.init();

					ta.writeLogToFile(id_task, x, "UTF-8");

					if (mi.to != null)
						ms.setStepProperty("recipient", mi.to);
					if (mi.bcc != null)
						ms.setStepProperty("h_recipient", mi.bcc);
					if (mi.sender != null)
						ms.setStepProperty("sender", mi.sender);
					if (mi.subject != null)
						ms.setStepProperty("subject", mi.subject);
					if (mi.body != null)
						ms.setStepProperty("body", mi.body);

					ms.setStepProperty("files", new File[] { x });
					ms.setStepProperty("descriptions", new String[] { "log.utf-8.doc" });
					ms.setStepProperty("filenames", new String[] { "log.utf-8.doc" });

					ms.exec();
				} finally {
					Process.freeTempFile(x);
				}
			}
		}
	}

	/**
	 * Запуск заданий на выполнение
	 */
	/*
	 * public final int runTasksFromDNF(long id, TaskInfo.Cntxt.Time time) throws
	 * SysException { TaskInfo info = (TaskInfo)getTaskInfo(); ITaskAPI ta =
	 * getTaskAPI(); int cnt = 0; ArrayList<center.task.TaskInfo.DNF> dnfs =
	 * info.getDNF(cntx); for (int i = 0; i < dnfs.size(); i++) {
	 * center.task.TaskInfo.DNF d = dnfs.get(i); if (ta.checkDNF(id, d,
	 * d.flds.toArray())) { center.task.TaskInfo.Cntxt cntx = d.getContext(); if
	 * (!time.isPartOf(cntx.time)) continue; long nid =
	 * ta.createTask(id_processor, cntx.cls); Iterator<String> keys =
	 * cntx.prms.keySet().iterator(); while (keys.hasNext()) { String k =
	 * keys.next(); Object o = cntx.prms.get(k); ta.setParamObject(nid, k,
	 * o.toString()); } ta.setTaskDT(nid, cntx.dt, cntx.duration,
	 * cntx.block_duration, null); ta.setReady(id_processor, nid, null); cnt++; }
	 * } // Если ни один DNF не сработал if (cnt == 0) {
	 * center.task.TaskInfo.Cntxt cntx = info._else; if ((cntx != null) &&
	 * (cntx.cls != null) && time.isPartOf(cntx.time)) { long nid =
	 * ta.createTask(id_processor, cntx.cls); Iterator<String> keys =
	 * cntx.prms.keySet().iterator(); while (keys.hasNext()) { String k =
	 * keys.next(); Object o = cntx.prms.get(k); ta.setParamObject(nid, k,
	 * o.toString()); } ta.setTaskDT(nid, cntx.dt, cntx.duration,
	 * cntx.block_duration, null); ta.setReady(id_processor, nid, null); } }
	 * return cnt; } //
	 */
	public final void saveParams(Saved.TIME tm) {
		ATaskInfo info = getTaskInfo();
		ArrayList<Saved> saves = new ArrayList<Saved>();
		String k;
		Object o;
		Saved s;
		Saved.TIME t;
		int p;
		int i;
		for (Map.Entry<String, Object> e : info.prms.entrySet()) {
			k = e.getKey();
			o = e.getValue();
			if (o != null && o instanceof Saved) {
				s = (Saved) o;
				t = s.timeSaved();
				p = s.priority();
				if (t != null && t.equals(tm)) {
					i = 0;
					for (; i < saves.size(); i++) 
						if (saves.get(i).priority() >= p) {
					    saves.add(i, s);
					    break;
						}
					if (i >= saves.size())
						saves.add(s);
				}
			}
		}
		for (i = 0; i < saves.size(); i++)
			try {
				s = saves.get(i);
				//System.out.println("=======SAVE===== " + ((IDependentParam)s).getGLName());
				s.save(cntx);
			} catch (SysException ex) {
				ex.printStackTrace();
			}
	}

	/**
	 * TODO Надо доработать сохранение параметров в режиме многопоточности
	 * 
	 * @param tm
	 * @param pool
	 */
	public void saveParams(Saved.TIME tm, PoolThread pool) {
		ATaskInfo info = getTaskInfo();
		ArrayList<WValue> list = new ArrayList<WValue>();
		for (Iterator<String> e = info.prms.keySet().iterator(); e.hasNext();)
			try {
				String k = e.next();
				Object o = info.prms.get(k);
				if (o != null && o instanceof Saved) {
					Saved s = (Saved) o;
					Saved.TIME t = s.timeSaved();
					if (t != null && t.equals(tm)) {
						Process prc = pool.occupy();
						if (prc != null) {
							Method m;
							try {
								m = s.getClass().getMethod("save", Context.class);
								list.add(prc.startMethod(m, s, cntx));
							} catch (SecurityException e1) {
								e1.printStackTrace();
							} catch (NoSuchMethodException e1) {
								e1.printStackTrace();
							} finally {
								pool.free(prc);
							}
						} else {
							s.save(cntx);
						}
					}
				}
			} catch (SysException ex) {
				ex.printStackTrace();
			}
		for (WValue v : list) {
			try {
				v.getValueWait();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	private AppInfo ai = null;

	public final void processing(Object p) throws Throwable {
		if (ai == null)
			ai = ApplicationManager.getCurrentAppInfo();
		Process.currentProcess().getTempInterface()
				.setTmpDir(ai.getTmpDir(), ai.isTmpDeleteOnExit());
		ITaskAPI ta = cntx.ta;
		long id_task = cntx.id_task;
		try {
			if (ta.setProcessing(id_processor, id_task) == id_task) {
				ta.log(id_processor, id_task, "Processing...");
				try {
					cntx.info.calculate(
							TimeState.getInstance(TimeState.Time.before, State.PROCESSING),
							cntx);
					saveParams(Saved.TIME.before);
				} catch (Throwable e) {
					e.printStackTrace();
				}
				State st;
				try {
					st = process(cntx);
					cntx.info.calculate(
							TimeState.getInstance(TimeState.Time.after, State.PROCESSING),
							cntx);
				} catch (Throwable e) {
					e.printStackTrace();
					throw e;
				}
				ta.log(id_processor, id_task,
						"The task was completed with " + st.name() + " status");
				saveParams(Saved.TIME.after);
				ta.setState(id_processor, id_task, st, null);
			} else {
				ta.setTaskError(cntx.id_task, new SysException(
						"It is impossible processing"));
				saveParams(Saved.TIME.after);
				ta.setCanceled(id_processor, id_task);
			}
		} catch (RuntimeException ex) {
			ex.printStackTrace();
			try {
				State st = State.DONE_ERR;
				ta.log(id_processor, id_task, ex.toString());
				ta.log(id_processor, id_task,
						"The task was completed with " + st.name() + " status");
				ta.setTaskError(id_task, ex);
				saveParams(Saved.TIME.after);
				ta.setDoneErr(id_processor, id_task);
			} catch (SysException ex1) {
				try {
					ta.log(id_processor, id_task, ex1.toString());
				} catch (SysException ex3) {
					System.out.println(ex1);
				}
				try {
					ta.setTaskError(cntx.id_task, ex1);
				} catch (SysException ex3) {
					System.out.println(ex1);
				}
			}
		} catch (SysException ex) {
			ex.printStackTrace();
			try {
				State st = State.DONE_ERR;
				ta.log(id_processor, id_task, ex.toString());
				ta.log(id_processor, id_task,
						"The task was completed with " + st.name() + " status");
				ta.setTaskError(id_task, ex);
				saveParams(Saved.TIME.after);
				ta.setDoneErr(id_processor, id_task);
			} catch (SysException ex1) {
				try {
					ta.log(id_processor, id_task, ex1.toString());
				} catch (SysException ex3) {
					System.out.println(ex1);
				}
			}
		} catch (TaskException ex) {
			State st = ex.getState();
			try {
				ta.log(id_processor, id_task, ex.toString());
				ta.log(id_processor, id_task,
						"The task was completed with " + st.name() + " status");
				ta.setTaskError(id_task, ex);
			} catch (SysException ex1) {
				try {
					ta.log(id_processor, id_task, ex1.toString());
				} catch (SysException ex3) {
					System.out.println(ex1);
				}
				try {
					ta.setTaskError(id_task, ex1);
				} catch (SysException ex3) {
					System.out.println(ex1);
				}
			}
			try {
				saveParams(Saved.TIME.after);
				ta.setState(id_processor, id_task, st, null);
			} catch (SysException e) {
				e.printStackTrace(); // To change body of catch statement use File |
				// Settings | File Templates.
				try {
					ta.setTaskError(id_task, e);
				} catch (SysException ex3) {
					System.out.println(ex3);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace(); // !!!
		}

		try {
			State st = null;
			try {
				st = ta.getState(id_task);
				sendLog(st);
			} catch (SysException ex) {
				try {
					ta.log(id_processor, id_task, ex.toString());
				} catch (SysException ex2) {
					System.out.println(ex);
				}
			}
			/*
			 * try { runTasksFromDNF(id_task, TaskInfo.Cntxt.Time.AFTER); } catch
			 * (SysException ex) { try { ta.log(id_processor, id_task, ex.toString());
			 * } catch (SysException ex2) { System.out.println(ex); } } //
			 */
		} finally {
			id_processor = 0;
			isRun = false;
		}
	}
}
