package center.task.api.rmi;

import java.io.File;
import java.io.Reader;
import java.rmi.*;
import java.util.Map;
import java.util.Set;

import center.task.State;
import center.task.api.TaskCI;

public interface InterfaceTaskAPI extends Remote {

	public InterfaceTaskAPI getInterfaceInstance(String alias) throws RemoteException;

	public void registerClasses(long subject_id, Set<String> hs)
			throws RemoteException;

	public String getAlias() throws RemoteException;

	public long getSubject(long old_subject_id) throws RemoteException;

	public long setParamObject(long task_id, String name, Object value)
			throws RemoteException;

	public long setParamObject(long task_id, String name, Object value,
			long wait, long calc, long scall) throws RemoteException;

	public long createTask(long subject_id, String cls) throws RemoteException;

	public long createTask(long subject_id, String cls, long parent_task_id)
			throws RemoteException;

	public String getClassName(long id) throws RemoteException;
  /*
	public boolean checkDNF(long id, DNF dnf, Object[] flds)
			throws RemoteException;
  //*/
	public long setTaskDT(long id, String dt, String duration,
			String block_duration, Double arch_day) throws RemoteException;

	public long setTaskError(long id, Throwable e) throws RemoteException;

	public String getParamTask(long id, String name) throws RemoteException;

	public Map<String, Object> getGroupParams(long id, String group)
			throws RemoteException;

	//public Reader getTaskParamLong(long id, String name) throws RemoteException;

	public State getState(long id) throws RemoteException;

	public long setState(long subject_id, long id, State st)
			throws RemoteException;

	public long setState(long subject_id, long id, State st, Long processor_id)
			throws RemoteException;

	public long setReady(long subject_id, long id) throws RemoteException;

	public long setReady(long subject_id, long id, Long processor_id)
			throws RemoteException;

	public long setBlocked(long subject_id, long id) throws RemoteException;

	public TaskCI getBlocked(long subject_id, long wait)
			throws RemoteException;

	public long setProcessing(long subject_id, long id) throws RemoteException;

	public long setTimeout(long subject_id, long id) throws RemoteException;

	public long setScheduled(long subject_id, long id) throws RemoteException;

	public long setBreaking(long subject_id, long id) throws RemoteException;

	public long setBroken(long subject_id, long id) throws RemoteException;

	public long setCanceled(long subject_id, long id) throws RemoteException;

	public long setDoneErr(long subject_id, long id) throws RemoteException;

	public long setDoneOk(long subject_id, long id) throws RemoteException;

	public State waitFinished(long subject_id, long id, long msec)
			throws RemoteException;

	public long log(long subject_id, long id, String info, boolean base)
			throws RemoteException;

	public long log(long subject_id, long id, String info) throws RemoteException;

	public void writeLogToFile(long id, File file, String charset)
			throws RemoteException;

	public Object getParam(long id, String fullname) throws RemoteException;

}
