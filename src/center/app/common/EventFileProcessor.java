package center.app.common;

import java.io.File;
import java.util.Set;

import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.util.Strings;
import center.task.AProcessor;
import center.task.Context;
import center.task.NewTaskInfo;
import center.task.State;
import center.task.TaskException;
/**
 * Процессор обработки файлов
 * @author Vassaev A.V.
 * @version 1.1
 */
public class EventFileProcessor extends AProcessor {
	private int sleep;
	private Object sync = new Object();
	
	private File occupyFile(File own, ru.vassaev.core.thread.Process prc
			, Context cntx, File from_dir, boolean subdirectories) throws SysException {
    if (!from_dir.exists()) {
    	boolean b = from_dir.mkdirs();
    	if (!b)
    		throw new SysException("Impossible creating directory with path '" + from_dir.getAbsolutePath() + "'");
    }
    if (!from_dir.isDirectory())
  		throw new SysException(from_dir.getAbsolutePath() + " is a file");
    File[] fls = from_dir.listFiles();
    for (int i = 0; i < fls.length; i++) {
      File fl = fls[i];
      if (subdirectories && fl.isDirectory()) {
      	File x = occupyFile(own, prc, cntx, fl, subdirectories);
      	if (x == null)
      		continue;
      	else
      		return x;
      }
      if (fl.isFile()) {
      	cntx.log(false, "found file '", fl.getAbsolutePath(), "'");
        String lm = Long.toString(fl.lastModified());
        prc.regResourceName(fl.getName(), "file.NAME");
        prc.regResourceName(lm, "file.LM");
        prc.regResourceName(fl.getParent(), "file.PATH");
        prc.regResourceName(fl.getAbsolutePath(), "file.FULLNAME");
        File f = fl.getParentFile();
        StringBuffer s = new StringBuffer();
        while (!own.equals(f)) {
        	s.insert(0, f.getName()).insert(0, File.separatorChar);
        	f = f.getParentFile();
        }
        regResourceName(s, "file.RPATH");
        String prc_dir = cntx.getPrmString("path_prc");
        if (prc_dir == null || prc_dir.length() == 0) {
          String skip_dir = cntx.getPrmString("path_skip");
          if (skip_dir == null || skip_dir.length() == 0) {
          	fl.delete();
          	cntx.log(false, fl.getAbsolutePath(), " was deleted");
            continue;
          }
          File skip_d = new File(skip_dir.toString());
          if (!skip_d.exists()) {
          	boolean b = skip_d.mkdirs();
          	if (!b)
          		throw new SysException("Impossible creating directory with path '" + skip_d.getAbsolutePath() + "'");
          }
          StringBuffer skip_file = new StringBuffer().append(skip_dir)
          		.append(File.separatorChar).append(fl.getName()).append('.').append(System.nanoTime());
        	cntx.log(false, fl.getAbsolutePath(), " was skipped and removed to ", skip_file);
        } else {
          File prc_d = new File(prc_dir.toString());
          if (!prc_d.exists()) {
          	boolean b = prc_d.mkdirs();
          	if (!b)
          		throw new SysException("Impossible creating directory with path '" + prc_d.getAbsolutePath() + "'");
          }
          StringBuffer prc_file = new StringBuffer().append(prc_dir)
          		.append(File.separatorChar).append(fl.getName()).append('.').append(System.nanoTime());
          File prc_fl = new File(prc_file.toString());
          if (fl.renameTo(prc_fl)) 
            if (fl.exists()) {
              prc_fl.delete();
              continue;
            } else { 
            	cntx.log(false, fl.getAbsolutePath(), " was renamed to ", prc_fl.getAbsolutePath());
              prc.regResourceName(prc_fl.getName(), "file_prc.NAME");
              prc.regResourceName(prc_fl.getParent(), "file_prc.PATH");
              prc.regResourceName(prc_fl.getAbsolutePath(), "file_prc.FULLNAME");
            	return prc_fl;
            }
        }
      }
    }
    return null;
	}

	public State process(Context cntx) throws TaskException, SysException, InterruptedException {
    String slp = cntx.getPrmNvl("sleep", "1000");
    try {
      this.sleep = Integer.parseInt(slp);
    } catch(NumberFormatException ex) {
    	this.sleep = 1000;
    }
    ru.vassaev.core.thread.Process prc = ru.vassaev.core.thread.Process.currentProcess();
    while (true) {
      try {
        if (prc.isWillBreak())
          return State.BROKEN;
        String path = cntx.getPrmString("path_from");
        boolean subdirectories = Strings.parseBooleanNvl(cntx.getPrmString("subdirectories"), true);
        File dir = new File(path);
        File d = occupyFile(dir, prc, cntx, dir, subdirectories);
        if (d != null) {
          NewTaskInfo nti = getChild();
          Context cntx_child = nti.createAndReadyTask(cntx.ta, cntx, cntx.id_subject, null, null);
          if ((cntx_child == null) || (cntx_child.id_task <= 0)) {
          	{
            	String rn = cntx.getPrmString("path_skip");
              if (rn == null || rn.length() == 0) {
              	cntx.log(false, d.getAbsolutePath(), " wasn't renamed");
              	continue;
              }
              File rn_d = new File(rn.toString());
              if (!rn_d.exists()) {
              	boolean b = rn_d.mkdirs();
              	if (!b)
              		throw new SysException("Impossible creating directory with path '" + rn_d.getAbsolutePath() + "'");
              }
              StringBuffer skip_file = new StringBuffer().append(rn)
              		.append(File.separatorChar).append(d.getName());
            	File r = new File(skip_file.toString());
            	d.renameTo(r);
            	cntx.log(false, d.getAbsolutePath(), " was renamed to ", r.getAbsolutePath());
          	}
          	wait0();
          	continue;
          }

          long wait = Strings.parseIntegerNvl(cntx.getPrmByFullName("wait"), 60000);//!!!
          cntx.log(false, "Start waiting (" + wait + ")");
          State st = cntx_child.ta.waitFinished(cntx_child.id_subject, cntx_child.id_task, wait);
          cntx.log(false, "End waiting");
          if (State.DONE_ERR.equals(st)) {
          	String rn = cntx.getPrmString("path_err");
            if (rn == null || rn.length() == 0) {
            	cntx.log(false, d.getAbsolutePath(), " wasn't renamed");
            	continue;
            }
            File rn_d = new File(rn.toString());
            if (!rn_d.exists()) {
            	boolean b = rn_d.mkdirs();
            	if (!b)
            		throw new SysException("Impossible creating directory with path '" + rn_d.getAbsolutePath() + "'");
            }
            StringBuffer err_file = new StringBuffer().append(rn)
            		.append(File.separatorChar).append(d.getName());
          	File r = new File(err_file.toString());
          	d.renameTo(r);
          	cntx.log(false, d.getAbsolutePath(), " was renamed to ", r.getAbsolutePath());
          } else if (State.DONE_OK.equals(st)) {
          	String rn = cntx.getPrmString("path_ok");
            if (rn == null || rn.length() == 0) {
            	cntx.log(false, d.getAbsolutePath() + " wasn't renamed");
            	continue;
            }
            File rn_d = new File(rn.toString());
            if (!rn_d.exists()) {
            	boolean b = rn_d.mkdirs();
            	if (!b)
            		throw new SysException("Impossible creating directory with path '" + rn_d.getAbsolutePath() + "'");
            }
            StringBuffer ok_file = new StringBuffer().append(rn)
            		.append(File.separatorChar).append(d.getName());
          	File r = new File(ok_file.toString());
          	d.renameTo(r);
          	cntx.log(false, d.getAbsolutePath() + " was renamed to " + r.getAbsolutePath());
          }
        }
      } catch (NullPointerException ex) {
        ex.printStackTrace();
      } catch (SysException ex) {
        ex.printStackTrace();
      }
      wait0();
    }
	}
	private void wait0() {
    synchronized (sync) {
      try {
        sync.wait(sleep);
      } catch (InterruptedException e) {
        e.printStackTrace();  //xTo change body of catch statement use File | Settings | File Templates.
      }
    }
	}
	@Override
	public Set<String> dependentOn() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<String> loopDependentOn() {
		// TODO Auto-generated method stub
		return null;
	}

}
