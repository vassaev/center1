package center.task.prm.alg;

import java.io.File;

import org.w3c.dom.Element;

import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.types.TimeList;
import ru.vassaev.core.types.TimeList.Type;
import ru.vassaev.core.util.Strings;
import center.task.CalculateException;
import center.task.Context;

public final class FileDel extends Merge {

	public FileDel(center.task.prm.Type tp, String owner, Element e) throws SysException {
    super(tp, owner, e);
	}

	protected Object calculate(TimeList tl, Context cntx) throws CalculateException {
		try {
	  	tl.addPointTime(Type.START);
	  	String filename = Strings.getString(super.calculate(tl, cntx));
	  	if (filename == null)
        return null;
	  	File f = new File(filename);
	  	return f.delete();
		} finally {
	  	tl.addPointTime(Type.END);
		}
	}

}
