package center.task;

import ru.vassaev.core.Pool;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.thread.Processed;

import java.util.Set;

import center.task.api.ITaskAPI;

public interface IProcessor extends Processed {
  public IProcessor newInstance();

  public long getProcessorID();

  public long getTaskID();

  public ITaskAPI getTaskAPI();

  public ATaskInfo getTaskInfo();

  public Context getContext();

  public void setPoolProcess(Pool<ru.vassaev.core.thread.Process> pool);

  public void start(ru.vassaev.core.thread.Process prc
          , long id_processor
          , long id_task
          , ITaskAPI ta
          , TaskInfo ti) throws SysException;

  /**
   * Основной процесс работы алгоритма
   *
   * @param cntx - контекст задачи
   * @throws TaskException        - задача завершена с ошибкой
   * @throws SysException         - общая ошибка
   * @throws InterruptedException - прерывание обработки
   */
  public State process(Context cntx) throws TaskException
          , SysException, InterruptedException;

  public boolean isBusy();
  public Set<String> dependentOn();
  public Set<String> loopDependentOn();

}
