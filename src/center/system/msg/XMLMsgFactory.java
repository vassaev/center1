package center.system.msg;

import ru.vassaev.core.Factory;
import ru.vassaev.core.xml.XMLMsg;

import org.w3c.dom.Element;

import java.util.Map;

/**
 * Фабрика сообщений
 */
public class XMLMsgFactory implements Factory<XMLMsg> {
  public void initFactory(Element el) {
  }

  public void initFactory(Map<String, Object> prms) {
  }

  public XMLMsg create() {
    return new XMLMsg();
  }

  public void initialization(XMLMsg inst) {
  }

  public boolean reset(XMLMsg inst) {
    inst.reset();
    return true;
  }

  public boolean canUseImmediately(XMLMsg inst) {
    return true;
  }

  public boolean isValid(XMLMsg inst) {
    return true;
  }

  public void destroy(XMLMsg inst) {
  }

  public Class<XMLMsg> getObjectClass() {
    return XMLMsg.class;
  }

  public String[] getPropertyNames() {
    return new String[0];
  }

  public Object getProperty(String name) {
    return null;
  }
}
