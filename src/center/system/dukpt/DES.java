package center.system.dukpt;

public class DES {
	public static final int DES_ENCRYPT = 1;
	public static final int DES_DECRYPT = 2;
	public static final int TDES_ENCRYPT = 3;
	public static final int TDES_DECRYPT = 4;

	private static byte puissance(byte puissance) {
		byte res = 1;
		for (int i = 1; i <= puissance; i++)
			res <<= 1;
		return res;
	}

	private static byte[] aip = { 58, 50, 42, 34, 26, 18, 10, 2, 60, 52, 44, 36,
			28, 20, 12, 4, 62, 54, 46, 38, 30, 22, 14, 6, 64, 56, 48, 40, 32, 24, 16,
			8, 57, 49, 41, 33, 25, 17, 9, 1, 59, 51, 43, 35, 27, 19, 11, 3, 61, 53,
			45, 37, 29, 21, 13, 5, 63, 55, 47, 39, 31, 23, 15, 7 };

	private static byte[] aipinv = { 40, 8, 48, 16, 56, 24, 64, 32, 39, 7, 47,
			15, 55, 23, 63, 31, 38, 6, 46, 14, 54, 22, 62, 30, 37, 5, 45, 13, 53, 21,
			61, 29, 36, 4, 44, 12, 52, 20, 60, 28, 35, 3, 43, 11, 51, 19, 59, 27, 34,
			2, 42, 10, 50, 18, 58, 26, 33, 1, 41, 9, 49, 17, 57, 25 };

	private static byte[] E = { 32, 1, 2, 3, 4, 5, 4, 5, 6, 7, 8, 9, 8, 9, 10,
			11, 12, 13, 12, 13, 14, 15, 16, 17, 16, 17, 18, 19, 20, 21, 20, 21, 22,
			23, 24, 25, 24, 25, 26, 27, 28, 29, 28, 29, 30, 31, 32, 1 };

	private static byte[] S1 = { 14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9,
			0, 7, 0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8, 4, 1, 14, 8,
			13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0, 15, 12, 8, 2, 4, 9, 1, 7, 5, 11,
			3, 14, 10, 0, 6, 13 };

	private static byte[] S2 = { 15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0,
			5, 10, 3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5, 0, 14, 7,
			11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15, 13, 8, 10, 1, 3, 15, 4, 2,
			11, 6, 7, 12, 0, 5, 14, 9 };

	private static byte[] S3 = { 10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4,
			2, 8, 13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1, 13, 6, 4, 9,
			8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7, 1, 10, 13, 0, 6, 9, 8, 7, 4, 15,
			14, 3, 11, 5, 2, 12 };

	private static byte[] S4 = { 7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12,
			4, 15, 13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9, 10, 6, 9, 0,
			12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4, 3, 15, 0, 6, 10, 1, 13, 8, 9, 4,
			5, 11, 12, 7, 2, 14 };

	private static byte[] S5 = { 2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0,
			14, 9, 14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6, 4, 2, 1, 11,
			10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14, 11, 8, 12, 7, 1, 14, 2, 13, 6,
			15, 0, 9, 10, 4, 5, 3 };

	private static byte[] S6 = { 12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7,
			5, 11, 10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8, 9, 14, 15,
			5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6, 4, 3, 2, 12, 9, 5, 15, 10, 11,
			14, 1, 7, 6, 0, 8, 13 };

	private static byte[] S7 = { 4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10,
			6, 1, 13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6, 1, 4, 11, 13,
			12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2, 6, 11, 13, 8, 1, 4, 10, 7, 9, 5,
			0, 15, 14, 2, 3, 12 };

	private static byte[] S8 = { 13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0,
			12, 7, 1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2, 7, 11, 4, 1,
			9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8, 2, 1, 14, 7, 4, 10, 8, 13, 15,
			12, 9, 0, 3, 5, 6, 11 };

	private static byte[] ap = { 16, 7, 20, 21, 29, 12, 28, 17, 1, 15, 23, 26, 5,
			18, 31, 10, 2, 8, 24, 14, 32, 27, 3, 9, 19, 13, 30, 6, 22, 11, 4, 25 };

	private static byte[] PC1 = { 57, 49, 41, 33, 25, 17, 9, 1, 58, 50, 42, 34,
			26, 18, 10, 2, 59, 51, 43, 35, 27, 19, 11, 3, 60, 52, 44, 36, 63, 55, 47,
			39, 31, 23, 15, 7, 62, 54, 46, 38, 30, 22, 14, 6, 61, 53, 45, 37, 29, 21,
			13, 5, 28, 20, 12, 4 };

	private static byte[] PC2 = { 14, 17, 11, 24, 1, 5, 3, 28, 15, 6, 21, 10, 23,
			19, 12, 4, 26, 8, 16, 7, 27, 20, 13, 2, 41, 52, 31, 37, 47, 55, 30, 40,
			51, 45, 33, 48, 44, 49, 39, 56, 34, 53, 46, 42, 50, 36, 29, 32 };

	private static byte[] LS = { 0, 1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2,
			1 };

	private static byte[][] Ki = new byte[17][49];

	public static void Init_bit_tab(byte[] dest, int pos, byte[] source,
			int pos2, int n) {

		for (int i = 0; i < n; i++) {
			int masque = 0x80;
			for (int j = 0; j < 8; j++) {
				int s = source[pos2 + i] & 0xff;
				dest[pos + 8 * i + j] = (byte) ((s & masque) >> (7 - j));
				masque >>= 1;
			}
		}
	}

	public static void memset(byte[] vect, int pos, byte value, int l) {
		for (int i = 0; i < l; i++) {
			vect[i] = value;
		}
	}

	/****************************************************************************
	 * Bin_to_Hex() : range la valeur hexa sur 8 octets d'un nombre binaire de 64
	 * bits
	 *****************************************************************************/
	public static void Bin_to_Hex(byte[] vect, int pos1, byte[] source, int pos2) {
		memset(vect, pos1, (byte) 0, 8);
		for (int i = 0; i < 8; i++) {
			byte masque = 7;
			for (int j = 0; j < 8; j++) {
				int tmp = vect[pos1 + i] & 0xff;
				tmp += (puissance(masque)) * (source[pos2 + i * 8 + j] & 0xff);
				vect[pos1 + i] = (byte) tmp;
				--masque;
			}
		}
	}

	public static void Vect_Permutation(byte[] vect, int pos, int n_vect,
			byte[] regle, int n_regle) {
		byte[] buff = new byte[65];

		memset(buff, 0, (byte) 0, 65);
		System.arraycopy(vect, pos, buff, 0, n_vect);

		for (int i = 0; i < n_regle; i++)
			vect[pos + i] = buff[regle[i] - 1];

	}

	public static void S_Box_Calc(byte[] vect, int pos) {
		byte[][] S_Box = new byte[][] { S1, S2, S3, S4, S5, S6, S7, S8 };
		int lig, col;

		for (int i = 0; i < 8; i++) {
			col = 8 * (vect[pos + 1 + 6 * i] & 0xff) + 4
					* (vect[pos + 2 + 6 * i] & 0xff) + 2 * (vect[pos + 3 + 6 * i] & 0xff)
					+ (vect[pos + 4 + 6 * i] & 0xff);
			lig = 2 * (vect[pos + 6 * i] & 0xff) + (vect[pos + 5 + 6 * i] & 0xff);
			Init_4bit_tab(vect, pos + 4 * i, S_Box[i][col + lig * 16]);
		}
	}

	public static void Init_4bit_tab(byte[] dest, int pos, byte source) {
		int masque = 0x08;
		int s = source & 0xff;
		for (int i = 0; i < 4; i++) {
			dest[pos + i] = (byte) ((s & masque) >> (3 - i));
			masque >>= 1;
		}
	}

	public static void Xor(byte[] vect1, int pos1, byte[] vect2, int pos2,
			int num_byte) {
		for (int i = 0; i < num_byte; i++)
			vect1[pos1 + i] ^= vect2[pos2 + i];
	}

	public static void Left_shifts(byte[] vect, int pos, int n) {
		byte tmp_vect28, tmp_vect0;

		for (int i = 0; i < n; i++) {
			tmp_vect0 = vect[pos];
			// memcpy(vect,&vect[1],27);
			for (int j = 0; j < 27; j++)
				vect[pos + j] = vect[pos + j + 1];
			vect[pos + 27] = tmp_vect0;

			tmp_vect28 = vect[pos + 28];
			// memcpy(&vect[28],&vect[29],27);
			for (int j = 28; j < 55; j++)
				vect[pos + j] = vect[pos + j + 1];
			vect[pos + 55] = tmp_vect28;
		}
	}

	public static void Calcul_sous_cles(byte[] DESKEY, int pos) {
		byte i;
		byte[] Kb = new byte[65];
		byte[] inter_key = new byte[57];

		Init_bit_tab(Kb, 1, DESKEY, pos, 8);
		Vect_Permutation(Kb, 1, 64, PC1, 56);

		for (i = 1; i <= 16; i++) {
			Left_shifts(Kb, 1, LS[i]);
			// memcpy(&inter_key[1],&Kb[1],56);
			System.arraycopy(Kb, 1, inter_key, 1, 56);
			Vect_Permutation(inter_key, 1, 56, PC2, 48);
			// memcpy(&Ki[i][1],&inter_key[1],48);
			System.arraycopy(inter_key, 1, Ki[i], 1, 48);
		}
	}

	public static void _Des(int cryp_decrypt, byte[] DES_DATA, int pos1,
			byte[] DES_RESULT, int pos2, byte[] DESKEY, int pos3) {

		byte[] right32_bit = new byte[32];
		byte[] Data_B = new byte[81];

		Init_bit_tab(Data_B, 1, DES_DATA, pos1, 8);
		Vect_Permutation(Data_B, 1, 64, aip, 64);

		Calcul_sous_cles(DESKEY, pos3);

		/******************* boucle principale de 15 iterations */
		for (int i = 1; i <= 15; i++) {

			// memcpy(right32_bit,&Data_B[33],32);
			System.arraycopy(Data_B, 33, right32_bit, 0, 32);
			Vect_Permutation(Data_B, 33, 32, E, 48);

			switch (cryp_decrypt) {
			case DES_ENCRYPT:
				Xor(Data_B, 33, Ki[i], 1, 48);
				break;

			case DES_DECRYPT:
				Xor(Data_B, 33, Ki[17 - i], 1, 48);
				break;
			}

			S_Box_Calc(Data_B, 33);
			Vect_Permutation(Data_B, 33, 32, ap, 32);
			Xor(Data_B, 33, Data_B, 1, 32);
			// memcpy(&Data_B[1],right32_bit,32);
			System.arraycopy(right32_bit, 0, Data_B, 1, 32);

		}

		/******************************** 16 iteration *****/

		// memcpy(right32_bit,&Data_B[33],32);
		System.arraycopy(Data_B, 33, right32_bit, 0, 32);
		Vect_Permutation(Data_B, 33, 32, E, 48);

		if (cryp_decrypt == DES_ENCRYPT)
			Xor(Data_B, 33, Ki[16], 1, 48);
		else
			Xor(Data_B, 33, Ki[1], 1, 48);

		S_Box_Calc(Data_B, 33);
		Vect_Permutation(Data_B, 33, 32, ap, 32);
		Xor(Data_B, 1, Data_B, 33, 32);

		// memcpy(&Data_B[33],right32_bit,32);
		System.arraycopy(right32_bit, 0, Data_B, 33, 32);

		Vect_Permutation(Data_B, 1, 64, aipinv, 64);

		Bin_to_Hex(DES_RESULT, pos2 + 0, Data_B, 1);

	}

	public static void MAC(byte msg[], int length, byte key[], byte result[]) {
		int block;

		block = 0;
		memset(result, 0, (byte) 0, 8);

		while (length > block) {
			if ((length - block) <= 8) {
				Xor(result, 0, msg, block, length - block);
				_Des(DES_ENCRYPT, result, 0, key, 0, result, 0);
				return;
			}
			Xor(result, 0, msg, block, 8);
			_Des(DES_ENCRYPT, result, 0, key, 0, result, 0);
			block += 8;
		}

	}

	public static void Des_string(byte[] in_data, int data_length, byte[] key,
			int key_lenth, byte[] out_data, int DES_MODE) {
		int data_block;
		byte[] tmp = new byte[8];
		data_block = data_length / 8;
		for (int i = 0; i < data_block; i++) {
			if (DES_MODE == DES_ENCRYPT) {
				_Des(DES_ENCRYPT, in_data, i * 8, out_data, i * 8, key, 0);
			} else if (DES_MODE == DES_DECRYPT) {
				_Des(DES_DECRYPT, in_data, i * 8, out_data, i * 8, key, 0);
			} else if (DES_MODE == TDES_ENCRYPT) {
				_Des(DES_ENCRYPT, in_data, i * 8, tmp, 0, key, 0);
				_Des(DES_DECRYPT, tmp, 0, tmp, 0, key, 8);
				if (key_lenth == 24) {
					_Des(DES_ENCRYPT, tmp, 0, out_data, i * 8, key, 16);
				} else {
					_Des(DES_ENCRYPT, tmp, 0, out_data, i * 8, key, 0);
				}

			} else if (DES_MODE == TDES_DECRYPT) {
				if (key_lenth == 24) {
					_Des(DES_DECRYPT, in_data, i * 8, tmp, 0, key, 16);
				} else {
					_Des(DES_DECRYPT, in_data, i * 8, tmp, 0, key, 0);
				}
				_Des(DES_ENCRYPT, tmp, 0, tmp, 0, key, 8);
				_Des(DES_DECRYPT, tmp, 0, out_data, i * 8, key, 0);
			}

		}
	}
	/*
		String cd = "000000000000000061AD7A396B4A891F60DD7C70E7AD0C53FDD2E4F8A4827465E148C1BAA1FE59377B351AC9C11B9A3AA4B406F594C4B150CCBF37413E576F70C68E938EC775AF56C392C8AB2E335778";
		byte[] key = new byte[]{	
				(byte)0xff, (byte)0xff, (byte)0xff, (byte)0xff, (byte)0xff, 
				(byte)0xff, (byte)0xff, (byte)0xff, (byte)0xff, (byte)0xff, 
				(byte)0xff, (byte)0xff, (byte)0xff, (byte)0xff, (byte)0xff, 
				(byte)0xff};
	  byte[] cdb = DUKPT.fromHexString(cd);
	  byte[] out_data = new byte[cdb.length];
	  Des_string(cdb, cdb.length, key,
				key.length, out_data, DES.TDES_DECRYPT);
	  System.out.println(new String(out_data, "utf-8"));
	  ///////////
	  String dc = "<ReaderID>1375c9</ReaderID>" +  
          "<NewKey>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE</NewKey>" +      
          "<UniqueID>1375c9</UniqueID>" +      
          "<Session>239E379D34704E9796E3D8731F1344BF_2821</Session>";
	  //String dc = "<ReaderID>1375c9</ReaderID><Session>239E379D34704E9796E3D8731F1344BF_2821</Session>";
	  //String dc = "<OpLogin>vassaev</OpLogin><OpPwd>vassaev</OpPwd>";
	  byte[] dcb = dc.getBytes("utf-8");
	  int l = (dcb.length / 8) * 8;
	  if (l != dcb.length)
	    l += 8;
	  l += 8;
	  dc = Strings.lpad(dc, l, (char)0);
	  dcb = dc.getBytes("utf-8");
	  out_data = new byte[dcb.length];
	  Des_string(dcb, dcb.length, key,
				key.length, out_data, DES.TDES_ENCRYPT);
	  System.out.println(DUKPT.toHexString(out_data));
	  
	  /////////
	  
	  java.util.Enumeration networkInterfaces = java.net.NetworkInterface.getNetworkInterfaces();
    while (networkInterfaces.hasMoreElements()) {
        java.net.NetworkInterface networkInterface = (java.net.NetworkInterface) networkInterfaces.nextElement();
        System.out.println("networkInterface.displayName=" + networkInterface.getDisplayName());
        System.out.println("networkInterface.name=" + networkInterface.getName());
        java.util.Enumeration inetAddresses = networkInterface.getInetAddresses();
        while (inetAddresses.hasMoreElements()) {
        	java.net.InetAddress inetAddress = (java.net.InetAddress) inetAddresses.nextElement();
          System.out.println("networkInterface.[" + networkInterface.getName() + "].inetAddress=" + inetAddress);
        }
        System.out.println("-----------------------------------");
    }
	 */
	/*
	public static void main(String[] args) throws java.io.UnsupportedEncodingException, java.net.SocketException {
    String in[] = new String[]{"5D64135BFB022CEF43524308F1D54089", "<ReaderID>1375c9</ReaderID>" +  
        "<NewKey>FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE</NewKey>" +      
        "<UniqueID>1375c9</UniqueID>" +      
        "<Session>239E379D34704E9796E3D8731F1344BF_2821</Session>"};
	  byte[] key = center.system.dukpt.DUKPT.parseHexStr2Byte(in[0].toString());
	  java.lang.String dc = (java.lang.String)in[1];
	  byte[] dcb = dc.getBytes("utf-8");
	  int l = (dcb.length / 8) * 8;
	  if (l != dcb.length)
	    l += 8;
	  l += 8;
	  dc = ru.vassaev.core.util.Strings.lpad(dc, l, (char)0);
	  System.out.println("LEN = " + l);
	  System.out.println("DC = " + dc);
	  dcb = dc.getBytes("utf-8");
	  byte[] out_data = new byte[dcb.length];
	  center.system.dukpt.DES.Des_string(dcb, dcb.length, key,
				key.length, out_data, center.system.dukpt.DES.TDES_ENCRYPT);
	  
	  String out = center.system.dukpt.DUKPT.toHexString(out_data);
	  System.out.println(out);
	  
    java.lang.String cd = out;
    byte[] cdb = center.system.dukpt.DUKPT.parseHexStr2Byte(cd);
    out_data = new byte[cd.length()/2];
    center.system.dukpt.DES.Des_string(cdb, cdb.length, key,
	   key.length, out_data, center.system.dukpt.DES.TDES_DECRYPT);
    int s = 0;
    while(s < out_data.length && out_data[s] == 0) s++;
    java.lang.String r = new java.lang.String(out_data, s, out_data.length - s, "utf-8");
    if (r.length() > 0 && r.charAt(0) == '<')
      out = "<root>" + r + "</root>";
    else
      out = null;
	  System.out.println(out);
	}
	*/
/*
5D64 135B FB02 2CEF 4352 4308 F1D5 4089   
CHECK 
1779 88     

TPK 
5B997446B446C73133F4D70ABE13ECBD 
CHECK 
31AF6A 

TAK 
CCF59A053DE9510419EE380CE4229F39 */
	
}