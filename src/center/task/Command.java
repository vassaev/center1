package center.task;

import ru.vassaev.core.exception.SysException;

import java.util.HashMap;

import center.task.api.ITaskAPI;

/**
 * Команды для перехода по статусам
 */
public enum Command {
    CREATE {protected void init() {
        directions.put(null,State.CREATED);
    }},
    COMPLETE {protected void init() {
        directions.put(State.CREATED,State.READY);
    }},
    ABORT {protected void init() {
        directions.put(State.BLOCKED,State.BREAKING);
        directions.put(State.SCHEDULED,State.BREAKING);
        directions.put(State.BREAKING,State.BROKEN);
        directions.put(State.CREATED,State.CANCELED);
        directions.put(State.READY,State.CANCELED);
    }},
    KEEP {protected void init() {
        directions.put(State.READY,State.BLOCKED);
    }},
    RETRACT {protected void init() {
        directions.put(State.BLOCKED, State.READY);
    }},
    APPOINT {protected void init() {
        directions.put(State.BLOCKED, State.SCHEDULED);
    }},
    START {protected void init() {
        directions.put(State.BLOCKED, State.PROCESSING);
        directions.put(State.SCHEDULED, State.PROCESSING);
    }},
    FINISH {protected void init() {
        directions.put(State.PROCESSING, State.DONE_OK);
        directions.put(State.BREAKING, State.DONE_OK);
    }},
    FINISH_WITH_ERR {protected void init() {
        directions.put(State.PROCESSING, State.DONE_ERR);
        directions.put(State.BREAKING, State.DONE_ERR);
        directions.put(State.BLOCKED, State.DONE_ERR);
    }};
    protected HashMap<State, State> directions = new HashMap<State, State>();
    protected abstract void init();
    public void exec(ITaskAPI api, long subject_id, long id) throws SysException {
        State st = api.getState(id);
    }
}
