package center.task.prm.tp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import ru.vassaev.core.thread.LogEvent;
import ru.vassaev.core.types.TimeList;
import ru.vassaev.core.base.Null;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.io.OutputByteBuffer;
import ru.vassaev.core.util.Strings;

import center.task.CalculateException;
import center.task.Context;
import center.task.Record;
import center.task.ATaskInfo;
import center.task.prm.Prm;
import center.task.prm.Type;
/**
 * Тип - строка
 * @author Vassaev A.V.
 * @version 1.0
 */
public class Str extends Type {

  private String encoding;
  public Str() throws SysException {
    super();
    isCache = true;
    isNonDB = true;
  }

  public Str(Element e, ATaskInfo owner) throws SysException {
    super(e, owner);
    if (e.hasAttribute("encoding"))
    	encoding = e.getAttribute("encoding");
    else
    	encoding = Strings.getXMLValue(e, "encoding");
    if (encoding == null || encoding.length() == 0)
    	encoding = "utf-8";
  }

  public Object calculate(TimeList tl, Context cntx) throws CalculateException {
  	LogEvent old = setEvent(LogEvent.CALC);
  	try {
  		tl.addPointTime(TimeList.Type.START);
      Object v = super.calculate0(tl, cntx);
      if (Null.NULL.equals(v))
        return null;
      v = super.modify(tl, cntx, v);
      if (v instanceof Node)
        return ((Node) v).getTextContent();
      else if (v instanceof Record) {
      	Record r = (Record) v;
      	if (r.size() == 0)
      		return null;
        return Strings.getString(r.value.get(0));
      } else if (v instanceof File) {
      	File f = (File)v;
      	FileInputStream is = null;
      	try {
        	is = new FileInputStream(f.getCanonicalPath());
        	return Strings.getStringBuffer(is, encoding);
      	} catch(IOException e) {
        	throw new CalculateException(this.getGLName(), e);
      	}	finally {
      		if (is != null) try {
      			is.close();
      		} catch(IOException e) {
      			e.printStackTrace();
      		}
      	}
      } else if (v instanceof OutputByteBuffer) {
      	InputStream is = ((OutputByteBuffer)v).getIS();
      	try {
        	return Strings.getStringBuffer(is, encoding);
      	} catch(IOException e) {
        	throw new CalculateException(this.getGLName(), e);
      	}	finally {
      		if (is != null) try {
      			is.close();
      		} catch(IOException e) {
      			e.printStackTrace();
      		}
      	}
      }
      return v.toString();
  	} catch(CalculateException e) {
  		if (whenException == null)
  			throw e;
  		else {
  			if (whenException instanceof Prm)
					try {
						return ((Prm)whenException).getObject(cntx);
					} catch (SysException e1) {
						Throwable cause = e1.getCause();
						if (cause instanceof CalculateException)
							throw (CalculateException)cause;
						throw new CalculateException(fullname, e1);
					}
  			return whenException;
  		}
  	} finally {
    	setEvent(old);
  		tl.addPointTime(TimeList.Type.END);
  	}
  }

  public String toMask(Context cntx) throws CalculateException {
    String v = Strings.getString(this.getValue(cntx));
    if (Null.equ(v))
      return v;
    return v;
  }

}
