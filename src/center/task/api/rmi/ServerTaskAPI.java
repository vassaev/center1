package center.task.api.rmi;

import java.io.File;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import ru.vassaev.core.exception.SysException;

import center.task.State;
import center.task.api.ATaskAPI;
import center.task.api.ITaskAPI;
import center.task.api.TaskCI;

public class ServerTaskAPI extends UnicastRemoteObject implements
		InterfaceTaskAPI {

	private static final long serialVersionUID = -4579207918366312210L;

	private static class TAPI {
		//String alias_native;
		ITaskAPI ta_native;
		ServerTaskAPI ta_rmi_server;
	}

	private static Map<String, TAPI> api = new HashMap<String, TAPI>();

	public static ServerTaskAPI register(String alias, String cls_native,
			String alias_native) throws RemoteException {
		synchronized (api) {
			TAPI tapi = api.get(alias);
			if (tapi == null) {
				ITaskAPI ta;
				try {
					ta = ATaskAPI.getInstance(cls_native, center.task.api.mem.TaskAPI.class,
							alias_native);
				} catch (SysException e) {
					throw new RemoteException(e.getMessage(), e);
				}
				if (ta == null)
					return null;
				tapi = new TAPI();
				//tapi.alias_native = alias_native;
				tapi.ta_native = ta;
				tapi.ta_rmi_server = new ServerTaskAPI(alias);
				api.put(alias, tapi);
			}
			return tapi.ta_rmi_server;
		}
	}

	private String alias;

	private ServerTaskAPI(String alias) throws RemoteException {
		this.alias = alias;
	}

	private ITaskAPI getNativeAPI() {
		synchronized (api) {
			TAPI tapi = api.get(alias);
			if (tapi != null)
			  return tapi.ta_native;
			return null;
		}
	}

	public InterfaceTaskAPI getInterfaceInstance(String alias)
			throws RemoteException {
		return getInstance(alias);
	}

	public ServerTaskAPI() throws RemoteException {
		this.alias = null;
	}

	public static ServerTaskAPI getInstance(String alias) {
		synchronized (api) {
			TAPI tapi = api.get(alias);
			if (tapi != null) {
				return tapi.ta_rmi_server;
			}
			return null;
		}
	}

	public String getAlias() throws RemoteException {
		System.out.println("get alias");
		return alias;
	}

	public void registerClasses(long subject_id, Set<String> hs)
			throws RemoteException {
		try {
			getNativeAPI().registerClasses(subject_id, hs);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public long getSubject(long old_subject_id) throws RemoteException {
		try {
			return getNativeAPI().getSubject(old_subject_id);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public long setParamObject(long task_id, String name, Object value)
			throws RemoteException {
		try {
			return getNativeAPI().setParamObject(task_id, name, value);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public long setParamObject(long task_id, String name, Object value,
			long wait, long calc, long scall) throws RemoteException {
		try {
			return getNativeAPI().setParamObject(task_id, name, value, wait, calc,
					scall);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public long createTask(long subject_id, String cls) throws RemoteException {
		try {
			return getNativeAPI().createTask(subject_id, cls);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public long createTask(long subject_id, String cls, long parent_task_id)
			throws RemoteException {
		try {
			return getNativeAPI().createTask(subject_id, cls, parent_task_id);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public String getClassName(long id) throws RemoteException {
		try {
			return getNativeAPI().getClassName(id);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}
  /*
	public boolean checkDNF(long id, DNF dnf, Object[] flds)
			throws RemoteException {
		try {
			return getNativeAPI().checkDNF(id, dnf, flds);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}
  //*/
	public long setTaskDT(long id, String dt, String duration,
			String block_duration, Double arch_day) throws RemoteException {
		try {
			return getNativeAPI().setTaskDT(id, dt, duration, block_duration,
					arch_day);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public long setTaskError(long id, Throwable e) throws RemoteException {
		try {
			return getNativeAPI().setTaskError(id, e);
		} catch (SysException e1) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public String getParamTask(long id, String name) throws RemoteException {
		try {
			return getNativeAPI().getParamTask(id, name);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public Map<String, Object> getGroupParams(long id, String group)
			throws RemoteException {
		try {
			return getNativeAPI().getGroupParams(id, group);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}
  /*
	public Reader getTaskParamLong(long id, String name) throws RemoteException {
		try {
			return getNativeAPI().getTaskParamLong(id, name);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}
  //*/
	public State getState(long id) throws RemoteException {
		try {
			return getNativeAPI().getState(id);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public long setState(long subject_id, long id, State st)
			throws RemoteException {
		try {
			return getNativeAPI().setState(subject_id, id, st);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public long setState(long subject_id, long id, State st, Long processor_id)
			throws RemoteException {
		try {
			return getNativeAPI().setState(subject_id, id, st, processor_id);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public long setReady(long subject_id, long id) throws RemoteException {
		try {
			return getNativeAPI().setReady(subject_id, id);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public long setReady(long subject_id, long id, Long processor_id)
			throws RemoteException {
		try {
			return getNativeAPI().setReady(subject_id, id, processor_id);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public long setBlocked(long subject_id, long id) throws RemoteException {
		try {
			return getNativeAPI().setBlocked(subject_id, id);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public TaskCI getBlocked(long subject_id, long wait)
			throws RemoteException {
		try {
			return getNativeAPI().getBlocked(subject_id, wait);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public long setProcessing(long subject_id, long id) throws RemoteException {
		try {
			return getNativeAPI().setProcessing(subject_id, id);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public long setTimeout(long subject_id, long id) throws RemoteException {
		try {
			return getNativeAPI().setTimeout(subject_id, id);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public long setScheduled(long subject_id, long id) throws RemoteException {
		try {
			return getNativeAPI().setScheduled(subject_id, id);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public long setBreaking(long subject_id, long id) throws RemoteException {
		try {
			return getNativeAPI().setBreaking(subject_id, id);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public long setBroken(long subject_id, long id) throws RemoteException {
		try {
			return getNativeAPI().setBroken(subject_id, id);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public long setCanceled(long subject_id, long id) throws RemoteException {
		try {
			return getNativeAPI().setCanceled(subject_id, id);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public long setDoneErr(long subject_id, long id) throws RemoteException {
		try {
			return getNativeAPI().setDoneErr(subject_id, id);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public long setDoneOk(long subject_id, long id) throws RemoteException {
		try {
			return getNativeAPI().setDoneOk(subject_id, id);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public State waitFinished(long subject_id, long id, long msec)
			throws RemoteException {
		try {
			return getNativeAPI().waitFinished(subject_id, id, msec);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public long log(long subject_id, long id, String info, boolean base)
			throws RemoteException {
		try {
			return getNativeAPI().log(subject_id, id, info, base);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public long log(long subject_id, long id, String info) throws RemoteException {
		try {
			return getNativeAPI().log(subject_id, id, info);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public void writeLogToFile(long id, File file, String charset)
			throws RemoteException {
		try {
			getNativeAPI().writeLogToFile(id, file, charset);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

	public Object getParam(long id, String fullname) throws RemoteException {
		try {
			return getNativeAPI().getParam(id, fullname);
		} catch (SysException e) {
			throw new RemoteException(e.getMessage(), e);
		}
	}

}
