package center.task;

/**
 */
public class TaskException extends Exception {

	private static final long serialVersionUID = 4121329732676830529L;
	public final State state;

  public State getState() {
    return state;
  }

  public TaskException(State st, String txt, Throwable ex) {
    super(txt, ex);
    this.state = st;
  }

  public TaskException(State st, Throwable ex) {
    super(ex);
    this.state = st;
  }

  public TaskException(State st, String txt) {
    super(txt);
    this.state = st;
  }

}