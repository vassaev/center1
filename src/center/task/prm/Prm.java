package center.task.prm;

import java.util.Collection;

import org.w3c.dom.Element;

import ru.vassaev.core.base.Null;
import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.thread.PoolThread;
import ru.vassaev.core.thread.ThreadEventListener;
import ru.vassaev.core.util.Strings;
import center.task.CalculateException;
import center.task.Context;

/**
 * Ссылка на параметр
 */
public class Prm {
	public final String fullname;
  public final boolean isParent;

  /**
   * Получить полное имя ссылки на параметр
   * @return полное имя
   */
	public final String getFullName() {
		return fullname;
	}

  /**
   * Конструктор по группе и имени
   * @param grp - группа
   * @param name - имя
   */
	public Prm(String grp, String name) {
		this(false, false, grp, name, null);
	}
  private Long wait = null;
	public Prm(Element e) {
    String name = Strings.getXMLValue(e, "name");
    if (name == null)
      name = e.getAttribute("name");
    String grp = Strings.getXMLValue(e, "grp");
    if (grp == null)
      grp = e.getAttribute("grp");
    fullname = (grp == null || grp.equals("")) ? name : grp + "/" + name;
    isParent = Strings.parseBooleanNvl(e.getAttribute("parent"), false);
    wait = Strings.parseLong(e.getAttribute("wait"));
    if (wait != null && wait <= 0)
    	wait = null;
  }
  private boolean isExcept = false;
  public Prm(boolean isExcept, boolean isParent, String grp, String name, Long wait) {
    fullname = (grp == null || grp.equals("")) ? name : grp + "/" + name;
    this.isParent = isParent;
    this.isExcept = isExcept;
    this.wait = (wait != null && wait > 0) ? wait : null;
  }
  /**
   * Получить значение параметра
   * @param cntx - контекст
   * @return значение
   * @throws SysException - в случае ошибок
   * @throws CalculateException 
   */
	public Object getObject(Context cntx) throws SysException, CalculateException {
		if (wait != null)
			return getObject(cntx, wait);
		Object result = null;
    if (isParent)
      if (cntx.parent != null)
      	result = cntx.parent.getPrmByFullName(fullname);
      else
      	result = null;
    else
    	result = cntx.getPrmByFullName(fullname);
    if (isExcept)
    	throw new CalculateException(Strings.getString(result));
    return result;
	}
	
	public Long getWait() {
		return wait;
	}
	
	public Object getObject(Context cntx, long timeout) throws SysException, CalculateException {
		Object result = null;
    if (isParent)
      if (cntx.parent != null)
      	result = cntx.parent.getPrmByFullName(fullname, timeout);
      else
      	result = null;
    else
    	result = cntx.getPrmByFullName(fullname, timeout);
    if (isExcept)
    	throw new CalculateException(Strings.getString(result));
    return result;
	}

	public Object getObjectWait(Context cntx, PoolThread pool, Collection<ThreadEventListener> lst) 
			throws SysException, CalculateException {
		Object result = null;
    if (isParent)
      if (cntx.parent != null)
      	result = cntx.parent.getPrmAsync(pool, fullname, lst);
      else
      	result = null;
    else
    	result = cntx.getPrmAsync(pool, fullname, lst);
    if (isExcept)
    	throw new CalculateException(Strings.getString(result));
    return result;
	}

	/**
   * Значение параметра как строка
   * @param cntx - контекст
   * @return значение параметра
   * @throws ru.vassaev.core.exception.SysException - в случае ошибки
   * @throws CalculateException 
   */
	public String getString(Context cntx) throws SysException, CalculateException {
		if (wait != null)
			return getString(cntx, wait);
		String result;
    if (isParent)
      if (cntx.parent != null)
      	result = Strings.getString(cntx.parent.getPrmByFullName(fullname));
      else
      	result = null;
    else
    	result = Strings.getString(cntx.getPrmByFullName(fullname));
    if (isExcept)
    	throw new CalculateException(result);
    return result;
	}
	public String getString(Context cntx, long timeout) throws SysException, CalculateException {
		String result;
    if (isParent)
      if (cntx.parent != null)
      	result = Strings.getString(cntx.parent.getPrmByFullName(fullname, timeout));
      else
      	result = null;
    else
    	result = Strings.getString(cntx.getPrmByFullName(fullname, timeout));
    if (isExcept)
    	throw new CalculateException(result);
    return result;
	}

  public final static Prm getLink(Element e) {
    String name = Strings.getXMLValue(e, "name");
    if (name == null)
      name = e.getAttribute("name");
    String grp = Strings.getXMLValue(e, "grp");
    if ((grp == null) && (e.hasAttribute("grp")))
      grp = e.getAttribute("grp");
    Long wait = Strings.parseLong(e.getAttribute("wait"));
    boolean isParent = Strings.parseBooleanNvl(e.getAttribute("parent"), false);
  	boolean isExcept = (e.getNodeName().equals("throw"));
    return new Prm(isExcept, isParent, grp, name, wait);
  }
  /**
	 * Получение ссылки на параметр или значение
	 * Варианты:
	 * <e parent="true/false"><name>NAME</name><grp>GRP</grp></e> - ссылка на параметр
	 * <e parent="true/false"><name>NAME</name></e> - ссылка на параметр
	 * <e><NULL/></e> - NULL
	 * <e>TEXT</e> - Текст
   * @param e - часть документа, кот. подлежит обработке
   * @return объект Prm или String
   */
	public static Object getPrm(Element e) {
		if (e == null)
			return null;
		Element val = (Element)Strings.getXMLObject(e, "prm");
		if (val != null)
			return getLink(val);
		val = (Element)Strings.getXMLObject(e, "NULL");
		if (val != null) 
			return Null.NULL;
		return e.getTextContent();
	}

  public static Prm getPrmOwner(Element e) {
    if (e == null)
      return null;
    return getLink(e);
  }

  public static Prm getPrmChild(Element e, String childPath) {
    if (e == null)
      return null;
    Element ch = (Element) Strings.getXMLObject(e, childPath);
    if (ch == null)
      return null;
    return getLink(ch);
  }

  public static Object getPrm(Element e, String nodePrm) {
    if (e == null)
      return null;
    Element val = (Element)Strings.getXMLObject(e, nodePrm);
    if (val != null)
      return getLink(val);
    val = (Element)Strings.getXMLObject(e, "NULL");
    if (val != null)
      return Null.NULL;
    return e.getTextContent();
  }
	/**
	 * Получение ссылки на параметр или значения
	 * Варианты:
	 * <e><enviroment>NAME</enviroment></e> - ссылка на параметр
	 * <e><sysprop>NAME</sysprop></e> - ссылка на параметр
	 * <e><NULL/></e> - NULL
	 * <e>TEXT</e> - Текст
   * @param e - часть документа, кот. подлежит обработке
   * @return объект Prm или String
	 */
	public static String getConst(Element e) {
		if (e == null)
			return null;
		String nm = Strings.getXMLValue(e, "environment");
		if (nm != null)
			return System.getenv(nm);
		nm = Strings.getXMLValue(e, "sysprop");
		if (nm != null)
			return System.getProperty(nm);
		Element val = (Element)Strings.getXMLObject(e, "NULL");
		if (val != null) 
			return null;
		return e.getTextContent();
	}
}
