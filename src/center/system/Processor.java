package center.system;

import java.util.Properties;

import center.task.api.ITaskAPI;
import ru.vassaev.core.base.Null;
import ru.vassaev.core.exception.SysException;

/**
 * @author Vassaev A.V.
 * @version 1.0
 */
public abstract class Processor {

    public static final Object getProperty(Properties prop, Object key) {
        Object o = prop.get(key);
        if (o == null)
            return null;
        if (o.equals(Null.getInstance()))
            return null;
        return o;
    }

    private boolean isInit = false;
    private Properties props = new Properties();
    private Properties stepProps = new Properties();
//
    protected final Properties getInitProperties() {
        return props;
    }

    protected final Properties getStepProperties() {
        return stepProps;
    }

    public Processor() {
        super();
    }

//
    public final void init () throws SysException {
        synchronized (this) {
            isInit = false;
            initialize();
            isInit = true;
        }
    }

    public final boolean exec () throws SysException {
        synchronized (this) {
            if (isInit)
                return execute();
            return false;
        }
    }

    public final void setInitProperty(Object key, Object val) {
        props.put(key, val);
    }

    public final Object getInitProperty(Object key) {
        return props.get(key);
    }

    public final void clearInitProperty(Object key) {
        props.put(key, Null.getInstance());
    }

    public final void setStepProperty(Object key, Object val) {
        synchronized (this) {
            if (isInit)
                stepProps.put(key, val);
        }
    }

    public final Object getStepProperty(Object key) {
        synchronized (this) {
            if (isInit)
                return stepProps.get(key);
            return null;
        }
    }

    public final void clearStepProperty(Object key) {
        synchronized (this) {
            if (isInit)
                stepProps.put(key, Null.getInstance());
        }
    }

    private ITaskAPI ta = null;
    private long subject_id;
    private long id;
    public void setTaskAPIParams(ITaskAPI ta, long subject_id, long id) {
        this.ta = ta;
        this.subject_id = subject_id;
        this.id = id;
    }
    public void logLN(String txt) throws SysException {
        if (ta == null)
            System.out.println(txt);
        else
            ta.log(subject_id, id, txt);
    }
//
    protected abstract void initialize() throws SysException;
    protected abstract boolean execute() throws SysException;
}
