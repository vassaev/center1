package center.system.msg;

import ru.vassaev.core.exception.SysException;
import ru.vassaev.core.PrmInterface;

import java.io.File;

/**
 */
public interface FileQuery {
    public File sendFile(File query, PrmInterface prms, File to) throws SysException;
    public File sendFile(byte[] query, PrmInterface prms, File to) throws SysException;
}
