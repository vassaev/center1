CREATE OR REPLACE 
FUNCTION add_timestamp_sec(pStart in timestamp, pSec in number) 
return timestamp
deterministic
-- ���������� ������ �� ������� � ������� timestamp
as  
    xSec interval day to second;
    xCur date;
    xNext date;
    xDayN number;
begin
    xDayN := trunc(pSec/(60*60*24));
    xCur := cast(pStart as date);
    xNext := xCur + xDayN;
    xSec := pStart - cast(xCur as timestamp)
        + NUMTODSINTERVAL(pSec - xDayN*(60*60*24), 'SECOND');
    return cast(xNext as timestamp) + xSec;
end;
/
CREATE SEQUENCE seq_params
/
CREATE SEQUENCE seq_subject
/
CREATE SEQUENCE seq_tasks
/
CREATE TABLE classes
    (processor_id                   NUMBER NOT NULL,
    classname                      VARCHAR2(32) NOT NULL)
/
CREATE UNIQUE INDEX idx_classes ON classes
  (
    processor_id                    ASC,
    classname                       ASC
  )
/
COMMENT ON TABLE classes IS '������ ������� �������, ������� ����� ��������� ����������'
/
COMMENT ON COLUMN classes.classname IS '��� ������ �������'
/
COMMENT ON COLUMN classes.processor_id IS '������������� ����������'
/
CREATE TABLE log
    (dt                             TIMESTAMP (6) DEFAULT systimestamp,
    txt                            VARCHAR2(2000))
/
CREATE INDEX idx_log_01 ON log
  (
    dt                              ASC
  )
/
COMMENT ON TABLE log IS '������� ����� ��� �������'
/
COMMENT ON COLUMN log.dt IS '�����'
/
COMMENT ON COLUMN log.txt IS '������'
/
CREATE TABLE params
    (id                             NUMBER(*,0) NOT NULL,
    cr_dt                          TIMESTAMP (6) DEFAULT systimestamp NOT NULL,
    task_id                        NUMBER(*,0) NOT NULL,
    grp                            VARCHAR2(32) DEFAULT NULL,
    name                           VARCHAR2(64),
    value                          VARCHAR2(4000) DEFAULT NULL,
    value_clob                     CLOB DEFAULT NULL,
    value_clmn                     VARCHAR2(16) DEFAULT 'VALUE' NOT NULL,
    value_blob                     BLOB DEFAULT NULL,
    stat_wait                      NUMBER DEFAULT 0,
    stat_calc                      NUMBER DEFAULT 0,
    len                            NUMBER DEFAULT 0,
    tp                             VARCHAR2(255),
    stat_scall                     NUMBER DEFAULT 0)
/
CREATE INDEX idx_params_01 ON params
  (
    task_id                         ASC
  )
REVERSE
/
CREATE UNIQUE INDEX idx_params_00 ON params
  (
    task_id                         ASC,
    grp                             ASC,
    name                            ASC
  )
/
CREATE UNIQUE INDEX idx_params ON params
  (
    id                              ASC
  )
/
CREATE OR REPLACE TRIGGER tr_params_00
 BEFORE
  INSERT
 ON params
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
WHEN (NEW.id is null)
begin
    select seq_params.NEXTVAL into :NEW.id from dual;
end;
/
COMMENT ON TABLE params IS '��������� � �������'
/
COMMENT ON COLUMN params.cr_dt IS '����� �������� ���������'
/
COMMENT ON COLUMN params.grp IS '������ ���������'
/
COMMENT ON COLUMN params.id IS '���������� ������������� ���������'
/
COMMENT ON COLUMN params.len IS '����� �������� � ��������/������'
/
COMMENT ON COLUMN params.name IS '��� ���������'
/
COMMENT ON COLUMN params.stat_calc IS '����� �� ����������'
/
COMMENT ON COLUMN params.stat_scall IS '����� �� ������������ ������'
/
COMMENT ON COLUMN params.stat_wait IS '����� �� ��������'
/
COMMENT ON COLUMN params.task_id IS '������������� �������'
/
COMMENT ON COLUMN params.tp IS '���'
/
COMMENT ON COLUMN params.value IS '�������� ���������'
/
COMMENT ON COLUMN params.value_blob IS '�������������� ���� ��� �������� �������� ������ ���������� � �������� ����'
/
COMMENT ON COLUMN params.value_clmn IS '��� �������, ����������� �������� ��������� ��� ''FILE'''
/
COMMENT ON COLUMN params.value_clob IS '�������������� ���� ��� �������� �������� ������ ����������'
/
CREATE TABLE params_arch
    (id                             NUMBER(*,0),
    cr_dt                          TIMESTAMP (6) DEFAULT NULL,
    task_id                        NUMBER(*,0),
    grp                            VARCHAR2(32) DEFAULT NULL,
    name                           VARCHAR2(64),
    value                          VARCHAR2(4000),
    value_clob                     CLOB DEFAULT NULL,
    value_clmn                     VARCHAR2(16) DEFAULT NULL,
    value_blob                     BLOB DEFAULT NULL,
    stat_wait                      NUMBER DEFAULT 0,
    stat_calc                      NUMBER DEFAULT 0,
    len                            NUMBER DEFAULT 0,
    tp                             VARCHAR2(255),
    stat_scall                     NUMBER DEFAULT 0)
/
CREATE INDEX idx_params_arch ON params_arch
  (
    id                              ASC
  )
/
CREATE INDEX idx_params_arch_00 ON params_arch
  (
    task_id                         ASC,
    grp                             ASC,
    name                            ASC
  )
/
COMMENT ON TABLE params_arch IS '��������� � ������� � ������'
/
COMMENT ON COLUMN params_arch.cr_dt IS '����� �������� ���������'
/
COMMENT ON COLUMN params_arch.grp IS '������ ���������'
/
COMMENT ON COLUMN params_arch.id IS '���������� ������������� ���������'
/
COMMENT ON COLUMN params_arch.len IS '����� �������� � ��������/������'
/
COMMENT ON COLUMN params_arch.name IS '��� ���������'
/
COMMENT ON COLUMN params_arch.stat_calc IS '����� �� ����������'
/
COMMENT ON COLUMN params_arch.stat_scall IS '����� �� ������������ ������'
/
COMMENT ON COLUMN params_arch.stat_wait IS '����� ��������'
/
COMMENT ON COLUMN params_arch.task_id IS '������������� �������'
/
COMMENT ON COLUMN params_arch.tp IS '���'
/
COMMENT ON COLUMN params_arch.value IS '�������� ���������'
/
COMMENT ON COLUMN params_arch.value_blob IS '�������������� ���� ��� �������� �������� ������ ���������� � �������� ����'
/
COMMENT ON COLUMN params_arch.value_clmn IS '��� �������, ����������� �������� ��������� ��� ''FILE'''
/
COMMENT ON COLUMN params_arch.value_clob IS '�������������� ���� ��� �������� �������� ������ ����������'
/
CREATE TABLE params_array
    (id                             NUMBER(*,0) NOT NULL,
    idx                            NUMBER(*,0) NOT NULL,
    cr_dt                          TIMESTAMP (6) DEFAULT systimestamp NOT NULL,
    value                          VARCHAR2(4000) DEFAULT NULL,
    value_clob                     CLOB DEFAULT NULL,
    value_blob                     BLOB DEFAULT NULL,
    idx_name                       VARCHAR2(255),
    len                            NUMBER DEFAULT 0)
/
CREATE UNIQUE INDEX idx_params_array ON params_array
  (
    id                              ASC,
    idx                             ASC
  )
/
COMMENT ON TABLE params_array IS '������ � ���������� �������'
/
COMMENT ON COLUMN params_array.cr_dt IS '����� �������� ���������'
/
COMMENT ON COLUMN params_array.id IS '���������� ������������� ���������'
/
COMMENT ON COLUMN params_array.idx IS '������ � �������'
/
COMMENT ON COLUMN params_array.idx_name IS '��� �������'
/
COMMENT ON COLUMN params_array.len IS '����� �������� � ��������/������'
/
COMMENT ON COLUMN params_array.value IS '�������� - ������'
/
COMMENT ON COLUMN params_array.value_blob IS '�������� - �������� ������'
/
COMMENT ON COLUMN params_array.value_clob IS '�������� - ����� ����� 4000 ��������'
/
CREATE TABLE params_array_arch
    (id                             NUMBER(*,0) NOT NULL,
    idx                            NUMBER(*,0) NOT NULL,
    cr_dt                          TIMESTAMP (6) DEFAULT NULL NOT NULL,
    value                          VARCHAR2(4000) DEFAULT NULL,
    value_clob                     CLOB DEFAULT NULL,
    value_blob                     BLOB DEFAULT NULL,
    idx_name                       VARCHAR2(255),
    len                            NUMBER DEFAULT 0)
  LOB ("VALUE_CLOB") STORE AS SYS_LOB0000057381C00005$$
  (
   NOCACHE LOGGING
   CHUNK 8192
   PCTVERSION 10
  )
  LOB ("VALUE_BLOB") STORE AS SYS_LOB0000057381C00006$$
  (
   NOCACHE LOGGING
   CHUNK 8192
   PCTVERSION 10
  )
/
CREATE INDEX idx_params_array_arch ON params_array_arch
  (
    id                              ASC,
    idx                             ASC
  )
/
COMMENT ON TABLE params_array_arch IS '������ � ���������� ������� � ������'
/
COMMENT ON COLUMN params_array_arch.cr_dt IS '����� �������� ���������'
/
COMMENT ON COLUMN params_array_arch.id IS '���������� ������������� ���������'
/
COMMENT ON COLUMN params_array_arch.idx IS '������ � �������'
/
COMMENT ON COLUMN params_array_arch.idx_name IS '��� �������'
/
COMMENT ON COLUMN params_array_arch.len IS '����� �������� � ��������/������'
/
COMMENT ON COLUMN params_array_arch.value IS '�������� - ������'
/
COMMENT ON COLUMN params_array_arch.value_blob IS '�������� - �������� ������'
/
COMMENT ON COLUMN params_array_arch.value_clob IS '�������� - ����� ����� 4000 ��������'
/
CREATE GLOBAL TEMPORARY TABLE params_cpy
    (id                             NUMBER(*,0) NOT NULL,
    cr_dt                          TIMESTAMP (6) DEFAULT systimestamp NOT NULL,
    task_id                        NUMBER(*,0) NOT NULL,
    grp                            VARCHAR2(32) DEFAULT 'IN' NOT NULL,
    name                           VARCHAR2(64),
    value                          VARCHAR2(4000),
    value_clob                     CLOB DEFAULT NULL,
    value_clmn                     VARCHAR2(16) DEFAULT 'VALUE' NOT NULL,
    value_blob                     BLOB DEFAULT NULL,
    len                            NUMBER DEFAULT 0,
    tp                             VARCHAR2(255))
ON COMMIT DELETE ROWS
/
CREATE UNIQUE INDEX idx_params_cpy ON params_cpy
  (
    id                              ASC
  )
/

CREATE UNIQUE INDEX idx_params_cpy_00 ON params_cpy
  (
    task_id                         ASC,
    grp                             ASC,
    name                            ASC
  )
/
COMMENT ON TABLE params_cpy IS '��������� � ������� ��� �����������'
/
COMMENT ON COLUMN params_cpy.cr_dt IS '����� �������� ���������'
/
COMMENT ON COLUMN params_cpy.grp IS '������ ���������'
/
COMMENT ON COLUMN params_cpy.id IS '���������� ������������� ���������'
/
COMMENT ON COLUMN params_cpy.len IS '�����'
/
COMMENT ON COLUMN params_cpy.name IS '��� ���������'
/
COMMENT ON COLUMN params_cpy.task_id IS '������������� �������'
/
COMMENT ON COLUMN params_cpy.tp IS '���'
/
COMMENT ON COLUMN params_cpy.value IS '�������� ���������'
/
COMMENT ON COLUMN params_cpy.value_blob IS '�������������� ���� ��� �������� �������� ������ ���������� � �������� ����'
/
COMMENT ON COLUMN params_cpy.value_clmn IS '��� �������, ����������� �������� ���������'
/
COMMENT ON COLUMN params_cpy.value_clob IS '�������������� ���� ��� �������� �������� ������ ����������'
/
CREATE TABLE protocol
    (dt                             TIMESTAMP (6) NOT NULL,
    task_id                        NUMBER NOT NULL,
    subject_id                     NUMBER NOT NULL,
    idx                            NUMBER NOT NULL,
    text                           VARCHAR2(4000))
/
CREATE INDEX idx_protocol_00 ON protocol
  (
    dt                              ASC
  )
/
CREATE INDEX idx_protocol ON protocol
  (
    task_id                         ASC
  )
/
CREATE INDEX idx_protocol_01 ON protocol
  (
    dt                              ASC,
    task_id                         ASC
  )
/
COMMENT ON TABLE protocol IS '�������� ��������'
/
COMMENT ON COLUMN protocol.dt IS '���� ������'
/
COMMENT ON COLUMN protocol.idx IS '����� ������ �� �������'
/
COMMENT ON COLUMN protocol.subject_id IS '�����, ����������� ������ � �������'
/
COMMENT ON COLUMN protocol.task_id IS '������������� �������'
/
COMMENT ON COLUMN protocol.text IS '����������'
/
CREATE TABLE protocol_arch
    (dt                             TIMESTAMP (6) NOT NULL,
    task_id                        NUMBER NOT NULL,
    subject_id                     NUMBER NOT NULL,
    idx                            NUMBER NOT NULL,
    text                           VARCHAR2(4000))
/
CREATE INDEX idx_protocol_arch ON protocol_arch
  (
    dt                              ASC,
    task_id                         ASC
  )
/
CREATE INDEX idx_protocol_arch_01 ON protocol_arch
  (
    dt                              ASC
  )
/
CREATE INDEX idx_protocol_arch_00 ON protocol_arch
  (
    task_id                         ASC
  )
/
COMMENT ON TABLE protocol_arch IS '�������� �������� � ������'
/
COMMENT ON COLUMN protocol_arch.dt IS '���� ������'
/
COMMENT ON COLUMN protocol_arch.idx IS '����� ������ �� �������'
/
COMMENT ON COLUMN protocol_arch.subject_id IS '�����, ����������� ������ � �������'
/
COMMENT ON COLUMN protocol_arch.task_id IS '������������� �������'
/
COMMENT ON COLUMN protocol_arch.text IS '����������'
/
CREATE TABLE status
    (name                           VARCHAR2(16) NOT NULL,
    flag                           NUMBER(*,0) DEFAULT 0 NOT NULL)
/
CREATE UNIQUE INDEX idx_status ON status
  (
    name                            ASC
  )
/
COMMENT ON TABLE status IS '������ ��������� �������� �������'
/
COMMENT ON COLUMN status.flag IS '0-��������� ������, 
1-����������� ������, 
>1-�������������'
/
COMMENT ON COLUMN status.name IS '������������ �������'
/
CREATE TABLE status_history
    (task_id                        NUMBER NOT NULL,
    dt                             TIMESTAMP (6) DEFAULT systimestamp NOT NULL,
    old_status                     VARCHAR2(16),
    new_status                     VARCHAR2(16) NOT NULL,
    usr                            VARCHAR2(32))
/
CREATE INDEX idx_status_history ON status_history
  (
    task_id                         ASC
  )
/
COMMENT ON TABLE status_history IS '������� ��������� ������� �������'
/
COMMENT ON COLUMN status_history.dt IS '���� ��������� �������'
/
COMMENT ON COLUMN status_history.new_status IS '����� ������'
/
COMMENT ON COLUMN status_history.old_status IS '���������� ������'
/
COMMENT ON COLUMN status_history.task_id IS '������������� �������'
/
COMMENT ON COLUMN status_history.usr IS '������������, ���������� ������'
/
CREATE TABLE status_history_arch
    (task_id                        NUMBER NOT NULL,
    dt                             TIMESTAMP (6) DEFAULT systimestamp NOT NULL,
    old_status                     VARCHAR2(16),
    new_status                     VARCHAR2(16) NOT NULL,
    usr                            VARCHAR2(32))
/
CREATE INDEX idx_status_history_arch ON status_history_arch
  (
    task_id                         ASC
  )
/
COMMENT ON TABLE status_history_arch IS '������� ��������� ������� �������'
/
COMMENT ON COLUMN status_history_arch.dt IS '���� ��������� �������'
/
COMMENT ON COLUMN status_history_arch.new_status IS '����� ������'
/
COMMENT ON COLUMN status_history_arch.old_status IS '���������� ������'
/
COMMENT ON COLUMN status_history_arch.task_id IS '������������� �������'
/
COMMENT ON COLUMN status_history_arch.usr IS '������������, ���������� ������'
/
CREATE TABLE tasks
    (id                             NUMBER(*,0) NOT NULL,
    dt_created                     TIMESTAMP (6) DEFAULT systimestamp,
    dt_goal                        TIMESTAMP (6),
    dt_started                     TIMESTAMP (6),
    dt_finished                    TIMESTAMP (6),
    status_id                      VARCHAR2(16) DEFAULT 'CREATED',
    classname                      VARCHAR2(32),
    initiator_id                   NUMBER(*,0) DEFAULT 0,
    processor_id                   NUMBER(*,0) DEFAULT 0,
    program                        VARCHAR2(32),
    opid                           VARCHAR2(32),
    block_duration                 NUMBER DEFAULT 10,
    finish_duration                NUMBER DEFAULT 120,
    arch_day                       NUMBER DEFAULT 7,
    last_ping                      TIMESTAMP (6) DEFAULT systimestamp,
    errs                           VARCHAR2(4000),
    errp                           CLOB,
    parent_id                      NUMBER,
    dt_end_block                   TIMESTAMP (6) DEFAULT NULL,
    dt_arch                        TIMESTAMP (6))
/
CREATE INDEX idx_tasks_00 ON tasks
  (
    classname                       ASC
  )
/
CREATE INDEX idx_tasks_01 ON tasks
  (
    dt_goal                         ASC,
    classname                       ASC
  )
/
CREATE INDEX idx_tasks_05 ON tasks
  (
    status_id                       ASC,
    dt_end_block                    ASC
  )
REVERSE
/
CREATE INDEX idx_tasks_06 ON tasks
  (
    dt_arch                         ASC
  )
REVERSE
/
CREATE INDEX idx_tasks_04 ON tasks
  (
    status_id                       ASC,
    dt_goal                         ASC,
    "DT_GOAL"+"BLOCK_DURATION"/86400 ASC
  )
/
CREATE INDEX idx_tasks_03 ON tasks
  (
    opid                            ASC
  )
/
CREATE INDEX idx_tasks_02 ON tasks
  (
    status_id                       ASC,
    dt_goal                         ASC
  )
/
CREATE UNIQUE INDEX idx_tasks ON tasks
  (
    id                              ASC
  )
/
ALTER TABLE tasks
ADD CONSTRAINT limit_duration CHECK (block_duration >= 0 and block_duration <= 3600*24)
/
CREATE OR REPLACE TRIGGER tr_alert
 AFTER
   INSERT OR UPDATE OF status_id
 ON tasks
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
declare
       message varchar2(200);
       flag number;
begin
     select flag into flag from status where name=:new.status_id;
     message := 'flag='||flag||';id='||:new.id
             ||';old='||:old.status_id||';new='||:new.status_id||';class='||:new.classname;
     if (flag = 1) then
        dbms_alert.signal('center$change_status_finish',message);
     elsif (:new.status_id = 'READY') then
        dbms_alert.signal('center$change_status_ready',message);
     --else
        --dbms_alert.signal('center$change_status',message);
     end if;
     insert into status_history(task_id, dt, old_status, new_status, usr)
     values (:new.id, systimestamp, :old.status_id, :new.status_id, user);
end;
/
CREATE OR REPLACE TRIGGER tr_coml
 BEFORE
   INSERT OR UPDATE OF dt_goal, arch_day, dt_finished, block_duration
 ON tasks
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
begin
    :NEW.DT_END_BLOCK := ADD_TIMESTAMP_SEC(nvl(:NEW.DT_GOAL, systimestamp),nvl(:NEW.BLOCK_DURATION, 0));
    if :NEW.DT_FINISHED is not null then
        :NEW.DT_ARCH := ADD_TIMESTAMP_SEC(:NEW.DT_FINISHED,nvl(:NEW.ARCH_DAY, 7)*60*60*24);
    end if;
end;
/
COMMENT ON TABLE tasks IS '������ �������'
/
COMMENT ON COLUMN tasks.arch_day IS '���������� ����, ����� ������� ����� ���������� ������ ������ ���� �������� � �����'
/
COMMENT ON COLUMN tasks.block_duration IS '������������ �������� ���������� ������� � ��������'
/
COMMENT ON COLUMN tasks.classname IS '����� ������'
/
COMMENT ON COLUMN tasks.dt_arch IS '����� timeout ����������'
/
COMMENT ON COLUMN tasks.dt_created IS '���� �������� ������'
/
COMMENT ON COLUMN tasks.dt_finished IS '���� ���������� ������'
/
COMMENT ON COLUMN tasks.dt_goal IS '����, �� ������� ��������� ���������� �������'
/
COMMENT ON COLUMN tasks.dt_started IS '���� ������ ���������� ������'
/
COMMENT ON COLUMN tasks.errp IS '��������� ��������� �� ������'
/
COMMENT ON COLUMN tasks.errs IS '���������� �� ������'
/
COMMENT ON COLUMN tasks.finish_duration IS '������������ �������� ���������� ������� � �������� � ������� dt_goal'
/
COMMENT ON COLUMN tasks.id IS '������������� ������'
/
COMMENT ON COLUMN tasks.initiator_id IS '��������� ������'
/
COMMENT ON COLUMN tasks.last_ping IS '��������� ��������� � ������ �� �����������'
/
COMMENT ON COLUMN tasks.opid IS '������� �������������'
/
COMMENT ON COLUMN tasks.parent_id IS '������ - �������� ������'
/
COMMENT ON COLUMN tasks.processor_id IS '��������� ������'
/
COMMENT ON COLUMN tasks.program IS '���������, ����������� ������'
/
COMMENT ON COLUMN tasks.status_id IS '������� ������ ��������� ������'
/
CREATE TABLE tasks_arch
    (id                             NUMBER(*,0) NOT NULL,
    dt_created                     TIMESTAMP (6) DEFAULT systimestamp,
    dt_goal                        TIMESTAMP (6),
    dt_started                     TIMESTAMP (6),
    dt_finished                    TIMESTAMP (6),
    status_id                      VARCHAR2(16) DEFAULT 'CREATED',
    classname                      VARCHAR2(32),
    initiator_id                   NUMBER(*,0) DEFAULT 0 NOT NULL,
    processor_id                   NUMBER(*,0) DEFAULT 0,
    program                        VARCHAR2(32),
    opid                           VARCHAR2(32),
    block_duration                 NUMBER DEFAULT 10,
    finish_duration                NUMBER DEFAULT 120,
    arch_day                       NUMBER DEFAULT 7,
    last_ping                      TIMESTAMP (6) DEFAULT systimestamp,
    errs                           VARCHAR2(4000),
    errp                           CLOB,
    parent_id                      NUMBER,
    dt_end_block                   TIMESTAMP (6),
    dt_arch                        TIMESTAMP (6))
/
CREATE INDEX idx_tasks_arch ON tasks_arch
  (
    id                              ASC
  )
/
CREATE INDEX idx_tasks_arch_00 ON tasks_arch
  (
    classname                       ASC
  )
/
CREATE INDEX idx_tasks_arch_01 ON tasks_arch
  (
    dt_goal                         ASC,
    classname                       ASC
  )
/
CREATE INDEX idx_tasks_arch_02 ON tasks_arch
  (
    opid                            ASC
  )
/
ALTER TABLE tasks_arch
ADD CONSTRAINT limit1_duration CHECK (block_duration >= 0 and block_duration <= 3600*24)
/
COMMENT ON TABLE tasks_arch IS '������ ������� � ������'
/
COMMENT ON COLUMN tasks_arch.arch_day IS '���������� ����, ����� ������� ����� ���������� ������ ������ ���� �������� � �����'
/
COMMENT ON COLUMN tasks_arch.block_duration IS '������������ �������� ���������� ������� � ��������'
/
COMMENT ON COLUMN tasks_arch.classname IS '����� ������'
/
COMMENT ON COLUMN tasks_arch.dt_arch IS '���� ���������'
/
COMMENT ON COLUMN tasks_arch.dt_created IS '���� �������� ������'
/
COMMENT ON COLUMN tasks_arch.dt_end_block IS '������� ���� ����������'
/
COMMENT ON COLUMN tasks_arch.dt_finished IS '���� ���������� ������'
/
COMMENT ON COLUMN tasks_arch.dt_goal IS '����, �� ������� ��������� ���������� �������'
/
COMMENT ON COLUMN tasks_arch.dt_started IS '���� ������ ���������� ������'
/
COMMENT ON COLUMN tasks_arch.errp IS '��������� ��������� �� ������'
/
COMMENT ON COLUMN tasks_arch.errs IS '���������� �� ������'
/
COMMENT ON COLUMN tasks_arch.finish_duration IS '������������ �������� ���������� �������'
/
COMMENT ON COLUMN tasks_arch.id IS '������������� ������'
/
COMMENT ON COLUMN tasks_arch.initiator_id IS '��������� ������'
/
COMMENT ON COLUMN tasks_arch.last_ping IS '��������� ��������� � ������ �� �����������'
/
COMMENT ON COLUMN tasks_arch.opid IS '������� �������������'
/
COMMENT ON COLUMN tasks_arch.parent_id IS '������ - �������� ������'
/
COMMENT ON COLUMN tasks_arch.processor_id IS '��������� ������'
/
COMMENT ON COLUMN tasks_arch.program IS '���������, ����������� ������'
/
COMMENT ON COLUMN tasks_arch.status_id IS '������� ������ ��������� ������'
/
CREATE OR REPLACE 
FUNCTION create_task
  ( pClassName IN tasks.classname%type
  , pSubject_Id IN tasks.initiator_id%type
  , pParent_Id IN tasks.parent_id%type DEFAULT 0
  , pBlock_duration IN tasks.block_duration%type default 0
  , pFinish_duration IN tasks.finish_duration%type default 0) 
  RETURN  tasks.id%type IS
    xId tasks.id%type;
BEGIN 
    insert into tasks(id, status_id, classname, initiator_id, parent_id, block_duration, finish_duration)
    values(seq_tasks.NEXTVAL, 'CREATED', pClassName, pSubject_Id, pParent_Id, pBlock_duration, pFinish_duration)
    returning id into xId;
    commit;
    RETURN xId;
END;
/
CREATE OR REPLACE 
FUNCTION get_second_interval(pStart in timestamp, pEnd in timestamp) 
return number
-- �������� ������� � �������� ����� ����� ������ � ������� timestamp
as  
    xSec number;
    xMin number;
    xHour number;
    xDay number;
begin
    xSec := extract(second from (pEnd - pStart));
    xMin := extract(minute from (pEnd - pStart));
    xHour := extract(hour from (pEnd - pStart));
    xDay := extract(day from (pEnd - pStart));
    return xSec + 60*(xMin + 60*(xHour + xDay*24));
end;
/
CREATE OR REPLACE 
FUNCTION get_subject_id
  RETURN  tasks.initiator_id%type IS
    xId tasks.initiator_id%type;
BEGIN
    select seq_subject.NEXTVAL into xId from dual;
    RETURN xId;
END;
/


CREATE OR REPLACE 
FUNCTION get_value_from_split (
  pName IN varchar2,               -- ��� ���������
  pStr IN varchar2,                -- ������ ���������� � �������� � ������������� ���� 'aaa1=bbb;aaa2=ccc'
  pSplit IN varchar2 default ';',  -- �����������
  pEq IN varchar2 default '=' )    -- ���������
RETURN  varchar2 IS
  i integer;
  j integer;
  pos integer;
BEGIN
  if ( instr( pName, pSplit ) > 0  or instr( pName, pEq ) > 0 ) then
    return null; -- ���������� null ���� ��� ��������� �������� ����������� ��� ���� ���������
  end if;
  
  i := instr( pStr, pSplit || pName || pEq ); -- ������� ���������
  if (i <= 0) then
    i := instr( pStr, pName || pEq ); -- �������� ��� ������
    if (i != 1) then
      return null;
    end if;
  else
    i := i + length( pSplit ); -- �������� ��� �� ������
  end if;
  
  if (i > 0) then  -- �������� ������ � ������
    pos := i + length( pName || pEq ); -- ������� �������� ���������
    j := instr( pStr, pSplit, pos ); --  ��������� �������� (�����������)
    if (j > 0) then
      return substr( pStr, pos, j - pos ); -- ����� ���� ������ ���������
    else
      return substr( pStr, pos ); -- �������� ��� ���������
    end if;
  end if;
  
  return null;
EXCEPTION
   when others then
       return null;
END;
/
CREATE OR REPLACE 
FUNCTION set_blocked
  (pProgram IN tasks.program%type
  , pProcessorId IN tasks.processor_id%type
  , pWait in number default 120
  , pCls out varchar2)
  RETURN  tasks.id%type IS
    constEvent constant varchar2(32) := 'center$change_status_ready';
    
    xId tasks.id%type;
    xFirst  timestamp;
    xLast   timestamp;
    xMsg    varchar2(2000);
    xsId    varchar2(32);
    xStatus number;
    xi      number;
    xsClass varchar2(32);
    xDt     timestamp;

    FUNCTION get_value(pName IN varchar2, pStr IN varchar2)
    RETURN  varchar2 IS
        i integer;
        j integer;
        pos integer;
    BEGIN 
        i := instr(pStr, ';'||pName||'=');
        if (i <= 0) then
            i := instr(pStr, pName||'=');
            if (i != 1) then
                return null;
            end if;
        else
            i := i + 1;
        end if;
        if (i > 0) then
            pos := i + length(pName) + 1;
            j := instr(pStr, ';', pos);
            if (j > 0) then
             return substr(pStr, pos, j - pos);
            else
             return substr(pStr, pos);
            end if;
        end if;
        RETURN null ;
    EXCEPTION WHEN others THEN
       return null;
    END;

    FUNCTION block_by_id(pID IN number)
    RETURN  number IS
        x number;
    BEGIN 
        select id 
        into x
        from tasks u
        where u.id = pId
            and u.processor_id in (0, pProcessorId)
            and u.status_id='READY'
        for update nowait;
        if (x is not null)then
            x := null;
                
            update tasks u
            set u.status_id = 'BLOCKED',
                u.program = pProgram,
                u.processor_id = pProcessorId
            where u.id = pId
                and u.processor_id in (0, pProcessorId)
                and u.status_id='READY'
            returning u.id, u.classname into x, pCls;
            commit;
            return x;
        end if;
    EXCEPTION WHEN others THEN
       return null;
    END;
BEGIN
    if (pWait <= 0) then
        return null;
    end if;
--write_log(pProcessorId||':START');
    commit;
    -- ����������� ���������� �������
    dbms_alert.register(constEvent);
    commit;
--write_log(pProcessorId||':REGISTER');

    xId := null;
    -- ���� ������ ��������
    xFirst := SYSTIMESTAMP;
    -- ���� ����� ��������
    xLast := add_timestamp_sec(xFirst, pWait);

    xDt := xFirst;
    -- ���� ����� ��� �� �����
    while xDt < xLast loop
--write_log(pProcessorId||':'||xDT||' < '||xLast);
        -- ������� �������� �������
        for v in (select * from (
                select /*+index(l idx_tasks_05)*/ l.id id
                from tasks l
                where l.status_id='READY' 
                    and dt_end_block > xDt
                    and l.dt_goal <= xDt
                    and exists(select r.classname from classes r
                        where r.processor_id = pProcessorId and r.classname = l.classname)
                    and l.processor_id in (0, pProcessorId)
                order by dt_end_block
                )
                union all select * from (
                select l.id id
                from tasks l
                where l.status_id='READY' 
                    and l.dt_goal <= xDt 
                    and exists(select r.classname from classes r
                        where r.processor_id = pProcessorId and r.classname = l.classname)
                    and l.block_duration = 0
                    and l.processor_id in (0, pProcessorId)
                order by l.dt_goal
                )
        ) loop
            begin
--write_log(pProcessorId||':LOOP ID='||v.ID);
                xId := block_by_id(v.Id);
                -- ���� ������� ��������
                if (xId is not null and xId > 0) then
                    dbms_alert.remove(constEvent);
--write_log(pProcessorId||':RETURN ID='||xID);
                    RETURN xId;
                end if;
            exception when others then
--write_log(pProcessorId||':EXCEPTION:'||sqlerrm);
                null;
            end;
        end loop;

        xDt := SYSTIMESTAMP;
        -- ���� �������� ��������� ��������� �� timeout
        while (xDt < xLast) loop
--write_log(pProcessorId||':LOOP WAIT');
            commit;
            -- ��������
            dbms_alert.waitone(constEvent,xMsg,xStatus,get_second_interval(xDt, xLast));
            --  �������� ���������
--write_log(pProcessorId||':STATUS = '||xSTATUS);
            if (xStatus = 0) then
                xsClass := get_value('class', xMsg);

                begin
                    select 1 
                    into xI
                    from classes 
                    where processor_id = pProcessorId 
                        and classname = xsClass
                        and rownum = 1;
                    -- ������������� ������
                    xsId := get_value('id', xMsg);
                    if (xsId is not null and to_number(xsId) > 0) then
                        -- ������� �������������
                        xDt := SYSTIMESTAMP;
                        xId := block_by_id(to_number(xsId));
                        -- ���� ������� ��������
                        if (xId is not null and xId > 0) then
                            dbms_alert.remove(constEvent);
--write_log(pProcessorId||':RETURN ID='||xID);
                            RETURN xId;
                        end if;
                    end if;
                exception when no_data_found then
                    null;
                end;
                goto cntn;
            end if;
            xDt := SYSTIMESTAMP;
        end loop;
        -- ����� �� timeout 
        dbms_alert.remove(constEvent);
--write_log(pProcessorId||':return null');
        return null;
<<cntn>>
        -- ����� - �� ����� ����
        xDt := SYSTIMESTAMP;
    end loop;
    dbms_alert.remove(constEvent);
--write_log(pProcessorId||':return null');
    return null;
exception when others then
    dbms_alert.remove(constEvent);
    raise;
END;
/
CREATE OR REPLACE 
FUNCTION set_param(pTask_id in params.task_id%type
    , pGrp in params.grp%type
    , pName in params.name%type
    , pValue in params.value%type
    , pValue_clob in params.value_clob%type
    , pValue_blob in params.value_blob%type
    , pValue_clmn in params.value_clmn%type
    , pLen in params.len%type
    , pTP in params.tp%type default null
    , pWait in params.stat_wait%type default null
    , pCalc in params.stat_calc%type default null
    , pSCall in params.stat_scall%type default null
    )
return number
is
    xId number;
begin
    insert into params(task_id, grp, name
        , value, value_clob, value_blob
        , value_clmn, len, tp, stat_wait, stat_calc, stat_scall)
    values (pTask_id, pGrp, pName
        , pValue, pValue_clob, pValue_blob
        , pValue_clmn, pLen, pTP, pWait, pCalc, pSCall)
    returning id into xId;
    return xId;
exception when dup_val_on_index then
    update params
    set value = pValue
        , value_clob = pValue_clob
        , value_blob = pValue_blob
        , value_clmn = pValue_clmn
        , len = pLen
        , tp = pTP
        , stat_wait = pWait
        , stat_calc = pCalc
        , stat_scall = pSCall
    where (task_id = pTask_id
        and pGrp is null 
        and grp is null
        and name = pName)
        or (task_id = pTask_id
        and grp = pGrp
        and name = pName)
    returning id into xId;
    return xId;
end;
/
CREATE OR REPLACE 
FUNCTION set_ready
  (pId IN tasks.Id%type
  , pGoal IN tasks.dt_goal%type DEFAULT systimestamp)
  RETURN  tasks.id%type IS
    xId tasks.id%type;
BEGIN 
    xId := 0;
    update tasks
    set 
        status_id = 'READY',
        dt_goal = pGoal
    where 
        id = pId and 
        status_id = 'CREATED'
    returning id into xId;
    commit;
    RETURN xId;
END;
/
CREATE OR REPLACE 
FUNCTION wait_for_end
  ( pId IN tasks.id%type
  , pTimeOutSec IN number DEFAULT 120)
  RETURN  status.name%type IS
    constEvent constant varchar2(32) := 'center$change_status_finish';

    xName status.name%type;
    xMsg varchar2(2000);
    xStatus integer;
    xTimeOutSec Number;
    xFirst date;
    xLast date;
    xCT date;
    xFlag number;
    xId number;

    CURSOR xCur(pId IN tasks.id%type) IS
        select l.name
        from status l inner join tasks r on (l.name = r.status_id) 
        where r.id = pId and l.flag=1;
BEGIN
    xName := null;
    xTimeOutSec := nvl(pTimeOutSec, 120)/(60.0*60.0*24.0);
    xFirst := sysdate;
    xLast := xFirst + xTimeOutSec;
    -- ����������� �������� ������� � ������ �������
    dbms_alert.register(constEvent);
    open xCur(pId);
    fetch xCur into xName;
    close xCur;

    while (xName is null) loop
        xMsg := null;
        commit;
        xCT := sysdate;
        if (xCT > xLast) then
            dbms_alert.waitone(constEvent,xMsg,xStatus,0);
        else
            dbms_alert.waitone(constEvent,xMsg,xStatus,(xLast-xCT)*24*60*60);
        end if;
        if (xStatus != 0) then 
            goto END_LOOP; 
        end if;
        xId := to_number(GET_VALUE_FROM_SPLIT('id', xMsg));
        if (xId = pId) then
            xName := GET_VALUE_FROM_SPLIT('new', xMsg);
            dbms_alert.remove(constEvent);
            return xName;
        end if;
    <<END_LOOP>>
        if (sysdate > xLast) then
           dbms_alert.remove(constEvent);
           return xName;
        end if;

        open xCur(pId);
        fetch xCur into xName;
        close xCur;
    end loop;

    dbms_alert.remove(constEvent);
    return xName;
exception when others then
    dbms_alert.remove(constEvent);
    raise;
end;
/
CREATE OR REPLACE 
PROCEDURE archive_sys
IS
   xdt    TIMESTAMP;
   xid    NUMBER;
   xcnt   NUMBER;
--       xT number := 7; -- ������������ ������ ����� � ����
   xp     NUMBER    := 2;             -- ������������ ������ ��������� � ����
/**
������� ������ �� ������� ������ � ��������
�����: ������� �.�.
������: 1.1 23/06/2013
**/
BEGIN
   xdt := SYSTIMESTAMP;

   DELETE FROM params
         WHERE task_id <= 0;

   -- ��������� ��������� ������������� �����
   INSERT INTO params_arch
      SELECT p.*
        FROM tasks t, params p
       WHERE t.dt_arch < xdt AND p.task_id = t.ID;

   DELETE FROM params u
         WHERE u.ID IN (SELECT p.ID
                          FROM tasks t, params p
                         WHERE t.dt_arch < xdt AND p.task_id = t.ID);

   COMMIT;

   -- ��������� ������������� ������
   INSERT INTO tasks_arch
      SELECT *
        FROM tasks
       WHERE dt_arch < xdt;

   DELETE FROM tasks
         WHERE dt_arch < xdt;

   COMMIT;

   -- ��������� ������ ������������� ��� ���� ����������, ���� ���������,
   -- �� �� �����������
   INSERT INTO tasks_arch
      SELECT *
        FROM tasks t
       WHERE t.dt_goal < CAST (xdt - arch_day AS TIMESTAMP)
         AND EXISTS (
                   SELECT *
                     FROM status s
                    WHERE s.NAME = t.status_id AND flag IN (0, 1)
                          AND ROWNUM = 1);

   DELETE FROM tasks t
         WHERE t.dt_goal < CAST (xdt - arch_day AS TIMESTAMP)
           AND EXISTS (SELECT *
                         FROM status s
                        WHERE s.NAME = t.status_id AND flag IN (0, 1));

   COMMIT;

   INSERT INTO tasks_arch
      SELECT *
        FROM tasks t
       WHERE t.dt_goal + t.finish_duration / (24.0 * 60.0 * 60.0) <
                                            CAST (xdt - arch_day AS TIMESTAMP)
         AND t.finish_duration > 0
         AND (status_id = 'READY' OR status_id = 'BLOCKED');

   DELETE FROM tasks t
         WHERE t.dt_goal + t.finish_duration / (24.0 * 60.0 * 60.0) <
                                            CAST (xdt - arch_day AS TIMESTAMP)
           AND t.finish_duration > 0
           AND (status_id = 'READY' OR status_id = 'BLOCKED');

   COMMIT;

   -- ��������� ����������� ���������
   INSERT INTO params_arch
      SELECT *
        FROM params
       WHERE NOT EXISTS (SELECT *
                           FROM tasks t
                          WHERE t.ID = task_id AND ROWNUM = 1);

   DELETE FROM params
         WHERE NOT EXISTS (SELECT *
                             FROM tasks t
                            WHERE t.ID = task_id AND ROWNUM = 1);

   COMMIT;

   -- ��������� ����������� �������
   INSERT INTO params_array_arch
      SELECT *
        FROM params_array a
       WHERE NOT EXISTS (SELECT *
                           FROM params p
                          WHERE p.ID = a.ID AND ROWNUM = 1);

   DELETE FROM params_array a
         WHERE NOT EXISTS (SELECT *
                             FROM params p
                            WHERE p.ID = a.ID AND ROWNUM = 1);

   COMMIT;

   -- ��������� ����������� ������� ��������
   INSERT INTO status_history_arch
      SELECT *
        FROM status_history s
       WHERE NOT EXISTS (SELECT *
                           FROM tasks t
                          WHERE t.ID = s.task_id AND ROWNUM = 1);

   DELETE FROM status_history s
         WHERE NOT EXISTS (SELECT *
                             FROM tasks t
                            WHERE t.ID = s.task_id AND ROWNUM = 1);

   COMMIT;
   -- ��������� ��������
   xdt := CAST (SYSDATE - xp AS TIMESTAMP);

   INSERT INTO protocol_arch
      SELECT *
        FROM protocol
       WHERE dt < xdt;

   DELETE FROM protocol
         WHERE dt < xdt;

   COMMIT;
END;
/
CREATE OR REPLACE 
PROCEDURE write_log
   ( pTXT IN varchar2)
   IS
pragma autonomous_transaction;
BEGIN
    insert into "LOG"(txt) values(pTXT);
    commit;
END;
/

INSERT INTO status
("NAME","FLAG")
VALUES
('UNKNOWN',2)
/
INSERT INTO status
("NAME","FLAG")
VALUES
('CREATED',0)
/
INSERT INTO status
("NAME","FLAG")
VALUES
('READY',2)
/
INSERT INTO status
("NAME","FLAG")
VALUES
('PROCESSING',2)
/
INSERT INTO status
("NAME","FLAG")
VALUES
('DONE_OK',1)
/
INSERT INTO status
("NAME","FLAG")
VALUES
('DONE_ERR',1)
/
INSERT INTO status
("NAME","FLAG")
VALUES
('CANCELED',1)
/
INSERT INTO status
("NAME","FLAG")
VALUES
('BROKEN',1)
/
INSERT INTO status
("NAME","FLAG")
VALUES
('BLOCKED',2)
/
INSERT INTO status
("NAME","FLAG")
VALUES
('BREAKING',2)
/
INSERT INTO status
("NAME","FLAG")
VALUES
('SCHEDULED',2)
/
INSERT INTO status
("NAME","FLAG")
VALUES
('DONE_TOUT',1)
/
commit
/
