package center.task;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.Set;

import ru.vassaev.core.base.Null;

/**
 * Список значений по именам и индексам
 * Для межпоточного использования необходима синхронизация
 * @author Vassaev A.V.
 * @version 1.1 05/06/2012
 */
public class Record implements Serializable {
	private static final long serialVersionUID = -1595687107496864939L;
	public LinkedList<Object> value = new LinkedList<Object>();
  public HashMap<String, Integer> name = new HashMap<String, Integer>();
  /**
   * Получить значение по индексу
   * @param index - строковое представление индекса
   * @return Значение
   * @throws NullPointerException в случае, если index = null
   */
  public final Object getObject(String index) throws NullPointerException {
		try {
			int i = Integer.parseInt(index);
			if (value.size() <= i)
				return null;
			return value.get(i);
		} catch (NumberFormatException e) {
			Integer i = name.get(index);
			if (i == null)
				return null;
			return value.get(i);
		}
  }

  public final Object getObject(int i) {
		return value.get(i);
  }
  public final Set<String> getNames() {
  	return name.keySet();
  }
  public final void setObject(String index, Object o) {
  	Integer i = name.get(index);
  	if (i != null) {
  		value.set(i, o);
  	} else {
  		i = value.size();
  		name.put(index, i);
  		value.addLast(o);
  	}
  }
  
  public final void clear() {
  	value.clear();
  	name.clear();
  }
  
  public final Object clone() {
  	Record r = new Record();
  	r.value = (LinkedList<Object>)value.clone();
  	r.name = (HashMap<String, Integer>)name.clone();
  	return r;
  }

  public final void setSize(int size) {
		for(int i = value.size(); i < size; i++)
			value.add(Null.NULL);
		for(int i = value.size() - 1; i >= size; i--)
			value.remove(i);
  }

  public final int size() {
  	return value.size();
  }

  public final String toString() {
  	try {
      Object o = value.getFirst();
      if (o != null)
        return o.toString();
      return null;
  	} catch(NoSuchElementException e) {
  		return null;
  	}
  }
}
