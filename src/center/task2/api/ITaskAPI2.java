package center.task2.api;

import java.util.Set;

import center.task.State;

import ru.vassaev.core.exception.SysException;

public interface ITaskAPI2 {
	/**
	 * Зарегистрировать участника
	 * @param info - строка описания участника
	 * @param prms - параметры участника
	 * @return возвращает уникальный идентификатор участника
	 * @throws SysException
	 */
	public Long registerSubject(String info, String ...prms) throws SysException;
	
	/**
	 * Получить шаблон для заполнения задачи
	 * @param subject - идентификатор участника (инициатора)
	 * @return - структура задачи для заполнения
	 * @throws SysException
	 */
	public Tsk createTemplateTask(Long subject) throws SysException, TAPI2Exception;

	/**
   * Создать задачу
   * @param subject - идентификатор участника (инициатор)
   * @param tsk - структура задачи
   * @return обновлённая структура задачи
   * @throws SysException
   */
	public Tsk createTask(Long subject, Tsk tsk) throws SysException, TAPI2Exception;

	/**
	 * Установить параметр задачи
	 * @param subject - участник (инициатор или исполнитель)
	 * @param tsk - задача
	 * @param grp - группа параметра
	 * @param name - наименование параметра
	 * @param value - значение параметра
	 * @return Идентификатор параметра
	 * @throws SysException - в случае системной ошибки
	 * @throws TAPI2Exception - в случае отсутствия регистрации участника
	 */
	public Long putParam(Long subject, Long tsk, String grp, String name, Object value) throws SysException, TAPI2Exception;

	/**
	 * Сообщить, что задача подготовлена
	 * @param subject - участник (инициатор задачи)
	 * @param tsk - идентификатор задачи
	 * @throws SysException - в случае системной ошибки
	 * @throws TAPI2Exception - в случае отсутствия регистрации участника
	 */
	public void readyTask(Long subject, Long tsk) throws SysException, TAPI2Exception;
	/**
	 * Добавить список классов, которые может обрабатывать исполнитель
	 * @param subject - идентификатор участника (исполнитель)
	 * @param classes - список классов
	 * @throws SysException - в случае системной ошибки
	 * @throws TAPI2Exception - в случае отсутствия регистрации участника
	 */
	public void addClasses(Long subject, String ...classes) throws SysException, TAPI2Exception;
	/**
	 * Удалить классы из списка возможной обработки
	 * @param subject - идентификатор участника (исполнитель)
	 * @param classes - список классов
	 * @throws SysException - в случае системной ошибки
	 * @throws TAPI2Exception - в случае отсутствия регистрации участника
	 */
	public void removeClasses(Long subject, String ...classes) throws SysException, TAPI2Exception;
	/**
	 * Получить задачу от брокера для процессирования
	 * @param subject - исполнитель
	 * @param wait - период ожидания получения задачи от брокера
	 * @return Заполненная структура задачи
	 * @throws SysException - в случае системной ошибки
	 * @throws TAPI2Exception - в случае отсутствия регистрации участника
	 */
	public Tsk takeTask(Long subject, long wait) throws SysException, TAPI2Exception;
	/**
	 * Сообщить о начале процессирования задачи
	 * @param subject - исполнитель
	 * @param task - идентификатор задачи
	 * @throws SysException - в случае системной ошибки
	 * @throws TAPI2Exception - в случае отсутствия регистрации участника
	 */
	public void fireProcessing(Long subject, Long task) throws SysException, TAPI2Exception;
	/**
	 * Сообщить о завершении задачи
	 * @param subject - исполнитель
	 * @param task - идентификатор задачи
	 * @param state - состояние, в которое задача переведена
	 * @throws SysException - в случае системной ошибки
	 * @throws TAPI2Exception - в случае отсутствия регистрации участника
	 */
	public void fireEnd(Long subject, Long task, State state) throws SysException, TAPI2Exception;
	/**
	 * Установить параметр в задании
	 * @param subject - участник (исполнитель или инициатор)
	 * @param task - идентификатор задачи
	 * @param grp - группа параметра
	 * @param name - имя параметра
	 * @param value - значение параметра
	 * @param wait - общее время ожидания при вычислении параметра
	 * @param calc - общее время собственно вычисления параметра
	 * @param scall - общее время, потраченное на системные вызовы при вычислении параметра
	 * @return Идентификатор параметра
	 * @throws SysException - в случае системной ошибки
	 * @throws TAPI2Exception - в случае отсутствия регистрации участника
	 */
  public Long setParam(Long subject, Long task, String grp, String name, Object value, Long wait, Long calc, Long scall) throws SysException, TAPI2Exception;
  /**
   * Получить параметр
	 * @param subject - участник (исполнитель или инициатор)
	 * @param task - идентификатор задачи
	 * @param grp - группа параметра
	 * @param name - имя параметра
	 * @return Значение параметра
	 * @throws SysException - в случае системной ошибки
	 * @throws TAPI2Exception - в случае отсутствия регистрации участника
	 */
  public Object getParam(Long subject, Long task, String grp, String name) throws SysException, TAPI2Exception;
  
  public boolean tryCancel(Long subject, Long task) throws SysException, TAPI2Exception;
  
}