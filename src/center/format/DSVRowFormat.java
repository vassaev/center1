package center.format;

import javax.swing.table.TableModel;
import ru.vassaev.core.util.Strings;

/**
 * @author Vassaev A.V.
 * @version 1.0
 */
public class DSVRowFormat implements IRowFormat {
    public String format(TableModel tm, int row) {
        StringBuffer sb = new StringBuffer();
        int l = tm.getColumnCount();
        for (int j = 0; j < l; j++) {
            Object obj = tm.getValueAt(row, j);
            if (obj != null)
                Strings.addDVSFieldTo(sb, obj.toString());
            else
                Strings.addDVSFieldTo(sb, null);
        }
        return sb.toString();
    }

    public void load(TableModel tm, int row, String str) {
        String[] vals = Strings.parseDVSLine(str);
        int l = vals.length;
        for (int j = 0; j < l; j++) {
            tm.setValueAt(vals[j], row, j);
        }
    }

    public String format(TableModel tm, int row, int[] clmns) {
        StringBuffer sb = new StringBuffer();
        int l = clmns.length;
        for (int j = 0; j < l; j++) {
            Object obj = tm.getValueAt(row, clmns[j]);
            if (obj != null)
                Strings.addDVSFieldTo(sb, obj.toString());
            else
                Strings.addDVSFieldTo(sb, null);
        }
        return sb.toString();
    }

    public void load(TableModel tm, int row, String str, int[] clmns) {
        String[] vals = Strings.parseDVSLine(str);
        int l = vals.length;
        for (int j = 0; j < l; j++) {
            tm.setValueAt(vals[j], row, clmns[j]);
        }
    }
}
